module.exports = {
  endOfLine: "auto",
  arrowParens: "avoid",
  semi: false,
}
