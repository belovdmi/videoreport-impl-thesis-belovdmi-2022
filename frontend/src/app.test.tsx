import { render, screen } from "@testing-library/react"
import { RecoilRoot } from "recoil"
import App from "./app"

test("renders learn react link", () => {
  render(
    <RecoilRoot>
      <App />
    </RecoilRoot>
  )
  const linkElement = screen.getAllByText(/Registrovat/i)
  expect(linkElement).toBeTruthy()
})
