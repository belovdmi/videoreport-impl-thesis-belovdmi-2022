/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  AssignJobPayload,
  CreateJobPayload,
  UpdateJobPayload,
  CreateVideoResultPayload,
  Job,
  UpdateVideoResultPayload,
  User,
} from "./data-contracts"
import { ContentType, HttpClient, RequestParams } from "./http-client"

export type SortJobsBy =
  | "price.asc"
  | "price.desc"
  | "created.asc"
  | "created.desc"
  | "jobId.asc"
  | "jobId.desc"
  | "dueDate.asc"
  | "dueDate.desc"

export type SortSubsBy =
  | "score.asc"
  | "score.desc"
  | "numberOfReviews.asc"
  | "numberOfReviews.desc"

export type Status =
  | "free"
  | "assigned"
  | "canceled_by_owner"
  | "canceled_by_fulfiller"
  | "submitted"
  | "in_progress"
  | "finished"

export type Role = "fulfiller" | "subscriber" | "assigned" | "creator"

export type GetJobQuery = {
  sortBy?: SortJobsBy
  page?: number
  count?: number
  priceGte?: number
  priceLte?: number
  dueDateGte?: string
  dueDateLte?: string
  region?: string[]
  username?: string
  role?: Role
  category?: string
  status?: Status
}

export class Jobs<
  SecurityDataType = unknown
> extends HttpClient<SecurityDataType> {
  /**
   * @description Creator of job will be logged user. Info about him will be taken from auth token.
   *
   * @tags jobs
   * @name CreateJob
   * @summary Create job.
   * @request POST:/jobs
   * @secure
   */
  createJob = (body: CreateJobPayload, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs`,
      method: "POST",
      body: body,
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetJobs
   * @summary Finds jobs.
   * @request GET:/jobs
   */
  getJobs = (query?: GetJobQuery, params: RequestParams = {}) =>
    this.request<
      { page?: number; count?: number; totalCount?: number; jobs?: Job[] },
      void
    >({
      path: `/jobs`,
      method: "GET",
      query: query,
      format: "json",
      ...params,
    })
  /**
   * @description Creator of job can update job in free state. Info about him will be taken from auth token.
   *
   * @tags jobs
   * @name UpdateJob
   * @summary Update job.
   * @request POST:/jobs/{jobId}
   * @secure
   */
  updateJob = (
    jobId: number,
    body: UpdateJobPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/jobs/${jobId}`,
      method: "POST",
      body: body,
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetJobById
   * @summary Gets job by id.
   * @request GET:/jobs/{jobId}
   */
  getJobById = (jobId: number, params: RequestParams = {}) =>
    this.request<Job, void>({
      path: `/jobs/${jobId}`,
      method: "GET",
      format: "json",
      ...params,
    })
  /**
   * @description Only creator can preforme this in free state. Admin can preforme this at any state.
   *
   * @tags jobs
   * @name DeleteJob
   * @summary Delets job by id.
   * @request DELETE:/jobs/{jobId}
   * @secure
   */
  deleteJob = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}`,
      method: "DELETE",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetJobsCreator
   * @summary Gets user that created job.
   * @request GET:/jobs/{jobId}/creator
   * @secure
   */
  getJobsCreator = (jobId: number, params: RequestParams = {}) =>
    this.request<User, void>({
      path: `/jobs/${jobId}/creator`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    })
  /**
   * @description User will be identified by auth token.  Only user subscriber can be assigned.
   *
   * @tags jobs
   * @name AssignJob
   * @summary Logged user that is creator of job assignes user.
   * @request POST:/jobs/{jobId}/assignee
   * @secure
   */
  assignJob = (
    jobId: number,
    body: AssignJobPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/assignee`,
      method: "POST",
      body: body,
      secure: true,
      type: ContentType.Json,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetAssignee
   * @summary Gets assignee of job.
   * @request GET:/jobs/{jobId}/assignee
   */
  getAssignee = (jobId: number, params: RequestParams = {}) =>
    this.request<User, void>({
      path: `/jobs/${jobId}/assignee`,
      method: "GET",
      format: "json",
      ...params,
    })
  /**
   * @description User will be identified by auth token. Only assigned user of job can preforme this.
   *
   * @tags jobs
   * @name DeclineAssigment
   * @summary Logged user declines job assigment.
   * @request DELETE:/jobs/{jobId}/assignee
   * @secure
   */
  declineAssigment = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/assignee`,
      method: "DELETE",
      secure: true,
      ...params,
    })
  /**
   * @description User will be identified by auth token.  Only user assigned to job can preforme this.
   *
   * @tags jobs
   * @name AcceptFulfillment
   * @summary Logged user accepts fulfillmet of job.
   * @request POST:/jobs/{jobId}/fulfiller
   * @secure
   */
  acceptFulfillment = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/fulfiller`,
      method: "POST",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetJobsFulfiller
   * @summary Gets fulfiller of job.
   * @request GET:/jobs/{jobId}/fulfiller
   */
  getJobsFulfiller = (jobId: number, params: RequestParams = {}) =>
    this.request<User, void>({
      path: `/jobs/${jobId}/fulfiller`,
      method: "GET",
      format: "json",
      ...params,
    })
  /**
   * @description User will be identified by auth token. Only fullfiller of job can preforme this.
   *
   * @tags jobs
   * @name DeclineFulfillment
   * @summary Logged user declines fulfillmet of job.
   * @request DELETE:/jobs/{jobId}/fulfiller
   * @secure
   */
  declineFulfillment = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/fulfiller`,
      method: "DELETE",
      secure: true,
      ...params,
    })
  /**
   * @description User will be identified by auth token.
   *
   * @tags jobs
   * @name Subscribe
   * @summary Logged user will subscribed to job.
   * @request POST:/jobs/{jobId}/subscribers
   * @secure
   */
  subscribe = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/subscribers`,
      method: "POST",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name JobGetSubscribers
   * @summary Gets all subscribers of job.
   * @request GET:/jobs/{jobId}/subscribers
   */
  jobGetSubscribers = (
    jobId: number,
    query?: {
      sortBy?:
        | "score.asc"
        | "score.desc"
        | "numberOfReviews.asc"
        | "numberOfReviews.desc"
    },
    params: RequestParams = {}
  ) =>
    this.request<User[], void>({
      path: `/jobs/${jobId}/subscribers`,
      method: "GET",
      query: query,
      format: "json",
      ...params,
    })
  /**
   * @description Only user that is subscriber (that will be deleted) can preforme this. User will be identified by auth token.
   *
   * @tags jobs
   * @name JobDeleteSubscriber
   * @summary Subscriber cancels subscripton to job.
   * @request DELETE:/jobs/{jobId}/subscribers/{username}
   * @secure
   */
  jobDeleteSubscriber = (
    jobId: number,
    username: string,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/subscribers/${username}`,
      method: "DELETE",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetResults
   * @summary Gets all ids of jobs results.
   * @request GET:/jobs/{jobId}/results
   * @secure
   */
  getResults = (jobId: number, params: RequestParams = {}) =>
    this.request<{ videos?: number[] }, void>({
      path: `/jobs/${jobId}/results`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name SubmitResults
   * @summary Submits work to creator. Work is marked as finished.
   * @request POST:/jobs/{jobId}/results/submit
   * @secure
   */
  submitResults = (jobId: number, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/results/submit`,
      method: "POST",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name CreateVideoResult
   * @summary Creates videos resilt and saves it.
   * @request POST:/jobs/{jobId}/results/videos
   * @secure
   */
  createVideoResult = (
    jobId: number,
    data: CreateVideoResultPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/results/videos`,
      method: "POST",
      body: data,
      secure: true,
      type: ContentType.FormData,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name UpdateVideoResult
   * @summary Updates vidoe result.
   * @request POST:/jobs/{jobId}/results/videos/{videosId}
   * @secure
   */
  updateVideoResult = (
    jobId: number,
    videosId: number,
    data: UpdateVideoResultPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/jobs/${jobId}/results/videos/${videosId}`,
      method: "POST",
      body: data,
      secure: true,
      type: ContentType.FormData,
      ...params,
    })
  /**
   * No description
   *
   * @tags jobs
   * @name GetVideoResult
   * @summary Gets url for downloading video result.
   * @request GET:/jobs/{jobId}/results/videos/{videosId}
   * @secure
   */
  getVideoResult = (
    jobId: number,
    videosId: number,
    params: RequestParams = {}
  ) =>
    this.request<{ url?: string }, void>({
      path: `/jobs/${jobId}/results/videos/${videosId}`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    })
}

export const securedJob = new Jobs({
  secure: true,
  securityWorker: accessToken => {
    return accessToken
      ? { headers: { Authorization: `Token ${accessToken}` } }
      : {}
  },
})
