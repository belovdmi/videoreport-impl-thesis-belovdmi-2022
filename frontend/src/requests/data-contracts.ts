/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Job {
  id?: number

  /** @example Please get me a video of this car ! */
  title?: string

  /** @example Just some video will be nice. */
  description?: string

  /** @example 399 */
  price?: number

  /** @example 27.1.2021 */
  dueDate?: string

  /** @example 777711232 */
  contactPhoneNumber?: string

  /** @example other@mail.com */
  contactEmail?: string

  /** @example https://www.sauto.cz/osobni/detail/mercedes-benz/sls-amg/160897006 */
  advertisementLink?: string
  status?: "free" | "assigned" | "in_progress" | "finished"
  category?: { id?: number; name?: string }
  location?: {
    latitude?: number
    longitude?: number
    address?: string
    town?: string
    region?: string
  }
}

export interface User {
  /** @example admin */
  username?: string

  /** @example I like cars. */
  description?: string
  specializations?: { id?: number; name?: string }[]

  /** @example zchecknito@gmial.cz */
  email?: string

  /** @example 723643621 */
  phoneNumber?: string

  /** @example PHA */
  region?: string
  scoreSummary?: {
    score?: number
    jobsTaken?: number
    jobsGiven?: number
    numberOfReviews?: number
  }
  isAdmin?: boolean
  isBlocked?: boolean
}

export interface Review {
  id: number
  reviewerUsername: string
  jobId: number
  review: string
  score: number
  created: string
}

export interface CreateUserPayload {
  /**
   * must be unique
   * @example user123
   */
  username?: string

  /**
   * must be unique
   * @example gloznam@centrum.cz
   */
  email?: string

  /** @example User123 */
  password?: string
}

export interface LoginUserPayload {
  /** @example admin */
  username?: string

  /** @example Admin123 */
  password?: string
}

export interface RefreshAuthTokenPayload {
  /** @example eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiaGVsbG8iLCJpYXQiOjE1MTYyMzkwMjJ9.yx5ckmmOHM_kUrqaH_KIfRDvymrB4Z1S2ReshBtVatw */
  refreshToken?: string
}

export interface ForgotPasswordPayload {
  /** @example zchecknito@gmail.com */
  email?: string
}

export interface ResetForgotenPasswordPayload {
  /** @example Heslo123 */
  newPassword?: string

  /** @example eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiaGVsbG8iLCJpYXQiOjE1MTYyMzkwMjJ9.yx5ckmmOHM_kUrqaH_KIfRDvymrB4Z1S2ReshBtVatw */
  resetToken?: string
}

export interface UpdateUserPayload {
  /** @example Hello, my name is Peter and I like cars. */
  description?: string
  specializations?: string[]

  /**
   * Email format will be validated
   * @example newEmail@gmail.com
   */
  email?: string

  /** @example 777 888 111 */
  phoneNumber?: string

  /**
   * Abbreviations are user for region. Only regions of CZE are supported.
   * @example PHA
   */
  region?: string
}

export interface ChangePasswordPayload {
  oldPassword?: string
  newPassword?: string
}

export interface CreateReviewPayload {
  jobId?: number
  score?: number
  review?: number
}

export interface CreateJobPayload {
  /** @example Please get me a video of this car ! */
  title?: string

  /** @example Just some video will be nice. */
  description?: string

  /** @example 400 */
  price?: number

  /** @example 29.8.2022 */
  dueDate?: string

  /** @example Auta */
  category?: string

  /** @example 29.8.2022 */
  phoneNumber?: string

  /** @example novyPetr123@gmial.com */
  email?: string

  /** @example V Zátoce 11 */
  address?: string

  /** @example PHA */
  region?: string

  /** @example Praha */
  town?: string

  /** @example https://www.sauto.cz/osobni/detail/mercedes-benz/sls-amg/160897006 */
  advertisementLink?: string
}

export interface UpdateJobPayload {
  /** @example Please get me a video of this car ! */
  title?: string

  /** @example Just some video will be nice. */
  description?: string

  /** @example 400 */
  price?: number

  /** @example 29.8.2022 */
  dueDate?: string

  /** @example Auta */
  category?: string

  /** @example 29.8.2022 */
  phoneNumber?: string

  /** @example novyPetr123@gmial.com */
  email?: string

  /** @example V Zátoce 11 */
  address?: string

  /** @example PHA */
  region?: string

  /** @example Praha */
  town?: string

  /** @example https://www.sauto.cz/osobni/detail/mercedes-benz/sls-amg/160897006 */
  advertisementLink?: string
}

export interface AssignJobPayload {
  /**
   * Username of user that will be assigned.
   * @example novyPetr123
   */
  username?: string
}

export interface CreateVideoResultPayload {
  /** The uploaded vidoe file. */
  video: File
}

export interface UpdateVideoResultPayload {
  /** The uploaded vidoe file. */
  video: File
}
