import jwt_decode from "jwt-decode"
import { Users } from "./Users"
export type ExecRefreshTokenProps = {
  refreshToken: string
  authToken: string
}

interface ExecRefreshToken {
  (execData: ExecRefreshTokenProps): Promise<string>
}

/**
 * gets expiration from JWT token
 * @param  {string} token is token to parse
 * @return {number} returns expiration... if can't parse -1
 */
const getTokenExpiration = (token: string): number => {
  if (!token?.length) {
    return -1
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const expiration = jwt_decode<{ exp: number }>(token)?.exp

  return expiration ?? -1
}

export const execRefreshToken: ExecRefreshToken = async execData => {
  const { refreshToken, authToken } = execData
  let newAuthToken = authToken

  const refreshExpiration = getTokenExpiration(refreshToken)
  // checks if refreshToken is not valid
  if (refreshExpiration < Date.now() / 1000) {
    throw new Error("refresh token expired")
  }

  const authExpiration = getTokenExpiration(authToken)
  // checks if authToken is not valid
  if (authExpiration < Date.now() / 1000) {
    const api = new Users()
    // gets new authToken
    const { data } = await api.refreshAuthToken({ refreshToken })
    newAuthToken = data.authToken || ""
  }

  return newAuthToken
}
