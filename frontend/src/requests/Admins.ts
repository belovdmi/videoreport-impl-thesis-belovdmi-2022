/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { HttpClient, RequestParams } from "./http-client"

export class Admins<
  SecurityDataType = unknown
> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags admins
   * @name AddAdminPrivilegesToUser
   * @summary Gives existing user admin priviliges.
   * @request POST:/admins/
   * @secure
   */
  addAdminPrivilegesToUser = (
    body: { username?: string },
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/admins/`,
      method: "POST",
      body: body,
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags admins
   * @name TakesAdminPriviligesFromUser
   * @summary Takes admin priviliges from user.
   * @request DELETE:/admins/{username}
   * @secure
   */
  takesAdminPriviligesFromUser = (
    username: string,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/admins/${username}`,
      method: "DELETE",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags admins
   * @name BlockUser
   * @summary Admin blocks user.
   * @request POST:/admins/user-managment/blocked
   * @secure
   */
  blockUser = (body: { username?: string }, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/admins/user-managment/blocked`,
      method: "POST",
      body: body,
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags admins
   * @name GetBlockedUsers
   * @summary Gets all blocked users.
   * @request GET:/admins/user-managment/blocked
   * @secure
   */
  getBlockedUsers = (params: RequestParams = {}) =>
    this.request<{ usernames?: string[] }, void>({
      path: `/admins/user-managment/blocked`,
      method: "GET",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags admins
   * @name UnblockedUser
   * @summary Unblocks user.
   * @request DELETE:/admins/user-managment/blocked/{username}
   * @secure
   */
  unblockedUser = (username: string, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/admins/user-managment/blocked/${username}`,
      method: "DELETE",
      secure: true,
      ...params,
    })
}
