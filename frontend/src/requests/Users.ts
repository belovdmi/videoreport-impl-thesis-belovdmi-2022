/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  ChangePasswordPayload,
  CreateReviewPayload,
  CreateUserPayload,
  ForgotPasswordPayload,
  LoginUserPayload,
  RefreshAuthTokenPayload,
  ResetForgotenPasswordPayload,
  Review,
  UpdateUserPayload,
  User,
} from "./data-contracts"
import { ContentType, HttpClient, RequestParams } from "./http-client"

export type SortReviewsBy =
  | "created.asc"
  | "created.desc"
  | "score.asc"
  | "score.desc"
  | undefined

export type GetUserReviewsQuery = {
  sortBy?: SortReviewsBy
  page?: number
  count?: number
  role?: "reviewer" | "reciver"
}

export class Users<
  SecurityDataType = unknown
> extends HttpClient<SecurityDataType> {
  /**
   * @description Creation must be with unique email and username.
   *
   * @tags users
   * @name CreateUser
   * @summary Create user.
   * @request POST:/users
   */
  createUser = (body: CreateUserPayload, params: RequestParams = {}) =>
    this.request<
      void,
      "email taken" | "username taken" | "email username taken" | "bad request"
    >({
      path: `/users`,
      method: "POST",
      body: body,
      type: ContentType.Json,
      ...params,
    })
  /**
   * No description
   *
   * @tags users
   * @name GetUsers
   * @summary Gets users.
   * @request GET:/users
   */
  getUsers = (
    query?: {
      sortBy?: (
        | "username.asc"
        | "username.desc"
        | "userId.asc"
        | "userId.desc"
      )[]
      page?: number
      count?: number
    },
    params: RequestParams = {}
  ) =>
    this.request<
      { page?: number; count?: number; totalCount?: number; users?: User[] },
      void
    >({
      path: `/users`,
      method: "GET",
      query: query,
      format: "json",
      ...params,
    })
  /**
   * @description In case of success user will recive auth a refresh token.
   *
   * @tags users
   * @name LoginUser
   * @summary Logs user into the system.
   * @request POST:/users/login
   */
  loginUser = (body: LoginUserPayload, params: RequestParams = {}) =>
    this.request<{ authToken?: string; refreshToken?: string }, void>({
      path: `/users/login`,
      method: "POST",
      body: body,
      type: ContentType.Json,
      format: "json",
      ...params,
    })
  /**
   * No description
   *
   * @tags users
   * @name RefreshAuthToken
   * @summary Get new auth token by providing refresh token.
   * @request POST:/users/refresh-auth-token
   */
  refreshAuthToken = (
    body: RefreshAuthTokenPayload,
    params: RequestParams = {}
  ) =>
    this.request<{ authToken?: string }, void>({
      path: `/users/refresh-auth-token`,
      method: "POST",
      body: body,
      type: ContentType.Json,
      format: "json",
      ...params,
    })
  /**
   * @description User will be identified by auth token. After that refresh tokens will be destroyed.
   *
   * @tags users
   * @name LogoutUser
   * @summary Logs out user.
   * @request POST:/users/logout
   * @secure
   */
  logoutUser = (params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/users/logout`,
      method: "POST",
      secure: true,
      ...params,
    })
  /**
   * No description
   *
   * @tags users
   * @name ForgotPassword
   * @summary Sends link for changing password to user with given email address.
   * @request POST:/users/forgot-password
   */
  forgotPassword = (body: ForgotPasswordPayload, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/users/forgot-password`,
      method: "POST",
      body: body,
      type: ContentType.Json,
      ...params,
    })
  /**
   * No description
   *
   * @tags users
   * @name ResetForgotenPassword
   * @summary Reset password of user. User will be auth by reset token.
   * @request POST:/users/reset-forgotten-password
   */
  resetForgotenPassword = (
    body: ResetForgotenPasswordPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/users/reset-forgotten-password`,
      method: "POST",
      body: body,
      type: ContentType.Json,
      ...params,
    })
  /**
   * @description User will be identified by auth token.
   *
   * @tags users
   * @name UpdateUser
   * @summary Updates data of user. User can only change data to yourself.
   * @request POST:/users/{username}
   * @secure
   */
  updateUser = (
    username: string,
    body: UpdateUserPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/users/${username}`,
      method: "POST",
      body: body,
      secure: true,
      type: ContentType.Json,
      ...params,
    })
  /**
   * No description
   *
   * @tags users
   * @name GetUserByName
   * @summary Get user by name.
   * @request GET:/users/{username}
   */
  getUserByName = (username: string, params: RequestParams = {}) =>
    this.request<User, void>({
      path: `/users/${username}`,
      method: "GET",
      format: "json",
      ...params,
    })
  /**
   * @description User will be identified by auth token.
   *
   * @tags users
   * @name ChangePassword
   * @summary Changes password of logged user.
   * @request POST:/users/{username}/change-password
   * @secure
   */
  changePassword = (
    username: string,
    body: ChangePasswordPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/users/${username}/change-password`,
      method: "POST",
      body: body,
      secure: true,
      type: ContentType.Json,
      ...params,
    })
  /**
   * @description Creator will be identified by auth token. Creator can't be reviewed user.
   *
   * @tags users
   * @name CreateReview
   * @summary Creates review for user with given username.
   * @request POST:/users/{username}/reviews
   * @secure
   */
  createReview = (
    username: string,
    body: CreateReviewPayload,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/users/${username}/reviews`,
      method: "POST",
      body: body,
      secure: true,
      type: ContentType.Json,
      ...params,
    })
  /**
   * @description User will be identified by auth token.
   *
   * @tags users
   * @name GetUserReviews
   * @summary Gets reviews of user with given username.
   * @request GET:/users/{username}/reviews
   */
  getUserReviews = (
    username: string,
    query?: GetUserReviewsQuery,
    params: RequestParams = {}
  ) =>
    this.request<
      {
        page?: number
        count?: number
        totalCount?: number
        reviews?: Review[]
      },
      void
    >({
      path: `/users/${username}/reviews`,
      method: "GET",
      query: query,
      format: "json",
      ...params,
    })
  /**
   * @description Only for admin. Admin will be identified by auth token.
   *
   * @tags users
   * @name DeleteReview
   * @summary Deletes review of user with given username ad review id.
   * @request DELETE:/users/{username}/reviews/{reviewId}
   * @secure
   */
  deleteReview = (
    username: string,
    reviewId: number,
    params: RequestParams = {}
  ) =>
    this.request<void, void>({
      path: `/users/${username}/reviews/${reviewId}`,
      method: "DELETE",
      secure: true,
      ...params,
    })
}

export const securedUser = new Users({
  secure: true,
  securityWorker: accessToken => {
    return accessToken
      ? { headers: { Authorization: `Token ${accessToken}` } }
      : {}
  },
})
