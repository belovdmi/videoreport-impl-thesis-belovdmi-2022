/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  Box,
  Button,
  Drawer,
  Grid,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material"
import { useState } from "react"
import OfferCard from "../components/cards/offer"
import OffersFilterAside from "../components/asides/offers-filter-aside"
import PageWithAsideContainer from "../components/containers/page-with-aside-container"
import NextAndPrevPageButtons from "../components/ui/buttons/next-and-prev-page-buttons"
import { useRecoilState } from "recoil"
import { SortJobsBy } from "../requests/Jobs"
import { aOffersQuery } from "../state/global"
import useLoadJobs from "../hooks/use-load-jobs"
import InformationText from "../components/job-bottom-content/inforamtion-text"

const Offers = () => {
  const [sortBy, setSortBy] = useState<SortJobsBy | "">("")
  const [openDrawer, setOpenDrawer] = useState(false)
  const [offersQuery, setOffersQuery] = useRecoilState(aOffersQuery)

  const { jobs, totalCount } = useLoadJobs()

  const handleChange = (event: SelectChangeEvent) => {
    const newSortBy = event.target.value as SortJobsBy
    setSortBy(newSortBy)

    const newOffersQuery = { ...offersQuery }
    if (newSortBy !== offersQuery.sortBy) {
      newOffersQuery!.sortBy = newSortBy
      newOffersQuery!.page = 1
      newOffersQuery!.count = 12
      setOffersQuery(newOffersQuery)
    }
  }

  return (
    <PageWithAsideContainer asideContent={<OffersFilterAside />}>
      <Grid item container minHeight={50} alignItems="center">
        <Grid xs item>
          <Typography variant={"h4"} color={"primary.main"}>
            Nabidky Prace
          </Typography>
        </Grid>
        <Box>
          <Box pb={1} width={135} display={{ xs: "block", lg: "none" }}>
            <Button
              color="secondary"
              variant="outlined"
              sx={{
                fontWeight: 500,
                textTransform: "none",
                height: 30,
              }}
              size="small"
              fullWidth
              onClick={() => setOpenDrawer(true)}
            >
              Filtry
            </Button>
          </Box>
          <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={sortBy}
            variant="outlined"
            onChange={handleChange}
            sx={{
              width: 135,
              height: 30,
              textAlign: "center",
              fontSize: 13,
            }}
            displayEmpty
            MenuProps={{
              disableScrollLock: true,
            }}
            size="small"
          >
            <MenuItem value={""}>Řadit od</MenuItem>
            <MenuItem value={"price.asc"}>Nejnižší ceny</MenuItem>
            <MenuItem value={"price.desc"}>Nejvyšší ceny</MenuItem>
            <MenuItem value={"created.desc"}>Nejnovější</MenuItem>
            <MenuItem value={"dueDate.asc"}>Končí nejdřív</MenuItem>
            <MenuItem value={"dueDate.desc"}>Končí nejpozdějí</MenuItem>
          </Select>
        </Box>
      </Grid>
      <Grid
        item
        container
        pt={4}
        spacing={jobs && jobs?.length ? 3.5 : 0}
        minHeight="69vh"
      >
        {jobs && jobs?.length ? (
          jobs?.map(data => {
            return <OfferCard key={data.id} data={data} />
          })
        ) : (
          <Grid container justifyContent={"center"} pt={3}>
            <InformationText color={"secondary"}>
              Nic jsem nenašel...
            </InformationText>
          </Grid>
        )}
      </Grid>
      <NextAndPrevPageButtons
        prevDisabled={offersQuery.page === 1}
        nextDisabled={
          offersQuery?.page && offersQuery?.count
            ? offersQuery.page * offersQuery.count >= totalCount
            : true
        }
        onNextPage={() => {
          const newOffersQuery = { ...offersQuery }
          newOffersQuery!.page = (newOffersQuery?.page || 0) + 1
          setOffersQuery(newOffersQuery)
        }}
        onPrevPage={() => {
          const newOffersQuery = { ...offersQuery }
          newOffersQuery!.page = (newOffersQuery?.page || 2) - 1
          setOffersQuery(newOffersQuery)
        }}
      />
      <Drawer open={openDrawer} onClose={() => setOpenDrawer(false)}>
        <Box p={2}>
          <OffersFilterAside afterSubmit={() => setOpenDrawer(false)} />
        </Box>
      </Drawer>
    </PageWithAsideContainer>
  )
}

export default Offers
