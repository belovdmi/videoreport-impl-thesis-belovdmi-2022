import { Button, Paper, Grid, Typography } from "@mui/material"
import LoginRegisterCard from "../components/cards/login-register"

import LogoFooter from "../components/footer/logo-footer"
import CenterContentContainer from "../components/containers/center-content-container"
import ControlledTextField from "../components/ui/text-field/controlled-text-field"
import { ResetForgotenPasswordPayload } from "../requests/data-contracts"
import axios from "axios"
import { useForm } from "react-hook-form"
import { useSetRecoilState } from "recoil"
import { Users } from "../requests/Users"
import { aAlert, aErrorType, aLoading } from "../state/global"
import { Link, useLocation, useNavigate } from "react-router-dom"
import { useErrorHandler } from "react-error-boundary"

const ResetPassword = () => {
  const handleError = useErrorHandler()
  const { handleSubmit, control, reset, watch } = useForm()
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const navigate = useNavigate()

  const { search } = useLocation()
  const resetToken = search.replace("?resetToken=", "") || "badtoken"

  const onSubmit = async (data: ResetForgotenPasswordPayload) => {
    try {
      setLoading(true)
      const api = new Users()
      await api.resetForgotenPassword({
        resetToken,
        newPassword: data.newPassword,
      })
      navigate("/login")
      setAlert({
        open: true,
        severity: "success",
        alertMsg: "Heslo bylo změněno pokračujte prosím k přihlášení!",
      })
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("resetInvalidLink")
      }

      handleError(error)
    } finally {
      reset(undefined, {
        keepValues: false,
      })
      setLoading(false)
    }
  }

  const watchPassword = watch("newPassword", "")

  return (
    <CenterContentContainer>
      <Paper elevation={10} sx={{ width: "100%" }}>
        <Grid container width={"100%"}>
          <Grid
            item
            xs
            p={5}
            width={"100%"}
            container
            direction="column"
            justifyContent={"center"}
          >
            <Typography component={"div"} fontSize={32} pb={2}>
              Obnovení hesla
            </Typography>
            <Typography fontSize={13}>Zadajte vaše nové heslo:</Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid
                width={"100%"}
                container
                direction="column"
                justifyContent={"center"}
              >
                <ControlledTextField
                  name="newPassword"
                  label="Heslo"
                  type="password"
                  control={control}
                  sx={{ pb: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    validate: {
                      longEnough: (data: string) =>
                        (data && data?.length >= 8) ||
                        "Heslo musí mít alespoň 8 znaků.",
                      hasCapital: (data: string) =>
                        /[A-Z]+/g.test(data ?? "") ||
                        "Heslo musí obsahovat alespoň jedno velké písmeno.",
                      hasNumber: (data: string) =>
                        /[0-9]+/g.test(data ?? "") ||
                        "Heslo musí obsahovat alespoň jedno číslo.",
                    },
                  }}
                />
                <ControlledTextField
                  name="passwordRepeat"
                  label="Zopakované heslo"
                  type="password"
                  control={control}
                  sx={{ pb: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    validate: {
                      passwordConfirm: (data: string) =>
                        watchPassword === data ||
                        "Zopakované heslo se neshoduje.",
                    },
                  }}
                />
                <Grid item container alignItems={"center"}>
                  <Grid item xs>
                    <Button variant="outlined" type="submit">
                      Odeslat a změnit
                    </Button>
                  </Grid>
                  <Grid
                    item
                    xs
                    textAlign={"end"}
                    display={{ xs: "block", md: "none" }}
                  >
                    <Button
                      component={Link}
                      to={"/login"}
                      color={"success"}
                      size="small"
                    >
                      Přihlásit se
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </Grid>
          <LogoFooter hideOnXs />
          <Grid md={6} display={{ xs: "none", md: "block" }} item>
            <LoginRegisterCard onlySignIn />
          </Grid>
        </Grid>
      </Paper>
    </CenterContentContainer>
  )
}

export default ResetPassword
