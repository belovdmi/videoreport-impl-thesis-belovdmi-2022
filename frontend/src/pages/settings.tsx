import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material"
import axios from "axios"
import { useEffect, useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { Controller, useForm } from "react-hook-form"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import BasicPageContainer from "../components/containers/basic-page-container"
import ChangePasswordFrom from "../components/forms/change-password-form"
import ControlledTextField from "../components/ui/text-field/controlled-text-field"
import useAuthorizeUser from "../hooks/use-authorize-user"
import useLoadUser from "../hooks/use-load-user"
import { UpdateUserPayload } from "../requests/data-contracts"
import { execRefreshToken } from "../requests/refresh-token"
import { securedUser } from "../requests/Users"
import {
  aAlert,
  aAuthToken,
  aErrorType,
  aLoading,
  aLoggedUsername,
  aRefreshToken,
} from "../state/global"

const Settings = () => {
  const handleError = useErrorHandler()
  const setErrorType = useSetRecoilState(aErrorType)
  const [specializationsChecked, setSpecializationsChecked] = useState<
    string[]
  >([])
  const handleSpecializationsChecked = (value: string) => {
    let newSpecializationsChecked = specializationsChecked
    specializationsChecked.includes(value)
      ? (newSpecializationsChecked = newSpecializationsChecked.filter(
          item => item !== value
        ))
      : newSpecializationsChecked.push(value)

    setSpecializationsChecked(newSpecializationsChecked)
    return newSpecializationsChecked
  }
  const [region, setRegion] = useState<string>("")
  const handleRegionChange = (event: SelectChangeEvent<typeof region>) => {
    const {
      target: { value },
    } = event
    setRegion(value)
  }

  const { handleSubmit, control, setValue } = useForm()
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const setLoading = useSetRecoilState(aLoading)
  const setAlert = useSetRecoilState(aAlert)

  useAuthorizeUser()
  const userData = useLoadUser(loggedUsername)

  useEffect(() => {
    if (userData) {
      setValue("email", userData.email)
      setValue("phoneNumber", userData.phoneNumber)
      setValue("description", userData.description)

      const specializations: string[] = []
      if (userData?.specializations) {
        for (let i = 0; i < userData?.specializations.length; i++) {
          specializations.push(userData.specializations[i]?.name || "")
        }
      }

      setSpecializationsChecked(specializations)
      setRegion(userData?.region || "")
    }
  }, [userData, setRegion, setValue])

  const onSubmit = async (data: UpdateUserPayload) => {
    try {
      setLoading(true)

      const newAuthToken = await execRefreshToken({
        authToken,
        refreshToken,
      })
      setAuthToken(newAuthToken)

      const api = securedUser
      api.setSecurityData(newAuthToken)

      await api.updateUser(loggedUsername, {
        email: data.email,
        description: data.description || "",
        phoneNumber: data.phoneNumber,
        specializations: specializationsChecked,
        region,
      })
      setAlert({
        open: true,
        alertMsg: "Údaje byli úspěšně změněny.",
        severity: "success",
      })
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.data === "email taken") {
        setAlert({
          open: true,
          alertMsg: "Vámi zvolený email již používá jiný uživatel !",
          severity: "error",
        })
        return
      }

      if (
        error instanceof Error &&
        error?.message === "refresh token expired"
      ) {
        setErrorType("logAgain")
      }

      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("forbidden")
      }

      handleError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <BasicPageContainer>
      <Box pt={13} pb={12}>
        <Grid container direction={"column"}>
          <Grid xs item textAlign={"center"} pb={1.5}>
            <Typography
              variant="h4"
              gutterBottom
              fontWeight={500}
              color="primary.main"
            >
              Nastavení
            </Typography>
          </Grid>
          <Grid item pb={1.5}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid container columnSpacing={10} rowSpacing={2}>
                <Grid
                  xs={12}
                  md={6}
                  item
                  container
                  rowGap={2}
                  direction={"column"}
                >
                  <Typography variant="h6" fontWeight={600}>
                    Základní údaje:
                  </Typography>

                  <Typography sx={{ fontSize: 12 }} color="primary.light">
                    Uživatelské jméno:
                  </Typography>
                  <Typography mt={-1} pb={0.2} borderBottom={"solid 1px"}>
                    {loggedUsername || "Unknown"}
                  </Typography>
                  <ControlledTextField
                    name="email"
                    label="Email"
                    type="text"
                    control={control}
                    rules={{
                      maxLength: {
                        value: 100,
                        message: "Překračuje povolenou délku.",
                      },
                      pattern: {
                        /// to ignore: no-useless-escape
                        // eslint-disable-next-line
                        value: /^\w[\w + \.]*\w+@\w+\.[a-z + \.]*[a-z]$/g,
                        message: "Nesprávný formát emailu.",
                      },
                      required: "Toto pole je povinné.",
                    }}
                  />
                  <ControlledTextField
                    name="phoneNumber"
                    label="Telefoní číslo"
                    type="text"
                    control={control}
                    rules={{
                      maxLength: {
                        value: 13,
                        message: "Překračuje povolenou délku.",
                      },
                      minLength: {
                        value: 9,
                        message: "Nesprávný formát telefoního čísla.",
                      },
                      pattern: {
                        value: /^[+]?[0-9]*$/g,
                        message: "Nesprávný formát telefoního čísla.",
                      },
                    }}
                  />
                  <FormControl variant="standard" sx={{ width: "100%" }}>
                    <InputLabel id="demo-simple-select-helper-label">
                      Kraj
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-standard-label"
                      id="demo-simple-select-standard"
                      variant="standard"
                      value={region}
                      onChange={handleRegionChange}
                      label={"Kraj"}
                      MenuProps={{
                        disableScrollLock: true,
                      }}
                    >
                      <MenuItem value={""}>Kraj</MenuItem>
                      <MenuItem value={"PHA"}>Hlavní město Praha</MenuItem>
                      <MenuItem value={"STČ"}>Středočeský kraj</MenuItem>
                      <MenuItem value={"JHČ"}>Jihočeský kraj</MenuItem>
                      <MenuItem value={"PLK"}>Plzeňský kraj</MenuItem>
                      <MenuItem value={"KVK"}>Karlovarský kraj</MenuItem>
                      <MenuItem value={"ULK"}>Ústecký kraj</MenuItem>
                      <MenuItem value={"LBK"}>Liberecký kraj</MenuItem>
                      <MenuItem value={"HKK"}>Královéhradecký kraj</MenuItem>
                      <MenuItem value={"PAK"}>Pardubický kraj</MenuItem>
                      <MenuItem value={"VYS"}>Kraj Vysočina</MenuItem>
                      <MenuItem value={"JHM"}>Jihomoravský kraj</MenuItem>
                      <MenuItem value={"OLK"}>Olomoucký kraj</MenuItem>
                      <MenuItem value={"MSK"}>Moravskoslezský kraj </MenuItem>
                      <MenuItem value={"ZLK"}>Zlínský kraj</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid xs={12} md={6} item container direction={"column"}>
                  <Grid item xs>
                    <Typography
                      variant="h6"
                      component={"div"}
                      fontWeight={600}
                      gutterBottom
                    >
                      Popis:
                    </Typography>
                    <ControlledTextField
                      name="description"
                      type="text"
                      variant="outlined"
                      control={control}
                      rows={5}
                      multiline
                      fullWidth
                      rules={{
                        maxLength: {
                          value: 250,
                          message: "Překračuje povolenou délku.",
                        },
                      }}
                    />
                  </Grid>

                  <Grid item pt={14}>
                    <Typography
                      variant="h6"
                      component={"div"}
                      fontWeight={600}
                      gutterBottom
                    >
                      Specializace:
                    </Typography>
                    <Box display={"flex"} justifyContent={"space-between"}>
                      {["Auta", "Nemovitosti", "Ostatní"].map(name => {
                        return (
                          <FormControlLabel
                            control={
                              <Controller
                                name="specializations"
                                render={({ field }) => {
                                  return (
                                    <Checkbox
                                      checked={specializationsChecked.includes(
                                        name
                                      )}
                                      onChange={() =>
                                        field.onChange(
                                          handleSpecializationsChecked(name)
                                        )
                                      }
                                    />
                                  )
                                }}
                                control={control}
                              />
                            }
                            label={name}
                          />
                        )
                      })}
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Button
                sx={{ mt: 5, mb: -2 }}
                variant={"contained"}
                fullWidth
                type="submit"
              >
                Změnit údaje
              </Button>
            </form>
          </Grid>
          <Divider sx={{ mb: { xs: 6, md: 4 } }} />
          <ChangePasswordFrom />
        </Grid>
      </Box>
    </BasicPageContainer>
  )
}

export default Settings
