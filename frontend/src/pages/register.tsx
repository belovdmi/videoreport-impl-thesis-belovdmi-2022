import { Box, Button, Divider, Grid, Paper, Typography } from "@mui/material"
import CenterContentContainer from "../components/containers/center-content-container"
import LogoFooter from "../components/footer/logo-footer"
import { Link, useNavigate } from "react-router-dom"
import ControlledTextField from "../components/ui/text-field/controlled-text-field"
import { useForm } from "react-hook-form"
import { Users } from "../requests/Users"
import { useSetRecoilState } from "recoil"
import { aAlert, aLoading } from "../state/global"
import axios from "axios"
import { CreateUserPayload } from "../requests/data-contracts"
import { useErrorHandler } from "react-error-boundary"

const Register = () => {
  const handleError = useErrorHandler()
  const { handleSubmit, control, watch, reset } = useForm()
  const setLoading = useSetRecoilState(aLoading)
  const setAlert = useSetRecoilState(aAlert)
  const navigate = useNavigate()

  const onSubmit = async (data: CreateUserPayload) => {
    try {
      setLoading(true)
      const api = new Users()
      await api.createUser(data)
      navigate("/login")
      setAlert({
        open: true,
        alertMsg: "Registrace proběhla úspěšně. Nyní se múžete přihlasit.",
        severity: "success",
      })
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 400) {
        let message = "Něco se nepovedlo zkuste to prosím znovu !"

        if (error.response?.data === "username email taken") {
          message =
            "Uživatel s tímto emailem a uživatelským jménem již existuje !"
        }

        if (error.response?.data === "username taken") {
          message = "Uživatelské jméno je již obsazeno ! Zvolte prosím jiné."
        }

        if (error.response?.data === "email taken") {
          message = "Tento email je již registrován ! Zvolte prosím jiný."
        }

        setAlert({
          open: true,
          alertMsg: message,
          severity: "error",
        })
      } else {
        handleError(error)
      }
    } finally {
      reset(undefined, {
        keepValues: false,
      })
      setLoading(false)
    }
  }

  const watchPassword = watch("password", "")

  return (
    <CenterContentContainer>
      <Paper elevation={10} sx={{ width: "100%", maxWidth: 700 }}>
        <Grid container item maxWidth={700}>
          <Grid
            item
            xs
            px={5}
            py={3}
            width={"100%"}
            container
            direction="column"
            justifyContent={"center"}
          >
            <Typography component={"div"} fontSize={32} pb={2}>
              Registrace
            </Typography>
            <Typography fontSize={13}>Votvořte si nový účet</Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid
                width={"100%"}
                container
                direction="column"
                justifyContent={"center"}
              >
                <ControlledTextField
                  name="username"
                  label="Uživatelské jméno"
                  type="text"
                  control={control}
                  sx={{ my: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    validate: {
                      longEnough: (data: string) =>
                        (data && data?.length <= 35) ||
                        "Uživatelské jméno může mít maximálně 35 znaků.",
                      hasOnlyNumsAndLetters: (data: string) =>
                        /\w+/g.test(data ?? "") ||
                        "Uživatelské jméno může obsahovat pouze čísla a písmena.",
                      noSpaces: (data: string) =>
                        !data.includes(" ") ||
                        "Uživatelské jméno nemůže obsahovat mezery.",
                    },
                  }}
                />
                <ControlledTextField
                  name="email"
                  label="Email"
                  type="text"
                  control={control}
                  sx={{ pb: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    pattern: {
                      /// to ignore: no-useless-escape
                      // eslint-disable-next-line
                      value: /^\w[\w + \.]*\w+@\w+\.[a-z + \.]*[a-z]$/g,
                      message: "Nesprávný formát emailu.",
                    },
                  }}
                />
                <ControlledTextField
                  name="password"
                  label="Heslo"
                  type="password"
                  control={control}
                  sx={{ pb: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    validate: {
                      longEnough: (data: string) =>
                        (data && data?.length >= 8) ||
                        "Heslo musí mít alespoň 8 znaků.",
                      hasCapital: (data: string) =>
                        /[A-Z]+/g.test(data ?? "") ||
                        "Heslo musí obsahovat alespoň jedno velké písmeno.",
                      hasNumber: (data: string) =>
                        /[0-9]+/g.test(data ?? "") ||
                        "Heslo musí obsahovat alespoň jedno číslo.",
                    },
                  }}
                />
                <ControlledTextField
                  name="passwordRepeat"
                  label="Zopakované heslo"
                  type="password"
                  control={control}
                  sx={{ pb: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    validate: {
                      passwordConfirm: (data: string) =>
                        watchPassword === data ||
                        "Zopakované heslo se neshoduje.",
                    },
                  }}
                />
                <Box width={"100%"} textAlign={"center"} pb={3}>
                  <Button
                    type="submit"
                    variant="outlined"
                    size="large"
                    fullWidth
                  >
                    Register
                  </Button>
                </Box>
              </Grid>
            </form>
            <Divider />
            <Typography pt={2} textAlign={"center"} variant="subtitle2">
              Pokud již máte účet pokračujte prosím k přihlášení:
              <Button size="small" component={Link} to={"/login"}>
                Přihlásit se
              </Button>
            </Typography>
          </Grid>
          <LogoFooter />
        </Grid>
      </Paper>
    </CenterContentContainer>
  )
}

export default Register
