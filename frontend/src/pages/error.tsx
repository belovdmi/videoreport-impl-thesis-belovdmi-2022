import { useRecoilValue } from "recoil"
import { aErrorType } from "../state/global"
import ErrorTemplate from "../components/error/error-template"

/**
 * page will display correct error msg based on errorType
 */
const Error = () => {
  const errorType = useRecoilValue(aErrorType)

  if (errorType === "unauthorized") {
    return (
      <ErrorTemplate title="Sem zatím nesmíte...">
        Vypadáto že na tuto stránku zatím nemáte přistup. Buď nejste přihlášen
        nebo nemáte dostatečná práva. Pokračujte prosím zpět na úvodní
        obrazovku.
      </ErrorTemplate>
    )
  }

  if (errorType === "forbidden") {
    return (
      <ErrorTemplate
        title="Je pro tento krok je pořeba být přihlášen..."
        buttonText="Přihlásit se"
        buttonLink="/login"
      >
        Potřebujeme aby jste se přihlásil. Zdřejmě doslo k vypršení platnosti
        tokenu nebo zatím ještě přihlášen nejste. Prokračujte prosím k
        přihlášení. Pokud nejde o ani jeden z těchto připadů kontaktujte prosím
        administrátora.
      </ErrorTemplate>
    )
  }

  if (errorType === "resetInvalidLink") {
    return (
      <ErrorTemplate title="Odkaz pro resetování hesla jíž není platný...">
        Odkaz, kterým se pokoušíte obnovit heslo již vypršel nebo je neplatný.
        Prosíme proto o vygenerování nového validního odkazu pro obnovení hesla.
      </ErrorTemplate>
    )
  }

  if (errorType === "blocked") {
    return (
      <ErrorTemplate title="Váš účet je zablokován...">
        Váš účet byl zablokován administrátorem z důvodu narušení podmínek
        chování na stránce. Pro odbokování účtu nebo získání konkretnějšího
        důvodů kontaktujte prosím adminstrátora.
      </ErrorTemplate>
    )
  }

  if (errorType === "logAgain") {
    return (
      <ErrorTemplate
        title="Potřebujeme aby jste se přihlásíl znova..."
        buttonText="Přihlásit se"
        buttonLink="/login"
      >
        Vypadá to ze jste tady moc dlouho a proto výpršela platnost vašeho
        přihlášení. Proto je potřeba aby jste se přihlásil znova.
      </ErrorTemplate>
    )
  }

  return <ErrorTemplate />
}

export default Error
