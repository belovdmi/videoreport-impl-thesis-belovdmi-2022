import {
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Typography,
} from "@mui/material"
import { useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { useRecoilValue, useSetRecoilState } from "recoil"
import BasicPageContainer from "../components/containers/basic-page-container"
import ForeignProfileDialog from "../components/dialogs/foreign-profile-dialog"
import BasicLabeledValue from "../components/format/basic-labeled-value"
import JobBottomContent from "../components/job-bottom-content"
import JobDetails from "../components/job-details"
import useAuthorizeUser from "../hooks/use-authorize-user"
import useLoadJob from "../hooks/use-load-job"
import { User } from "../requests/data-contracts"
import { aCleanQuery, aViewedJob } from "../state/global"

import getJobStatus from "../utils/job-status-to-cze"

/**
 * page will display job that logged user is participating in
 */
const Wrok = () => {
  const navigate = useNavigate()
  const [profileDialog, setProfileDialog] = useState<{
    open: boolean
    user: User | undefined
  }>({ open: false, user: undefined })
  const { jobId } = useParams()
  const viewedJob = useRecoilValue(aViewedJob)
  const setCleanQuery = useSetRecoilState(aCleanQuery)

  useAuthorizeUser()

  useLoadJob(Number(jobId))

  return (
    <BasicPageContainer>
      <Container maxWidth={"lg"}>
        <Box pt={13} pb={12}>
          <JobDetails
            data={viewedJob || {}}
            onClose={() => {
              setCleanQuery(false)
              navigate("/my-works")
            }}
          />
          <Divider sx={{ pt: 2, mb: 2 }} />
          <Grid
            container
            direction={{ xs: "column", sm: "row" }}
            justifyContent={"space-around"}
            textAlign={"center"}
            rowGap={2}
          >
            <BasicLabeledValue labelColor="primary" label={"Zadavatel práce:"}>
              <Button
                color={"secondary"}
                onClick={() =>
                  setProfileDialog({ open: true, user: viewedJob?.creator })
                }
                sx={{ textTransform: "none" }}
              >
                {viewedJob?.creator?.username || "-"}
              </Button>
            </BasicLabeledValue>
            <BasicLabeledValue labelColor="primary" label={"Na práci dělá:"}>
              {viewedJob?.fulfiller ? (
                <Button
                  color={"secondary"}
                  sx={{ textTransform: "none" }}
                  onClick={() =>
                    setProfileDialog({ open: true, user: viewedJob?.fulfiller })
                  }
                >
                  {viewedJob?.fulfiller.username}
                </Button>
              ) : (
                "-"
              )}
            </BasicLabeledValue>
            <BasicLabeledValue labelColor="primary" label={"Stav práce:"}>
              <Typography variant={"h6"}>
                {getJobStatus(viewedJob?.status)}
              </Typography>
            </BasicLabeledValue>
          </Grid>
          <Divider sx={{ pt: 2, mb: 2 }} />
          <JobBottomContent jobData={viewedJob} />
        </Box>
      </Container>

      <ForeignProfileDialog
        open={profileDialog.open}
        onClose={() => setProfileDialog({ open: false, user: undefined })}
        userData={profileDialog.user}
      />
    </BasicPageContainer>
  )
}

export default Wrok
