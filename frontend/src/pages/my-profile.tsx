import { Box, Divider, Grid, Stack, Typography } from "@mui/material"
import { useRecoilValue } from "recoil"
import BasicPageContainer from "../components/containers/basic-page-container"
import ProfileDetails from "../components/profile-details"
import ReviewsTable from "../components/tables/reviews-table"
import useAuthorizeUser from "../hooks/use-authorize-user"
import useLoadUser from "../hooks/use-load-user"
import { aLoggedUsername } from "../state/global"

const MyProfile = () => {
  const loggedUsername = useRecoilValue(aLoggedUsername)

  useAuthorizeUser()

  const userData = useLoadUser(loggedUsername)

  return (
    <BasicPageContainer>
      <Stack pt={12} pb={10}>
        <ProfileDetails data={userData} editButton />
        <Grid item xs={12} width={"100%"} m={"auto"}>
          <Divider sx={{ mb: 2.5 }} />
          <Typography variant="h4" pt={1} pb={5} sx={{ fontWeight: 500 }}>
            Recenze
          </Typography>
          <Box px={{ xs: 0, sm: 3 }} pb={3}>
            <ReviewsTable username={loggedUsername || "-"} />
          </Box>
        </Grid>
      </Stack>
    </BasicPageContainer>
  )
}

export default MyProfile
