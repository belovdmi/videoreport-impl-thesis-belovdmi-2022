/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  Box,
  Button,
  Drawer,
  Grid,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material"
import { useState } from "react"
import IconCard from "../components/cards/icon-card"
import NextAndPrevPageButtons from "../components/ui/buttons/next-and-prev-page-buttons"
import PageWithAsideContainer from "../components/containers/page-with-aside-container"
import MyWorkMenuAside from "../components/asides/my-work-menu-aside"
import useAuthorizeUser from "../hooks/use-authorize-user"
import { useRecoilState, useRecoilValue } from "recoil"
import { SortJobsBy } from "../requests/Jobs"
import { aLoggedUsername, aOffersQuery } from "../state/global"
import { useNavigate } from "react-router-dom"
import getJobStatus from "../utils/job-status-to-cze"
import useLoadJobs from "../hooks/use-load-jobs"
import InformationText from "../components/job-bottom-content/inforamtion-text"
import useLoadReviews from "../hooks/use-load-reviews"

/**
 * page will display all jobs that logged user is participating in
 */
const MyWorks = () => {
  const navidate = useNavigate()
  const [sortBy, setSortBy] = useState<SortJobsBy | "">("")
  const [openDrawer, setOpenDrawer] = useState(false)
  const [offersQuery, setOffersQuery] = useRecoilState(aOffersQuery)
  const loggedUsername = useRecoilValue(aLoggedUsername)

  useAuthorizeUser()

  const { jobs, totalCount } = useLoadJobs(true, "creator", "free")

  const handleChange = (event: SelectChangeEvent) => {
    const newSortBy = event.target.value as SortJobsBy
    setSortBy(newSortBy)

    const newOffersQuery = { ...offersQuery }
    if (newSortBy !== offersQuery.sortBy) {
      newOffersQuery!.sortBy = newSortBy
      newOffersQuery!.page = 1
      newOffersQuery!.count = 12
      setOffersQuery(newOffersQuery)
    }
  }

  const { reviews } = useLoadReviews(loggedUsername, true)

  return (
    <PageWithAsideContainer asideContent={<MyWorkMenuAside />}>
      <Grid item container minHeight={50} alignItems="center">
        <Grid xs item>
          <Typography variant={"h4"} color={"primary.main"}>
            {offersQuery?.role !== "creator" ? "Vzaté" : "Zadané"} -{" "}
            {getJobStatus(offersQuery?.status)}
          </Typography>
        </Grid>
        <Box>
          <Box pb={1} width={135} display={{ xs: "block", lg: "none" }}>
            <Button
              color="secondary"
              variant="outlined"
              sx={{
                fontWeight: 500,
                textTransform: "none",
                height: 30,
              }}
              size="small"
              fullWidth
              onClick={() => setOpenDrawer(true)}
            >
              Menu Prací
            </Button>
          </Box>
          <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={sortBy}
            variant="outlined"
            onChange={handleChange}
            sx={{
              width: 135,
              height: 30,
              textAlign: "center",
              fontSize: 13,
            }}
            displayEmpty
            MenuProps={{
              disableScrollLock: true,
            }}
            size="small"
          >
            <MenuItem value="">Řadit od</MenuItem>
            <MenuItem value={"price.asc"}>Nejnižší ceny</MenuItem>
            <MenuItem value={"price.desc"}>Nejvyšší ceny</MenuItem>
            <MenuItem value={"created.desc"}>Nejnovější</MenuItem>
            <MenuItem value={"created.asc"}>Nejstarší</MenuItem>
            <MenuItem value={"dueDate.asc"}>Končí nejdřív</MenuItem>
            <MenuItem value={"dueDate.desc"}>Končí nejpozdějí</MenuItem>
          </Select>
        </Box>
      </Grid>
      <Box minHeight={"65vh"}>
        <Grid item container pt={4} spacing={jobs && jobs?.length ? 3.5 : 0}>
          {jobs && jobs.length ? (
            jobs?.map(data => {
              return (
                <IconCard
                  onClick={() => {
                    navidate(`/my-works/${data.id?.toString() || 0}`)
                  }}
                  title={data.title || "Unknown"}
                  icon={data.category?.name}
                  dataList={[
                    data.status !== "finished"
                      ? { name: "Cena:", value: data.price + " Kč" }
                      : {
                          name: "Ohodnoceno:",
                          value:
                            reviews &&
                            reviews?.some(revData => {
                              return revData.jobId === data.id
                            })
                              ? "Ano"
                              : "Ne",
                        },
                    { name: "Datum splnění od:", value: data.dueDate || "-" },
                    {
                      name: "Identifikační číslo:",
                      value: data.id?.toString() || "-",
                    },
                  ]}
                />
              )
            })
          ) : (
            <Grid container justifyContent={"center"} pt={3}>
              <InformationText color={"secondary"}>
                Nic jsem nenašel...
              </InformationText>
            </Grid>
          )}
        </Grid>
      </Box>

      <NextAndPrevPageButtons
        prevDisabled={offersQuery.page === 1}
        nextDisabled={
          offersQuery?.page && offersQuery?.count
            ? offersQuery.page * offersQuery.count >= totalCount
            : true
        }
        onNextPage={() => {
          const newOffersQuery = { ...offersQuery }
          newOffersQuery!.page = (newOffersQuery?.page || 0) + 1
          setOffersQuery(newOffersQuery)
        }}
        onPrevPage={() => {
          const newOffersQuery = { ...offersQuery }
          newOffersQuery!.page = (newOffersQuery?.page || 2) - 1
          setOffersQuery(newOffersQuery)
        }}
      />
      <Box>
        <Drawer open={openDrawer} onClose={() => setOpenDrawer(false)}>
          <Box p={2}>
            <MyWorkMenuAside closeDrawer={() => setOpenDrawer(false)} />
          </Box>
        </Drawer>
      </Box>
    </PageWithAsideContainer>
  )
}

export default MyWorks
