import { Button, Grid, Paper, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import LoginRegisterCard from "../components/cards/login-register"
import LogoFooter from "../components/footer/logo-footer"
import CenterContentContainer from "../components/containers/center-content-container"
import { useForm } from "react-hook-form"
import { useSetRecoilState } from "recoil"
import {
  aAlert,
  aAuthToken,
  aErrorType,
  aLoading,
  aLogged,
  aLoggedUsername,
  aRefreshToken,
} from "../state/global"
import { LoginUserPayload } from "../requests/data-contracts"
import ControlledTextField from "../components/ui/text-field/controlled-text-field"
import { Users } from "../requests/Users"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import { useErrorHandler } from "react-error-boundary"

const Login = () => {
  const handleError = useErrorHandler()
  const { handleSubmit, control, reset } = useForm()
  const setLoading = useSetRecoilState(aLoading)
  const setRefreshToken = useSetRecoilState(aRefreshToken)
  const setAuthToken = useSetRecoilState(aAuthToken)
  const setLoggedUsername = useSetRecoilState(aLoggedUsername)
  const setLogged = useSetRecoilState(aLogged)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const navigate = useNavigate()

  const onSubmit = async (data: LoginUserPayload) => {
    try {
      setLoading(true)

      const api = new Users()

      const res = await api.loginUser(data)

      const { refreshToken, authToken } = res.data
      setRefreshToken(refreshToken || "")
      setAuthToken(authToken || "")
      setLoggedUsername(data.username || "")
      setLogged(true)
      navigate("/my-profile")
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 404) {
        setAlert({
          open: true,
          severity: "error",
          alertMsg: "Uživatelské jméno nebo heslo bylo nesprávné.",
        })
      } else {
        if (axios.isAxiosError(error) && error.response?.status === 403) {
          setErrorType("blocked")
        }

        handleError(error)
      }
    } finally {
      reset(undefined, {
        keepValues: false,
      })
      setLoading(false)
    }
  }

  return (
    <CenterContentContainer>
      <Paper elevation={10} sx={{ width: "100%" }}>
        <Grid container width={"100%"}>
          <Grid item p={5} xs={12} md={6} width={"100%"}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid
                item
                width={"100%"}
                container
                direction="column"
                justifyContent={"center"}
              >
                <Typography component={"div"} fontSize={32} pb={2}>
                  Přihlašování
                </Typography>
                <Typography fontSize={13}>
                  Přihlašte se ke svému účtu
                </Typography>
                <ControlledTextField
                  name="username"
                  label="Uživatelské jméno"
                  type="text"
                  control={control}
                  sx={{ my: 2 }}
                  rules={{
                    required: "Toto pole je povinné.",
                  }}
                />
                <ControlledTextField
                  name="password"
                  label="Heslo"
                  type="password"
                  control={control}
                  sx={{ pb: 4 }}
                  rules={{
                    required: "Toto pole je povinné.",
                  }}
                />
                <Grid item container alignItems={"center"}>
                  <Grid item xs>
                    <Button variant="outlined" type="submit">
                      Přihlásit se
                    </Button>
                  </Grid>
                  <Grid item xs textAlign={"end"}>
                    <Button
                      component={Link}
                      to={"/forgot"}
                      color={"error"}
                      size="small"
                    >
                      Zapomněl jsem heslo!
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </Grid>

          <LogoFooter hideOnXs />
          <Grid md={6} item display={{ xs: "none", md: "block" }}>
            <LoginRegisterCard onlyRegister />
          </Grid>
        </Grid>
      </Paper>
    </CenterContentContainer>
  )
}

export default Login
