import { Box, Button, Paper, Grid, Typography } from "@mui/material"
import LoginRegisterCard from "../components/cards/login-register"

import LogoFooter from "../components/footer/logo-footer"
import CenterContentContainer from "../components/containers/center-content-container"
import ControlledTextField from "../components/ui/text-field/controlled-text-field"
import { ForgotPasswordPayload } from "../requests/data-contracts"
import axios from "axios"
import { useForm } from "react-hook-form"
import { useSetRecoilState } from "recoil"
import { Users } from "../requests/Users"
import { aAlert, aLoading } from "../state/global"
import { useErrorHandler } from "react-error-boundary"

const FrogotPassword = () => {
  const handleError = useErrorHandler()
  const { handleSubmit, control, reset } = useForm()
  const setLoading = useSetRecoilState(aLoading)
  const setAlert = useSetRecoilState(aAlert)

  const onSubmit = async (data: ForgotPasswordPayload) => {
    try {
      reset(undefined, {
        keepValues: false,
      })
      setLoading(true)
      const api = new Users()
      await api.forgotPassword(data)
      setAlert({
        open: true,
        severity: "success",
        alertMsg: "Email na obnovení byl uspěšné odeslán !",
      })
    } catch (error) {
      if (
        axios.isAxiosError(error) &&
        (error.response?.status === 404 || error.response?.status === 400)
      ) {
        return
      }

      handleError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <CenterContentContainer>
      <Paper elevation={10} sx={{ width: "100%" }}>
        <Grid container width={"100%"}>
          <Grid
            item
            xs
            p={5}
            width={"100%"}
            container
            direction="column"
            justifyContent={"center"}
          >
            <Typography component={"div"} fontSize={32} pb={2}>
              Zapomenuté heslo
            </Typography>
            <Typography fontSize={13}>
              Zadajte váš email pro obnovení hesla:
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid
                width={"100%"}
                container
                direction="column"
                justifyContent={"center"}
              >
                <ControlledTextField
                  name="email"
                  label="Email"
                  type="text"
                  control={control}
                  sx={{ mt: 1, pb: 4 }}
                  rules={{
                    required: "Toto pole je povinné.",
                    pattern: {
                      /// to ignore: no-useless-escape
                      // eslint-disable-next-line
                      value: /^\w[\w + \.]*\w+@\w+\.[a-z + \.]*[a-z]$/g,
                      message: "Nesprávný formát emailu.",
                    },
                  }}
                />
                <Box>
                  <Button variant="outlined" size="large" type="submit">
                    Odeslat
                  </Button>
                </Box>
              </Grid>
            </form>
          </Grid>
          <LogoFooter hideOnXs />
          <Grid md={6} display={{ xs: "none", md: "block" }} item>
            <LoginRegisterCard />
          </Grid>
        </Grid>
      </Paper>
    </CenterContentContainer>
  )
}

export default FrogotPassword
