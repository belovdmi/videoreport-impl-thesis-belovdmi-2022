import {
  MonetizationOnRounded,
  TimerRounded,
  VideoLibraryRounded,
} from "@material-ui/icons"
import { Box, Button, Divider, Grid, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import Car from "../assets/car.png"
import IconArticle from "../components/format/icon-article"
import BasicPageContainer from "../components/containers/basic-page-container"

const About = () => {
  return (
    <BasicPageContainer>
      <Box pt={13} pb={{ xs: 14, md: 7 }}>
        <Grid container alignItems={"center"}>
          <Grid xs={12} md={8} item pb={4}>
            <Typography variant="h3" mb={{ xs: -2, md: 4 }}>
              Jednoduchy nástroj který setří čas...
            </Typography>
            <Box
              display={{ xs: "block", md: "none" }}
              maxWidth={350}
              p={5}
              m={"auto"}
            >
              <img src={Car} alt="car-big" width={"100%"} height={"100%"} />
            </Box>
            <Typography variant="body1" lineHeight={2} mb={4}>
              Nemáte čas řešit osodní prohlídky ? Nebo byste naopak chtěl
              vydělat jejich návštěvou ? S obojím pomůže <b>Zchecknito</b> !
              Webový portál, který spojuje lidi s pořebou náhrady účasti na
              osobní prohlídce a lidi, kteří si chtěji ve volném čase přivydělat
              jejich návštěvou.
            </Typography>
            <Typography textAlign={{ xs: "center", md: "left" }}>
              <Button
                component={Link}
                to={"/register"}
                variant="outlined"
                size="large"
                sx={{ fontSize: { xs: 16, md: 18 } }}
              >
                Vyzkoušet
              </Button>
            </Typography>
          </Grid>
          <Grid
            xs={4}
            item
            container
            justifyContent={"end"}
            display={{ xs: "none", md: "block" }}
          >
            <Box p={2} maxWidth={300} m={"auto"}>
              <img src={Car} alt="car-small" width={"100%"} height={"100%"} />
            </Box>
          </Grid>
        </Grid>

        <Divider sx={{ pt: 1, mb: 2 }} />
        <Typography py={4} variant="h3" textAlign={"center"}>
          Jak to celé funguje ?
        </Typography>

        <Grid
          container
          justifyContent={"center"}
          columnGap={{ xs: 0, md: 10, lg: 20 }}
          rowGap={3}
        >
          <Grid item xs={12} md>
            <IconArticle
              Icon={<TimerRounded fontSize="inherit" />}
              title="Vytvoří se práce"
            >
              <br />
              Nějdřive se vytvoří práce, ve kterém bude uvedeno čeho se týká.
              Hlavní je uvěst adresu kde se bude osobní prohlídka konat a odkaz
              na inzerci, který se jí tyká.
            </IconArticle>
          </Grid>
          <Grid item xs={12} md>
            <IconArticle
              Icon={<VideoLibraryRounded fontSize="inherit" />}
              title="Nahrají se vysledky"
            >
              <br />
              Zvolený zájemce pak navštíví osobní prohlídku a pořídí z ní video.
              To následně připevní k práci a odešle ho. Video si budete moc v
              práci následně přehrát.
            </IconArticle>
          </Grid>
          <Grid item xs={12} md>
            <IconArticle
              Icon={<MonetizationOnRounded fontSize="inherit" />}
              title="Zaplatí se náhradníkovi"
            >
              <br />
              Za splněnou práci se následně zaplatí jejímu plniteli cena
              stanovená při zadání práce. Zavěrem se zadavatel a plnitel práce
              vzájemně ohodnotí podle spokojenosti se spoluprácí.
            </IconArticle>
          </Grid>
        </Grid>
      </Box>
    </BasicPageContainer>
  )
}

export default About
