import { Box, Divider, Button, Container } from "@mui/material"
import IconCard from "../components/cards/icon-card"
import BasicPageContainer from "../components/containers/basic-page-container"
import BasicButtonsContainer from "../components/containers/basic-buttons-container"
import BasicTitledContent from "../components/format/basic-titled-content"
import JobDetails from "../components/job-details"
import { useNavigate, useParams } from "react-router-dom"
import { securedJob } from "../requests/Jobs"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import {
  aAlert,
  aAuthToken,
  aErrorType,
  aLoading,
  aLogged,
  aRefreshToken,
} from "../state/global"
import { useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import axios from "axios"
import ForeignProfileDialog from "../components/dialogs/foreign-profile-dialog"
import { execRefreshToken } from "../requests/refresh-token"
import getSpecializationsNames from "../utils/get-secializations-names"
import useLoadJob from "../hooks/use-load-job"

const Offer = () => {
  const handleError = useErrorHandler()
  const navigate = useNavigate()
  const refreshToken = useRecoilValue(aRefreshToken)
  const setErrorType = useSetRecoilState(aErrorType)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const setLoading = useSetRecoilState(aLoading)
  const setAlert = useSetRecoilState(aAlert)
  const logged = useRecoilValue(aLogged)
  const [openProfileDialog, setOpenProfileDialog] = useState(false)
  const { jobId } = useParams()

  const { job, creator } = useLoadJob(Number(jobId) || -1)

  const subscribeJob = async () => {
    try {
      setLoading(true)
      const newAuthToken = await execRefreshToken({
        authToken,
        refreshToken,
      })
      setAuthToken(newAuthToken)

      const api = securedJob
      api.setSecurityData(newAuthToken)

      await api.subscribe(Number(jobId) || -1)
      setAlert({
        alertMsg:
          "Žádost o plnění práce byla uspěšně odeslána. Naleznete jí v sekci Vzaté práce kategorii Odebírané.",
        severity: "success",
        open: true,
      })
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 400) {
        setAlert({
          alertMsg:
            "Žádost o plnění práce může podat pouze uživatel, který se jí neůčastní nebo jí již neodebírá.",
          severity: "error",
          open: true,
        })
        return
      }

      if (
        error instanceof Error &&
        error?.message === "refresh token expired"
      ) {
        setErrorType("logAgain")
      }

      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("forbidden")
      }

      handleError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <BasicPageContainer>
      <Container maxWidth={"lg"}>
        <Box pt={13} pb={12}>
          <JobDetails
            data={job || {}}
            onClose={() => {
              navigate("/offers")
            }}
          />
          <Divider sx={{ pb: 2, mb: 2 }} />
          <BasicTitledContent title={"Zadavatel:"}>
            <IconCard
              title={creator?.username || "Unknown"}
              icon="profile"
              dataList={[
                {
                  name: "Specilizace:",
                  value:
                    getSpecializationsNames(creator?.specializations) || "-",
                },
                {
                  name: "Skóre:",
                  value: creator?.scoreSummary?.score?.toString() || "-",
                },
                {
                  name: "Počet hodnocení",
                  value:
                    creator?.scoreSummary?.numberOfReviews?.toString() || "-",
                },
              ]}
              onClick={() => setOpenProfileDialog(true)}
            />
          </BasicTitledContent>
          <Divider sx={{ pt: 2, mb: 2 }} />
          <Box py={"4%"}>
            <BasicButtonsContainer>
              <Button
                variant={"contained"}
                disabled={!logged}
                onClick={subscribeJob}
              >
                Odeslat žádost o plnění
              </Button>
            </BasicButtonsContainer>
          </Box>
        </Box>
      </Container>

      <ForeignProfileDialog
        open={openProfileDialog}
        onClose={() => setOpenProfileDialog(false)}
        userData={creator}
      />
    </BasicPageContainer>
  )
}

export default Offer
