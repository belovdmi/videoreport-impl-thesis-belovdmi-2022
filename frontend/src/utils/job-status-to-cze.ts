import { Status } from "../requests/Jobs"

/**
 * translates job status to czech
 * @param  {Status} status is status of job
 * @return {string} returns czech name for status (- if undefined)
 */
const getJobStatus = (status?: Status) => {
  switch (status) {
    case "free":
      return "Volná"
    case "assigned":
      return "Přiřazená"
    case "in_progress":
      return "Probíhající"
    case "finished":
      return "Ukončená"
    default:
      return "-"
  }
}

export default getJobStatus
