/**
 * gets array of specialization names from objects
 * @param  {Status} array is array of specialization objects
 * @return {string} returns array of specialization names
 */
const getSpecializationsNames = (array?: { id?: number; name?: string }[]) => {
  if (!array) {
    return array
  }
  const names: string[] = []

  for (const item of array) {
    names.push(item?.name || "")
  }

  return names.join(", ")
}

export default getSpecializationsNames
