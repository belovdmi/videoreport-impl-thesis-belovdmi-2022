/**
 * gets region name from abbreviation
 * @param  {string} regionAbbreviation abbreviation for region
 * @return {string} if abbreviation is valid returns full name otherwise returns "-"
 */
const regionNameFromAbbreviation = (regionAbbreviation?: string) => {
  switch (regionAbbreviation) {
    case "PHA":
      return "Hlavní město Praha"
    case "STČ":
      return "Středočeský kraj"
    case "JHČ":
      return "Jihočeský kraj"
    case "PLK":
      return "Plzeňský kraj"
    case "KVK":
      return "Karlovarský kraj"
    case "ULK":
      return "Ústecký kraj"
    case "LBK":
      return "Liberecký kraj"
    case "HKK":
      return "Královéhradecký kraj"
    case "PAK":
      return "Pardubický kraj"
    case "VYS":
      return "Kraj Vysočina"
    case "JHM":
      return "Jihomoravský kraj"
    case "OLK":
      return "Olomoucký kraj"
    case "MSK":
      return "Moravskoslezský kraj"
    case "ZLK":
      return "Zlínský kraj"
    default:
      return "-"
  }
}

export default regionNameFromAbbreviation
