import { ThemeProvider } from "@mui/material"
import { StrictMode } from "react"
import { render } from "react-dom"
import { RecoilRoot } from "recoil"
import App from "./app"
import reportWebVitals from "./reportWebVitals"

import "./styles/index.css"
import "./styles/video-react.css"
import zchecknitoTheme from "./themes/zchecknito-theme"

const renderApp = () =>
  render(
    <StrictMode>
      <RecoilRoot>
        <ThemeProvider theme={zchecknitoTheme}>
          <App />
        </ThemeProvider>
      </RecoilRoot>
    </StrictMode>,
    document.getElementById("root")
  )

;(() => {
  renderApp()
})()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
