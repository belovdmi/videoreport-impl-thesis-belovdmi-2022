import { createTheme, responsiveFontSizes } from "@mui/material"

const zchecknitoTheme = responsiveFontSizes(
  createTheme({
    typography: {
      fontFamily: "Nunito , sans-serif",
    },
    palette: {
      primary: {
        main: "#b764a4",
        contrastText: "#FBF7FF",
      },
      secondary: {
        main: "#52424D",
      },
    },

    components: {
      MuiButton: {
        styleOverrides: {
          root: {
            fontWeight: 600,
          },
        },
      },
    },
  })
)

export default zchecknitoTheme
