const { createProxyMiddleware } = require("http-proxy-middleware")

// change backend to localhost if not running in docker-compose
module.exports = function proxy(App) {
  App.use(
    "/api/",
    createProxyMiddleware({
      pathRewrite: { "^/api/": "/api/v1/" },
      target: "https://backend:3000/",
      changeOrigin: true,
      secure: false,
      logLevel: "debug",
    })
  )
}
