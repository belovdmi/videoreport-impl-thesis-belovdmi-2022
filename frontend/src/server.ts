import { createServer } from "miragejs"
import { AnyFactories, AnyModels, Registry } from "miragejs/-types"
import { Server } from "miragejs/server"

type Options = {
  environment: "test" | "development"
}

interface MakeServer {
  (options: Options): Server<Registry<AnyModels, AnyFactories>>
}

// can be used as mock server if needed
const makeServer: MakeServer = ({ environment = "test" }) => {
  return createServer({
    environment,

    routes() {
      // routes goes here
    },
  })
}

export default makeServer
