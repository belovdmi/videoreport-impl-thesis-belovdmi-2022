import { Button, Grid, Typography } from "@mui/material"
import axios from "axios"
import { useErrorHandler } from "react-error-boundary"
import { useForm } from "react-hook-form"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import { ChangePasswordPayload } from "../../requests/data-contracts"
import { execRefreshToken } from "../../requests/refresh-token"
import { securedUser } from "../../requests/Users"
import {
  aLoggedUsername,
  aAuthToken,
  aLoading,
  aRefreshToken,
  aErrorType,
  aAlert,
} from "../../state/global"
import ControlledTextField from "../ui/text-field/controlled-text-field"

const ChangePasswordFrom = () => {
  const handleError = useErrorHandler()
  const setErrorType = useSetRecoilState(aErrorType)
  const { handleSubmit, control, watch, reset } = useForm()
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const setLoading = useSetRecoilState(aLoading)
  const setAlert = useSetRecoilState(aAlert)

  const onSubmit = async (data: ChangePasswordPayload) => {
    try {
      setLoading(true)
      const newAuthToken = await execRefreshToken({ authToken, refreshToken })
      setAuthToken(newAuthToken)

      const api = securedUser
      api.setSecurityData(newAuthToken)

      await api.changePassword(loggedUsername, {
        oldPassword: data.oldPassword,
        newPassword: data.newPassword,
      })
      setAlert({
        open: true,
        alertMsg: "Heslo bylo úspěšně změněno.",
        severity: "success",
      })
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 400) {
        setAlert({
          open: true,
          alertMsg: "Staré heslo není správně. Zkuste to prosím znovu.",
          severity: "error",
        })
      }

      if (
        error instanceof Error &&
        error?.message === "refresh token expired"
      ) {
        setErrorType("logAgain")
      }

      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("forbidden")
      }

      handleError(error)
    } finally {
      reset(undefined, {
        keepValues: false,
      })
      setLoading(false)
    }
  }

  const watchPassword = watch("newPassword", "")
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid xs item container rowGap={1.5} direction={"column"}>
        <Typography variant="h6" fontWeight={600} mb={-1}>
          Změna hesla:
        </Typography>
        <ControlledTextField
          name="oldPassword"
          label="Staré heslo"
          type="password"
          control={control}
          rules={{
            required: "Toto pole je povinné.",
          }}
        />
        <ControlledTextField
          name="newPassword"
          label="Nové heslo"
          type="password"
          control={control}
          rules={{
            required: "Toto pole je povinné.",
            validate: {
              longEnough: (data: string) =>
                (data && data?.length >= 8) ||
                "Heslo musí mít alespoň 8 znaků.",
              hasCapital: (data: string) =>
                /[A-Z]+/g.test(data ?? "") ||
                "Heslo musí obsahovat alespoň jedno velké písmeno.",
              hasNumber: (data: string) =>
                /[0-9]+/g.test(data ?? "") ||
                "Heslo musí obsahovat alespoň jedno číslo.",
            },
          }}
        />
        <ControlledTextField
          name="passwordRepeat"
          label="Zopakované nové heslo"
          type="password"
          control={control}
          rules={{
            required: "Toto pole je povinné.",
            validate: {
              passwordConfirm: (data: string) =>
                watchPassword === data || "Zopakované heslo se neshoduje.",
            },
          }}
        />
        <Button variant={"contained"} sx={{ mt: 1.5 }} fullWidth type="submit">
          Změnit heslo
        </Button>
      </Grid>
    </form>
  )
}

export default ChangePasswordFrom
