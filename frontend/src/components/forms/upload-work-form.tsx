import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material"
import { useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import { CreateJobPayload } from "../../requests/data-contracts"
import { execRefreshToken } from "../../requests/refresh-token"
import {
  aAlert,
  aAuthToken,
  aErrorType,
  aLoading,
  aLoggedUsername,
  aOffersQuery,
  aRefreshToken,
} from "../../state/global"
import BasicButtonsContainer from "../containers/basic-buttons-container"
import ControlledTextField from "../ui/text-field/controlled-text-field"
import { securedJob } from "../../requests/Jobs"
import axios from "axios"
import { useErrorHandler } from "react-error-boundary"

type UploadWorkFromProps = {
  onSuccess: () => void
}

const UploadWorkFrom = ({ onSuccess }: UploadWorkFromProps) => {
  const handleError = useErrorHandler()
  const { handleSubmit, control } = useForm()
  const [region, setRegion] = useState<string>("PHA")
  const handleRegionChange = (event: SelectChangeEvent<typeof region>) => {
    const {
      target: { value },
    } = event
    setRegion(value)
  }
  const refreshToken = useRecoilValue(aRefreshToken)
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const setOffersQuery = useSetRecoilState(aOffersQuery)

  const onSubmit = async (data: CreateJobPayload) => {
    try {
      setLoading(true)
      const newAuthToken = await execRefreshToken({ authToken, refreshToken })
      setAuthToken(newAuthToken)

      const api = securedJob
      api.setSecurityData(newAuthToken)

      await api.createJob({ ...data, region })
      onSuccess()
      setAlert({
        alertMsg:
          "Práce byla úspěšně vytvořená. Naleznete jí v sekci Zadané práce kategorii Volné.",
        severity: "success",
        open: true,
      })
      setOffersQuery({
        sortBy: "created.desc",
        page: 1,
        count: 12,
        priceGte: undefined,
        priceLte: undefined,
        dueDateGte: undefined,
        dueDateLte: undefined,
        region: undefined,
        username: loggedUsername,
        category: undefined,
        role: "creator",
        status: "free",
      })
    } catch (error) {
      if (
        error instanceof Error &&
        error?.message === "refresh token expired"
      ) {
        setErrorType("logAgain")
      }

      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("forbidden")
      }

      handleError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container columnGap={6}>
        <Grid xs={12} md item container rowGap={2}>
          <ControlledTextField
            name="title"
            label="Název"
            type="text"
            control={control}
            rules={{
              maxLenght: { value: 50, message: "Překračuje povolenou délku." },
              required: "Toto pole je povinné.",
            }}
            fullWidth
          />
          <ControlledTextField
            name="price"
            label="Cena"
            type="number"
            control={control}
            rules={{
              min: { value: 1, message: "Minimální cena je 1." },
              max: { value: 10000, message: "Maximální cena je 10 000." },
              required: "Toto pole je povinné.",
            }}
            fullWidth
          />
          <ControlledTextField
            name="phoneNumber"
            label="Telefoní číslo"
            type="text"
            control={control}
            rules={{
              maxLength: {
                value: 13,
                message: "Překračuje povolenou délku.",
              },
              minLength: {
                value: 9,
                message: "Nesprávný formát telefoního čísla.",
              },
              pattern: {
                value: /^[+]?[0-9]*$/g,
                message: "Nesprávný formát telefoního čísla.",
              },
            }}
            fullWidth
          />
          <ControlledTextField
            name="email"
            label="Email"
            type="text"
            control={control}
            rules={{
              required: "Toto pole je povinné.",
              maxLenght: { value: 100, message: "Překračuje povolenou délku." },
              pattern: {
                /// to ignore: no-useless-escape
                // eslint-disable-next-line
                value: /^\w[\w + \.]*\w+@\w+\.[a-z + \.]*[a-z]$/g,
                message: "Nesprávný formát emailu.",
              },
            }}
            fullWidth
          />
          <ControlledTextField
            name="dueDate"
            label="Do"
            type="date"
            sx={{ width: "100%" }}
            control={control}
            rules={{
              validate: {
                value: (data: string) => {
                  const date = new Date()
                  const dataDate = new Date(data)
                  if (dataDate.getFullYear() >= 2050) {
                    return "Datum není v dovoleným rozsahu."
                  }

                  if (
                    dataDate.getFullYear() > date.getFullYear() ||
                    (dataDate.getFullYear() === date.getFullYear() &&
                      dataDate.getMonth() >= date.getMonth()) ||
                    (dataDate.getFullYear() === date.getFullYear() &&
                      dataDate.getMonth() === date.getMonth() &&
                      dataDate.getDay() >= date.getDay())
                  ) {
                    return
                  }
                  return "Datum není v dovoleným rozsahu."
                },
              },
              required: "Toto pole je povinné.",
            }}
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
          />
        </Grid>
        <Grid xs item container rowGap={2}>
          <ControlledTextField
            name="address"
            label="Adresa"
            type="text"
            control={control}
            rules={{
              maxLenght: { value: 150, message: "Překračuje povolenou délku." },
              required: "Toto pole je povinné.",
            }}
            fullWidth
          />
          <ControlledTextField
            name="town"
            label="Město"
            type="text"
            control={control}
            rules={{
              maxLenght: { value: 150, message: "Překračuje povolenou délku." },
              required: "Toto pole je povinné.",
            }}
            fullWidth
          />
          <FormControl variant="standard" sx={{ width: "100%" }}>
            <InputLabel id="demo-simple-select-helper-label">Kraj</InputLabel>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              variant="standard"
              value={region}
              onChange={handleRegionChange}
              label={"Kraj"}
              MenuProps={{
                disableScrollLock: true,
              }}
            >
              <MenuItem value={"PHA"}>Hlavní město Praha</MenuItem>
              <MenuItem value={"STČ"}>Středočeský kraj</MenuItem>
              <MenuItem value={"JHČ"}>Jihočeský kraj</MenuItem>
              <MenuItem value={"PLK"}>Plzeňský kraj</MenuItem>
              <MenuItem value={"KVK"}>Karlovarský kraj</MenuItem>
              <MenuItem value={"ULK"}>Ústecký kraj</MenuItem>
              <MenuItem value={"LBK"}>Liberecký kraj</MenuItem>
              <MenuItem value={"HKK"}>Královéhradecký kraj</MenuItem>
              <MenuItem value={"PAK"}>Pardubický kraj</MenuItem>
              <MenuItem value={"VYS"}>Kraj Vysočina</MenuItem>
              <MenuItem value={"JHM"}>Jihomoravský kraj</MenuItem>
              <MenuItem value={"OLK"}>Olomoucký kraj</MenuItem>
              <MenuItem value={"MSK"}>Moravskoslezský kraj </MenuItem>
              <MenuItem value={"ZLK"}>Zlínský kraj</MenuItem>
            </Select>
          </FormControl>
          <ControlledTextField
            name="advertisementLink"
            label="Odkaz na inzerát"
            type="text"
            control={control}
            rules={{
              maxLenght: { value: 100, message: "Překračuje povolenou délku." },
            }}
            fullWidth
          />
          <Box width={"100%"}>
            <Typography variant="subtitle2" component={"div"}>
              Kategorie:
            </Typography>
            <Controller
              rules={{ required: true }}
              control={control}
              defaultValue={"Ostatní"}
              name="category"
              render={({ field }) => (
                <RadioGroup
                  {...field}
                  sx={{ display: "flex", justifyContent: "space-between" }}
                  row
                >
                  <FormControlLabel
                    value="Nemovitosti"
                    control={<Radio />}
                    label="Nemovitosti"
                  />
                  <FormControlLabel
                    value="Auta"
                    control={<Radio />}
                    label="Auta"
                  />
                  <FormControlLabel
                    value="Ostatní"
                    control={<Radio />}
                    label="Ostatní"
                  />
                </RadioGroup>
              )}
            />
          </Box>
        </Grid>
      </Grid>
      <Box>
        <ControlledTextField
          name="description"
          label="Popis"
          variant="outlined"
          type="text"
          multiline
          control={control}
          fullWidth
          rows={4}
          rules={{
            maxLenght: { value: 250, message: "Překračuje povolenou délku." },
          }}
          sx={{ mt: 3, mb: 8 }}
        />
      </Box>
      <BasicButtonsContainer>
        <Button variant="contained" type="submit">
          Vytvořit
        </Button>
      </BasicButtonsContainer>
    </form>
  )
}

export default UploadWorkFrom
