/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  Stack,
  Typography,
  Select,
  RadioGroup,
  FormControlLabel,
  Radio,
  Box,
  Button,
  SelectChangeEvent,
  FormControl,
  InputLabel,
  MenuItem,
} from "@mui/material"
import { useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { useRecoilState } from "recoil"
import { GetJobQuery } from "../../requests/Jobs"
import { aOffersQuery } from "../../state/global"
import ControlledTextField from "../ui/text-field/controlled-text-field"

type OffersFilterAsideProps = {
  afterSubmit?: () => void
}

const OffersFilterAside = ({ afterSubmit }: OffersFilterAsideProps) => {
  const { handleSubmit, control } = useForm()
  const [offersQuery, setOffersQuery] = useRecoilState(aOffersQuery)
  const [regions, setRegions] = useState<string[]>(offersQuery?.region || [])
  const handleChange = (event: SelectChangeEvent<typeof regions>) => {
    const {
      target: { value },
    } = event
    setRegions(typeof value === "string" ? value.split(",") : value)
  }

  const onSubmit = async (data: GetJobQuery) => {
    const newOffersQuery = { ...offersQuery }

    newOffersQuery!.region = regions.length ? regions : undefined
    newOffersQuery!.dueDateGte = data.dueDateGte || undefined
    newOffersQuery!.dueDateLte = data.dueDateLte || undefined
    newOffersQuery!.priceGte = data.priceGte || undefined
    newOffersQuery!.priceLte = data.priceLte || undefined
    newOffersQuery!.category =
      data.category === "all" ? undefined : data.category

    setOffersQuery(newOffersQuery)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing={1}>
        <Typography fontWeight={500}>Místo</Typography>
        <FormControl variant="standard" sx={{ m: 1, width: "184px" }}>
          <InputLabel id="demo-simple-select-helper-label">Kraj</InputLabel>
          <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            variant="standard"
            value={regions}
            onChange={handleChange}
            label={"Kraj"}
            multiple
            MenuProps={{
              disableScrollLock: true,
            }}
          >
            <MenuItem value={""}>Kraj</MenuItem>
            <MenuItem value={"PHA"}>Hlavní město Praha</MenuItem>
            <MenuItem value={"STČ"}>Středočeský kraj</MenuItem>
            <MenuItem value={"JHČ"}>Jihočeský kraj</MenuItem>
            <MenuItem value={"PLK"}>Plzeňský kraj</MenuItem>
            <MenuItem value={"KVK"}>Karlovarský kraj</MenuItem>
            <MenuItem value={"ULK"}>Ústecký kraj</MenuItem>
            <MenuItem value={"LBK"}>Liberecký kraj</MenuItem>
            <MenuItem value={"HKK"}>Královéhradecký kraj</MenuItem>
            <MenuItem value={"PAK"}>Pardubický kraj</MenuItem>
            <MenuItem value={"VYS"}>Kraj Vysočina</MenuItem>
            <MenuItem value={"JHM"}>Jihomoravský kraj</MenuItem>
            <MenuItem value={"OLK"}>Olomoucký kraj</MenuItem>
            <MenuItem value={"MSK"}>Moravskoslezský kraj </MenuItem>
            <MenuItem value={"ZLK"}>Zlínský kraj</MenuItem>
          </Select>
        </FormControl>
        <Typography variant="body1" fontWeight={500} pt={2}>
          Cena
        </Typography>
        <ControlledTextField
          name="priceGte"
          label="Od"
          type="number"
          defaultValue={offersQuery?.priceGte?.toString()}
          sx={{ width: "100%" }}
          rules={{ min: { value: 1, message: "Minimální cena je 1." } }}
          control={control}
          fullWidth
        />
        <ControlledTextField
          name="priceLte"
          label="Do"
          type="number"
          defaultValue={offersQuery?.priceLte?.toString()}
          sx={{ width: "100%" }}
          rules={{ min: { value: 1, message: "Minimální cena je 1." } }}
          control={control}
          fullWidth
        />
        <Typography variant="body1" fontWeight={500} py={2}>
          Končí
        </Typography>
        <ControlledTextField
          name="dueDateGte"
          label="Od"
          type="date"
          defaultValue={offersQuery?.dueDateGte?.toString()}
          sx={{ width: "100%" }}
          control={control}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />
        <ControlledTextField
          name="dueDateLte"
          label="Do"
          type="date"
          defaultValue={offersQuery?.dueDateLte?.toString()}
          sx={{ width: "100%" }}
          control={control}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />
        <Typography variant="body1" fontWeight={500} py={2}>
          Kategorie
        </Typography>
        <Controller
          control={control}
          defaultValue={offersQuery?.category || "all"}
          name="category"
          render={({ field }) => (
            <RadioGroup {...field} sx={{ pb: 6 }}>
              <FormControlLabel value="all" control={<Radio />} label="Vše" />
              <FormControlLabel
                value="Nemovitosti"
                control={<Radio />}
                label="Nemovitosti"
              />
              <FormControlLabel value="Auta" control={<Radio />} label="Auta" />
              <FormControlLabel
                value="Ostatní"
                control={<Radio />}
                label="Ostatní"
              />
            </RadioGroup>
          )}
        />
      </Stack>
      <Box textAlign={"center"}>
        <Button variant="outlined" onClick={afterSubmit} type="submit">
          Filtrovat
        </Button>
      </Box>
    </form>
  )
}

export default OffersFilterAside
