/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { ExpandLess, ExpandMore } from "@material-ui/icons"
import { Collapse, List, ListItemButton, ListItemText } from "@mui/material"
import { useState } from "react"
import { useRecoilState } from "recoil"
import { Role, Status } from "../../requests/Jobs"
import { aOffersQuery } from "../../state/global"
import CreateOfferDialog from "../dialogs/create-offer-dialog"

type MyWorkMenuAsideProps = {
  closeDrawer?: () => void
}

const MyWorkMenuAside = ({ closeDrawer }: MyWorkMenuAsideProps) => {
  const [openUpper, setOpenUpper] = useState(true)
  const [openLower, setOpenLower] = useState(true)
  const [openCreateWork, setOpenCreateWork] = useState(false)
  const [offersQuery, setOffersQuery] = useRecoilState(aOffersQuery)

  const handleClickUpper = () => {
    setOpenUpper(!openUpper)
  }

  const handleClickLower = () => {
    setOpenLower(!openLower)
  }

  const switchStatusAndRole = (newStatus: Status, newRole: Role) => {
    const newOffersQuery = { ...offersQuery }
    if (newStatus !== offersQuery.status || newRole !== offersQuery.role) {
      newOffersQuery!.status = newStatus
      newOffersQuery!.role = newRole
      newOffersQuery!.page = 1
      newOffersQuery!.count = 12
      setOffersQuery(newOffersQuery)
    }

    if (closeDrawer) {
      closeDrawer()
    }
  }

  const setColor = (jobState: string, role: string) =>
    jobState === offersQuery.status && role === offersQuery.role
      ? "primary.main"
      : undefined

  return (
    <>
      <List
        sx={{
          width: "100%",
          minWidth: 180,
          maxWidth: 360,
          bgcolor: "background.paper",
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        <ListItemButton
          onClick={handleClickUpper}
          sx={{ color: "primary.main" }}
        >
          <ListItemText primary="Zadané práce" />
          {openUpper ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openUpper} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("free", "creator"),
              }}
              onClick={() => switchStatusAndRole("free", "creator")}
            >
              <ListItemText primary="Volné" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("assigned", "creator"),
              }}
              onClick={() => switchStatusAndRole("assigned", "creator")}
            >
              <ListItemText primary="Přiřazené" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("in_progress", "creator"),
              }}
              onClick={() => switchStatusAndRole("in_progress", "creator")}
            >
              <ListItemText primary="Probíhající" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("finished", "creator"),
              }}
              onClick={() => switchStatusAndRole("finished", "creator")}
            >
              <ListItemText primary="Ukončené" />
            </ListItemButton>
          </List>
        </Collapse>
        <ListItemButton
          onClick={handleClickLower}
          sx={{ color: "primary.main" }}
        >
          <ListItemText primary="Vzaté práce" />
          {openLower ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openLower} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("free", "subscriber"),
              }}
              onClick={() => switchStatusAndRole("free", "subscriber")}
            >
              <ListItemText primary="Odebírané" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("assigned", "assigned"),
              }}
              onClick={() => switchStatusAndRole("assigned", "assigned")}
            >
              <ListItemText primary="K přijetí" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("in_progress", "fulfiller"),
              }}
              onClick={() => switchStatusAndRole("in_progress", "fulfiller")}
            >
              <ListItemText primary="Probíhající" />
            </ListItemButton>
            <ListItemButton
              sx={{
                pl: 4,
                color: setColor("finished", "fulfiller"),
              }}
              onClick={() => switchStatusAndRole("finished", "fulfiller")}
            >
              <ListItemText primary="Ukončené" />
            </ListItemButton>
          </List>
        </Collapse>
        <ListItemButton
          onClick={() => {
            setOpenCreateWork(true)
          }}
        >
          <ListItemText primary="Zadat práci" />
        </ListItemButton>
      </List>
      <CreateOfferDialog
        open={openCreateWork}
        onClose={() => {
          setOpenCreateWork(false)
        }}
      />
    </>
  )
}

export default MyWorkMenuAside
