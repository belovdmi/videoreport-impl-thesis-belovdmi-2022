import {
  Card,
  CardContent,
  Stack,
  Box,
  Button,
  Typography,
} from "@mui/material"
import { Link } from "react-router-dom"
import LogoWhite from "../../assets/zcheckni-to-white.png"

type LoginRegisterCardProps = {
  onlyRegister?: boolean
  onlySignIn?: boolean
}

const LoginRegisterCard = ({
  onlyRegister = false,
  onlySignIn = false,
}: LoginRegisterCardProps) => {
  return (
    <Card
      sx={{
        bgcolor: "secondary.main",
        color: "secondary.contrastText",
      }}
      variant="outlined"
    >
      <CardContent>
        <Stack p={4}>
          <Box pb={4} textAlign={"center"}>
            <Link to={"/"}>
              <img src={LogoWhite} alt={"Zchecknito"} width={"300px"} />
            </Link>
          </Box>
          <Typography variant="body1">
            Nemáte čas řešit osodní prohlídky ? Nebo byste naopak chtěl vydělat
            jejich návštěvou ? S obojím pomůže Zchecknito ! Webový portál, který
            spojuje lidi s pořebou náhrady účasti na osobní prohlídce a lidi,
            kteří si chtěji ve volném čase přivydělat jejich návštěvou. Chcete
            se k nim přidat ?
          </Typography>

          <Stack
            direction="row"
            justifyContent={"center"}
            alignItems={"center"}
            pt={4}
          >
            {!onlyRegister && (
              <Button component={Link} to={"/login"} variant={"contained"}>
                Přihlasit se
              </Button>
            )}
            {!onlyRegister && !onlySignIn && <Box px={2}>NEBO</Box>}
            {!onlySignIn && (
              <Button component={Link} to={"/register"} variant={"contained"}>
                Registrovat se
              </Button>
            )}
          </Stack>
        </Stack>
      </CardContent>
    </Card>
  )
}

export default LoginRegisterCard
