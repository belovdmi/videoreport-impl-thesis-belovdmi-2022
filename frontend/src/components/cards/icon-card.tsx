import {
  Card,
  CardContent,
  Box,
  Button,
  Grid,
  Typography,
  CardMedia,
  Divider,
} from "@mui/material"
import HouseIcon from "../../assets/house-icon.png"
import ProfileIcon from "../../assets/profile-icon.png"
import CarIcon from "../../assets/car-icon.png"
import OtherIcon from "../../assets/other-icon.png"
import BasicLabeledValue from "../format/basic-labeled-value"

type DataItem = { name: string; value: string }

type IconCardProps = {
  title?: string
  icon?: string
  dataList?: DataItem[]
  onClick?: () => void
}

const IconCard = ({ title, icon, dataList, onClick }: IconCardProps) => {
  let iconPath
  if (icon === "profile") {
    iconPath = ProfileIcon
  } else if (icon === "Nemovitosti") {
    iconPath = HouseIcon
  } else if (icon === "Auta") {
    iconPath = CarIcon
  } else {
    iconPath = OtherIcon
  }

  return (
    <Grid item xs={12}>
      <Card sx={{ p: 1 }} raised>
        <Grid container>
          <Grid
            xs={12}
            md={4}
            item
            container
            direction={"row"}
            alignItems="center"
            width={"100%"}
          >
            <Box maxWidth={"50px"}>
              <CardMedia
                component="img"
                sx={{ height: "100%", py: 1 }}
                image={iconPath}
                alt="icon"
              />
            </Box>
            <Grid
              item
              xs
              overflow={"hidden"}
              textAlign={{ xs: "start", md: "center" }}
              pl={3}
              pr={"40px"}
            >
              <Typography variant="h6" fontWeight={600} noWrap>
                {title}
              </Typography>
            </Grid>

            <Divider
              orientation="vertical"
              sx={{
                ml: "auto",
                mr: 3,
                display: { xs: "none", md: "block" },
              }}
              component={"span"}
            />
          </Grid>
          <Grid xs={12} md={8} item container alignItems={"center"}>
            <Divider
              sx={{
                width: "100%",
                pt: 1,
                mb: 2,
                display: { xs: "block", md: "none" },
              }}
            />
            <CardContent
              sx={{
                px: 1,
                py: "0 !important",
                width: "100%",
              }}
            >
              <Grid container width="100%" rowSpacing={2} alignItems={"center"}>
                <Grid
                  xs={12}
                  sm
                  item
                  container
                  direction={{ xs: "column", sm: "row" }}
                  textAlign={"center"}
                >
                  {dataList?.map(item => (
                    <Grid xs={4} item overflow={"hidden"}>
                      <BasicLabeledValue label={item.name} value={item.value} />
                    </Grid>
                  ))}
                </Grid>

                <Grid item xs sm={12} md={"auto"} width={"100%"}>
                  <Button variant={"outlined"} onClick={onClick} fullWidth>
                    Více
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Grid>
        </Grid>
      </Card>
    </Grid>
  )
}

export default IconCard
