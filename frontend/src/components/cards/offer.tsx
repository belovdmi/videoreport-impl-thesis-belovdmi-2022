import {
  Grid,
  Card,
  Box,
  Typography,
  CardMedia,
  CardContent,
  Button,
} from "@mui/material"
import { Link } from "react-router-dom"

import Car from "../../assets/car.png"
import House from "../../assets/house.png"
import Other from "../../assets/other.png"
import { Job } from "../../requests/data-contracts"
import regionNameFromAbbreviation from "../../utils/region-name-from-abbreviation"
import BasicLabeledValue from "../format/basic-labeled-value"

type OfferCardProps = {
  data: Job
}

const OfferCard = ({ data }: OfferCardProps) => {
  const { id, price, title, category, dueDate, location } = data

  const locationMerged =
    location?.address +
    ", " +
    location?.town +
    ", " +
    regionNameFromAbbreviation(location?.region)

  let imageUtl = Car
  if (category?.name === "Nemovitosti") {
    imageUtl = House
  }

  if (category?.name === "Ostatní") {
    imageUtl = Other
  }

  return (
    <Grid xs={12} sm={6} lg={4} item>
      <Card sx={{ p: 2 }} raised>
        <Typography variant="h6" noWrap>
          {title || "Unknown"}
        </Typography>
        <Box display={"flex"}>
          <Box width={"160px"} height="160px" m={"auto"}>
            <CardMedia
              component="img"
              sx={{ width: "100%", py: 1 }}
              image={imageUtl}
              alt="illustration"
            />
          </Box>
          <CardContent sx={{ px: 1, py: "0 !important", m: "auto" }}>
            <BasicLabeledValue
              label={"Kategorie"}
              value={category?.name || "Unknown"}
            />
            <BasicLabeledValue
              label={"Cena"}
              value={price ? price + " Kč" : "Unknown"}
            />
            <BasicLabeledValue
              label={"Datum splnění do"}
              value={dueDate || "Unknown"}
            />
          </CardContent>
        </Box>

        <BasicLabeledValue
          label={"Místo"}
          value={locationMerged || "Unknown"}
        />
        <Box textAlign={"center"} pt={2}>
          <Button
            variant="outlined"
            fullWidth
            component={Link}
            to={`/offers/${id || 0}`}
          >
            Více
          </Button>
        </Box>
      </Card>
    </Grid>
  )
}

export default OfferCard
