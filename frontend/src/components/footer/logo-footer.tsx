import { Box, useTheme } from "@mui/material"
import { Link } from "react-router-dom"

import LogoWhite from "../../assets/zcheckni-to-white.png"

type LogoFooterProps = {
  hideOnXs?: boolean
}

const LogoFooter = ({ hideOnXs = false }: LogoFooterProps) => {
  const theme = useTheme()

  return (
    <Box
      py={2}
      textAlign={"center"}
      width={"100%"}
      bgcolor={theme.palette.secondary.main}
      display={{ xs: "block", md: hideOnXs ? "none" : "block" }}
    >
      <Link to="/">
        <img src={LogoWhite} alt={"Zchecknito"} width={"200px"} />
      </Link>
    </Box>
  )
}

export default LogoFooter
