import { Box, Divider, Grid, Link } from "@mui/material"

type PageFooterProps = {
  hasDivider?: boolean
}

const PageFooter = ({ hasDivider = true }: PageFooterProps) => {
  return (
    <Box position={"relative"} mt={-8} width={"100%"}>
      {hasDivider && <Divider />}
      <Grid
        container
        direction={"row"}
        px={3}
        py={1.5}
        alignItems="center"
        fontSize={12}
      >
        <Grid xs item>
          © 2022 Dmitry Belov
        </Grid>
        <Grid
          xs
          item
          container
          direction={"column"}
          textAlign={"right"}
          justifyContent={"end"}
          rowGap={0.5}
        >
          <Link>Smluvní podminky</Link>
          <Link>Ochrana údajů</Link>
        </Grid>
      </Grid>
    </Box>
  )
}

export default PageFooter
