import { Alert, AlertColor, Fade, FadeProps, Snackbar } from "@mui/material"
import { FC } from "react"
import { SyntheticEvent } from "react"
import { useRecoilValue } from "recoil"
import { aLoading } from "../../state/global"

type AlertSnackbarProps = {
  open: boolean
  onClose: () => void
  severity: AlertColor
}

const AlertSnackbar: FC<AlertSnackbarProps> = ({
  open,
  onClose,
  severity,
  children,
}) => {
  const loading = useRecoilValue(aLoading)
  const handleClose = (event?: SyntheticEvent | Event, reason?: string) => {
    if (reason === "clickaway") {
      return
    }

    onClose()
  }

  return (
    <Snackbar
      open={open && !loading}
      autoHideDuration={10000}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      disableWindowBlurListener
      onClose={handleClose}
      TransitionComponent={(props: FadeProps) => {
        return <Fade {...props} timeout={0} />
      }}
      key={"key"}
    >
      <Alert
        variant="filled"
        severity={severity}
        sx={{ width: "100%", fontSize: 16 }}
        onClose={handleClose}
      >
        {children}
      </Alert>
    </Snackbar>
  )
}

export default AlertSnackbar
