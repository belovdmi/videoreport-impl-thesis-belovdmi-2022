import {
  Box,
  Button,
  Dialog,
  Divider,
  Grid,
  Rating,
  Stack,
  Typography,
} from "@mui/material"
import CloseDialogButton from "../ui/buttons/close-dialog-button"
import ProfileIcon from "../../assets/profile-icon.png"
import { useState } from "react"
import BasicButtonsContainer from "../containers/basic-buttons-container"
import axios from "axios"
import { useErrorHandler } from "react-error-boundary"
import { useSetRecoilState, useRecoilValue, useRecoilState } from "recoil"
import { execRefreshToken } from "../../requests/refresh-token"
import {
  aLoading,
  aErrorType,
  aRefreshToken,
  aAuthToken,
  aAlert,
  aViewedJob,
} from "../../state/global"
import { securedUser } from "../../requests/Users"
import { useForm } from "react-hook-form"
import { CreateReviewPayload } from "../../requests/data-contracts"
import ControlledTextField from "../ui/text-field/controlled-text-field"

type ReviewDialogProps = {
  open: boolean
  onClose: () => void
  jobId: number
  username: string
}

const ReviewDialog = ({
  username,
  jobId,
  open,
  onClose,
}: ReviewDialogProps) => {
  const handleError = useErrorHandler()
  const { handleSubmit, control } = useForm()
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const [viewedJob, setViewedJob] = useRecoilState(aViewedJob)
  const [value, setValue] = useState<number | undefined>(0)

  const onSubmit = async (data: CreateReviewPayload) => {
    try {
      setLoading(true)
      const newAuthToken = await execRefreshToken({
        authToken,
        refreshToken,
      })
      setAuthToken(newAuthToken)

      const api = securedUser
      api.setSecurityData(newAuthToken)

      await api.createReview(username, { ...data, jobId, score: value })

      setAlert({
        alertMsg: "Recenze byla odeslána.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newViewedJob = { ...viewedJob }
        newViewedJob.reviewed = true
        setViewedJob(newViewedJob)
      }

      onClose()
    } catch (error) {
      if (
        error instanceof Error &&
        error?.message === "refresh token expired"
      ) {
        setErrorType("logAgain")
      }

      if (axios.isAxiosError(error) && error.response?.status === 403) {
        setErrorType("forbidden")
      }

      handleError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Dialog open={open} maxWidth={"md"} fullWidth onClose={onClose}>
      <Stack rowGap={2} p={2}>
        <Grid container display={"flex"} alignItems={"center"}>
          <Box pr={2} width={40}>
            <img src={ProfileIcon} alt="profile-icon" width={"100%"} />
          </Box>
          <Grid xs item>
            <Typography variant="h5">{username}</Typography>
          </Grid>
        </Grid>
        <CloseDialogButton onClick={onClose} />
        <Divider />
        <Grid container>
          <Typography
            variant="subtitle1"
            color={"text.secondary"}
            component="span"
            pr={3}
          >
            Skóre:
          </Typography>
          <Rating
            name="simple-controlled"
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue ?? 0)
            }}
            precision={0.5}
          />
        </Grid>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box pt={2} minHeight={200}>
            <Typography
              variant="h6"
              component={"div"}
              fontWeight={600}
              gutterBottom
            >
              Recenze:
            </Typography>
            <ControlledTextField
              name="review"
              type="text"
              variant="outlined"
              control={control}
              rows={5}
              rules={{
                maxLenght: {
                  value: 200,
                  message: "Překračuje povolenou délku.",
                },
              }}
              multiline
              fullWidth
            />
          </Box>
          <BasicButtonsContainer>
            <Button variant="contained" type="submit">
              Ohodnotit protistranu
            </Button>
          </BasicButtonsContainer>
        </form>
      </Stack>
    </Dialog>
  )
}

export default ReviewDialog
