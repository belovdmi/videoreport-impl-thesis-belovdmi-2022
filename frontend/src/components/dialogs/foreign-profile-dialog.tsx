import { Box, Button, Collapse, Dialog, Divider, Grid } from "@mui/material"
import { useState } from "react"
import { User } from "../../requests/data-contracts"
import ProfileLabeledValue from "../format/profile-labeled-value"
import ProfileDetails from "../profile-details"
import ReviewsTable from "../tables/reviews-table"
import CloseDialogButton from "../ui/buttons/close-dialog-button"

type ForeignProfileDialogProps = {
  userData: User | undefined
  open: boolean
  onClose: () => void
}

const ForeignProfileDialog = ({
  userData,
  open,
  onClose,
}: ForeignProfileDialogProps) => {
  const [showReviews, setShowReviews] = useState(false)

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
      <CloseDialogButton onClick={() => onClose()} />
      <ProfileDetails data={userData} />

      <Grid container rowGap={2} px={{ xs: 3, sm: 6 }} pb={3}>
        <Grid xs={12} md={6} container item direction={"column"} rowGap={2}>
          <ProfileLabeledValue
            label={"Skore:"}
            value={userData?.scoreSummary?.score?.toString() || "-"}
          />
          <ProfileLabeledValue
            label={"Počet recenzí:"}
            value={userData?.scoreSummary?.numberOfReviews?.toString() || "-"}
          />
        </Grid>
        <Grid xs={12} md={6} container item direction={"column"} rowGap={2}>
          <ProfileLabeledValue
            label={"Vytvořeno objednávek:"}
            value={userData?.scoreSummary?.jobsGiven?.toString() || "-"}
          />
          <ProfileLabeledValue
            label={"Splněno objednávek:"}
            value={userData?.scoreSummary?.jobsTaken?.toString() || "-"}
          />
        </Grid>
      </Grid>

      <Box textAlign={"center"} px={3} pb={3}>
        <Divider sx={{ mb: 3 }} />
        <Button sx={{ mb: 2 }} onClick={() => setShowReviews(!showReviews)}>
          {showReviews ? "Skrýt recenze" : "Zobrazit recenze"}
        </Button>
        <Collapse in={showReviews}>
          <ReviewsTable username={userData?.username || "-"} />
        </Collapse>
      </Box>
    </Dialog>
  )
}

export default ForeignProfileDialog
