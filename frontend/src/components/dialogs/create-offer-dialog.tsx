import { Dialog, Stack, Typography, Divider } from "@mui/material"
import UploadWorkFrom from "../forms/upload-work-form"
import CloseDialogButton from "../ui/buttons/close-dialog-button"

type CreateOfferDialogProps = {
  open: boolean
  onClose: () => void
}

const CreateOfferDialog = ({ open, onClose }: CreateOfferDialogProps) => {
  return (
    <Dialog open={open} maxWidth={"md"} fullWidth onClose={onClose}>
      <Stack rowGap={2} p={2}>
        <Typography variant="h4" color="primary">
          Vytvořit práci
        </Typography>
        <CloseDialogButton onClick={onClose} />
        <Divider />
        <UploadWorkFrom onSuccess={onClose} />
      </Stack>
    </Dialog>
  )
}

export default CreateOfferDialog
