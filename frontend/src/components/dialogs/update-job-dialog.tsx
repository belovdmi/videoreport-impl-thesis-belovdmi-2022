import { Dialog, Stack, Typography, Divider } from "@mui/material"
import UpdateWorkForm from "../forms/update-work-form"
import CloseDialogButton from "../ui/buttons/close-dialog-button"

type UpdateJobDialogProps = {
  open: boolean
  onClose: () => void
}

const UpdateJobDialog = ({ open, onClose }: UpdateJobDialogProps) => {
  return (
    <Dialog open={open} maxWidth={"md"} fullWidth onClose={onClose}>
      <Stack rowGap={2} p={2}>
        <Typography variant="h4" color="primary">
          Upravit práci
        </Typography>
        <CloseDialogButton onClick={onClose} />
        <Divider />
        <UpdateWorkForm onSuccess={onClose} />
      </Stack>
    </Dialog>
  )
}

export default UpdateJobDialog
