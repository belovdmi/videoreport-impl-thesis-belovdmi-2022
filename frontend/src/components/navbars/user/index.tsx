import { Grid } from "@mui/material"
import NavbarStandardButton from "../../ui/buttons/navbar-standard-button"
import SignOutButton from "../../ui/buttons/sign-out-button"
import DesktopNavbar from "../desktop-navbar"
import MobileNavbar from "../mobile-navbar"
import AccountNavbar from "./account"

const UserNavbar = () => {
  const navbarItems = [
    { name: "Profil", pathname: "/my-profile" },
    { name: "Moje práce", pathname: "/my-works" },
    { name: "Nabídka prací", pathname: "/offers" },
  ]

  return (
    <>
      <DesktopNavbar navbarItemList={navbarItems}>
        <Grid item>
          <AccountNavbar>
            <>
              <NavbarStandardButton pathname={"/settings"}>
                Nastavení
              </NavbarStandardButton>
              <SignOutButton />
            </>
          </AccountNavbar>
        </Grid>
      </DesktopNavbar>
      <MobileNavbar
        navbarItemList={[
          ...navbarItems,
          { name: "Nastavení", pathname: "/settings" },
        ]}
      >
        <Grid item>
          <SignOutButton />
        </Grid>
      </MobileNavbar>
    </>
  )
}

export default UserNavbar
