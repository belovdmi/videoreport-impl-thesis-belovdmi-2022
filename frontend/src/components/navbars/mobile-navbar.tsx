import { Close, Menu } from "@material-ui/icons"
import {
  Button,
  ClickAwayListener,
  Collapse,
  Divider,
  Grid,
  IconButton,
} from "@mui/material"
import { FC, useState } from "react"
import { Link } from "react-router-dom"
import NavbarStandardButton from "../ui/buttons/navbar-standard-button"
import NavbarContainer from "../containers/navbar-container"

type NavbarItem = { name: string; pathname: string }

type MobileNavbarProps = {
  navbarItemList?: NavbarItem[]
}

const MobileNavbar: FC<MobileNavbarProps> = ({ navbarItemList, children }) => {
  const [dropedMobile, setDropedMobile] = useState(false)

  return (
    <Grid display={{ xs: "block", md: "none" }}>
      <ClickAwayListener onClickAway={() => setDropedMobile(false)}>
        <div>
          <NavbarContainer>
            <Grid item xs={6} textAlign="end">
              <IconButton
                size="large"
                sx={{ fontSize: 28 }}
                onClick={() => {
                  setDropedMobile(!dropedMobile)
                }}
              >
                {dropedMobile ? (
                  <Close fontSize="inherit" />
                ) : (
                  <Menu fontSize="inherit" />
                )}
              </IconButton>
            </Grid>
            <Grid item xs={12} width={"100%"}>
              <Collapse in={dropedMobile}>
                <Grid
                  xs={12}
                  width={"100%"}
                  item
                  container
                  direction={"column"}
                  rowSpacing={2}
                  textAlign="center"
                  pb={2}
                  onClick={() => {
                    setDropedMobile(!dropedMobile)
                  }}
                >
                  <Divider sx={{ mt: 2 }} />
                  {navbarItemList?.map((item, index) => (
                    <Grid item key={item.name}>
                      {index + 1 !== navbarItemList.length || children ? (
                        <NavbarStandardButton pathname={item.pathname}>
                          {item.name}
                        </NavbarStandardButton>
                      ) : (
                        <Button
                          disableRipple
                          fullWidth
                          color="secondary"
                          variant="outlined"
                          component={Link}
                          to={item.pathname}
                        >
                          {item.name}
                        </Button>
                      )}
                    </Grid>
                  ))}
                  {children}
                </Grid>
              </Collapse>
            </Grid>
          </NavbarContainer>
        </div>
      </ClickAwayListener>
    </Grid>
  )
}

export default MobileNavbar
