import DesktopNavbar from "../desktop-navbar"
import MobileNavbar from "../mobile-navbar"

const MainNavbar = () => {
  const navbarItems = [
    { name: "O nás", pathname: "/" },
    { name: "Nabídka prací", pathname: "/offers" },
    { name: "Přihlásit", pathname: "/login" },
    { name: "Registrovat", pathname: "/register" },
  ]

  return (
    <>
      <DesktopNavbar navbarItemList={navbarItems} />
      <MobileNavbar navbarItemList={[...navbarItems]} />
    </>
  )
}

export default MainNavbar
