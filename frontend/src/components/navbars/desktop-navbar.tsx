import { Box, Button, Grid } from "@mui/material"
import { FC } from "react"
import { Link } from "react-router-dom"
import NavbarStandardButton from "../ui/buttons/navbar-standard-button"
import NavbarContainer from "../containers/navbar-container"

type NavbarItem = { name: string; pathname: string }

type DesktopNavbarProps = {
  navbarItemList?: NavbarItem[]
}

const DesktopNavbar: FC<DesktopNavbarProps> = ({
  navbarItemList,
  children,
}) => {
  return (
    <Box display={{ xs: "none", md: "flex" }}>
      <NavbarContainer>
        <Grid
          xs
          width={"100%"}
          item
          container
          justifyContent={"end"}
          alignItems={"center"}
          columnSpacing={2}
        >
          {navbarItemList?.map((item, index) => (
            <Grid key={item.name} item>
              {index + 1 !== navbarItemList.length || children ? (
                <NavbarStandardButton pathname={item.pathname}>
                  {item.name}
                </NavbarStandardButton>
              ) : (
                <Button
                  disableRipple
                  fullWidth
                  color="secondary"
                  variant="outlined"
                  component={Link}
                  to={item.pathname}
                >
                  {item.name}
                </Button>
              )}
            </Grid>
          ))}
          {children}
        </Grid>
      </NavbarContainer>
    </Box>
  )
}

export default DesktopNavbar
