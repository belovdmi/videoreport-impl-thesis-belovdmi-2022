import { Close } from "@material-ui/icons"
import { Container, IconButton, Box } from "@mui/material"
import { FC, useState } from "react"
import { Link } from "react-router-dom"
import { useEffectOnce } from "react-use"
import { useRecoilValue, useResetRecoilState } from "recoil"
import { aAlert, aLoading } from "../../state/global"
import Footer from "../footer/page-footer"
import LoadingOverlay from "../loading-overlay"
import AlertSnackbar from "../snackbars/alert-snackbar"

type CenterContentContainerProps = {
  dontRefresh?: boolean
}

const CenterContentContainer: FC<CenterContentContainerProps> = ({
  dontRefresh = true,
  children,
}) => {
  const loading = useRecoilValue(aLoading)
  const [redicetded, setRedicetded] = useState(false)
  const alert = useRecoilValue(aAlert)
  const resetAlert = useResetRecoilState(aAlert)

  if (redicetded && !dontRefresh) {
    window.location.reload()
  }

  useEffectOnce(() => {
    resetAlert()
  })
  return (
    <Box position={"relative"}>
      <Container maxWidth="lg" sx={{ minHeight: "100vh" }}>
        <IconButton
          aria-label="close"
          size="large"
          sx={{ position: "absolute", top: 10, right: 10 }}
          component={Link}
          to={"/"}
          onClick={() => setRedicetded(true)}
        >
          <Close fontSize="inherit" />
        </IconButton>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          minHeight={"100vh"}
        >
          {children}
        </Box>
      </Container>
      <Footer hasDivider={false} />
      <LoadingOverlay open={loading} />
      <AlertSnackbar
        severity={alert.severity}
        onClose={() => resetAlert()}
        open={alert.open}
      >
        {alert.alertMsg}
      </AlertSnackbar>
    </Box>
  )
}

export default CenterContentContainer
