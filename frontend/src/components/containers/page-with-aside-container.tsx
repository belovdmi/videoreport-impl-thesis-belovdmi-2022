import { Grid, Divider } from "@mui/material"
import { FC } from "react"
import { useRecoilValue } from "recoil"
import { aLoading } from "../../state/global"
import LoadingOverlay from "../loading-overlay"
import AsideMenuContainer from "./aside-menu-container"
import BasicPageContainer from "./basic-page-container"

type PageWithAsideContainerProps = {
  asideContent: JSX.Element
}

const PageWithAsideContainer: FC<PageWithAsideContainerProps> = ({
  asideContent,
  children,
}) => {
  const loading = useRecoilValue(aLoading)
  return (
    <BasicPageContainer>
      <Grid
        container
        alignContent={"stretch"}
        minHeight="100vh"
        pt={13}
        pb={10}
      >
        <AsideMenuContainer>{asideContent}</AsideMenuContainer>
        <Grid xs item container direction={"column"} pl={{ xs: 0, md: 4 }}>
          {children}
        </Grid>
        <Divider />
      </Grid>
      <LoadingOverlay open={loading} />
    </BasicPageContainer>
  )
}

export default PageWithAsideContainer
