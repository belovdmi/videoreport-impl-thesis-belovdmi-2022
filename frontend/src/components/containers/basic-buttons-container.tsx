import { Stack } from "@mui/material"
import { FC } from "react"

const BasicButtonsContainer: FC = ({ children }) => {
  return (
    <Stack
      pt={3}
      direction={{ xs: "column", md: "row" }}
      spacing={2}
      justifyContent={"space-around"}
    >
      {children}
    </Stack>
  )
}

export default BasicButtonsContainer
