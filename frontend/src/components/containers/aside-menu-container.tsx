import { Divider, Grid } from "@mui/material"
import { FC } from "react"

const AsideMenuContainer: FC = ({ children }) => {
  return (
    <>
      <Divider
        orientation="vertical"
        sx={{
          width: 10,
          height: "auto",
          display: { lg: "block", xs: "none" },
        }}
      />
      <Grid
        display={{ lg: "flex", xs: "none" }}
        flexDirection="column"
        item
        width={220}
        p={2}
        overflow={"auto"}
        maxHeight={"100%"}
        height="auto"
      >
        {children}
      </Grid>
      <Divider
        orientation="vertical"
        sx={{
          width: 10,
          height: "auto",
          display: { lg: "block", xs: "none" },
        }}
      />
    </>
  )
}

export default AsideMenuContainer
