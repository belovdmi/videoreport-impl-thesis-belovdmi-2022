import { Box, Container, Divider, Grid } from "@mui/material"
import { FC } from "react"
import { Link } from "react-router-dom"
import { useRecoilValue } from "recoil"
import Logo from "../../assets/zcheckni-to.png"
import { aLoggedUsername } from "../../state/global"

const NavbarContainer: FC = ({ children }) => {
  const loggedUsername = useRecoilValue(aLoggedUsername)

  return (
    <Box
      bgcolor={"primary.contrastText"}
      position="fixed"
      width={"100%"}
      sx={{
        backdropFilter: "blur(20px)",
        backgroundColor: "#ffffff85",
      }}
      zIndex={100}
    >
      <Container maxWidth="xl">
        <Grid container alignItems={"center"} py={3}>
          <Grid xs={6} md={"auto"} item container>
            <Link to={loggedUsername ? "/my-profile" : "/"}>
              <img src={Logo} alt="zchecknito" height={30} width={"100%"} />
            </Link>
          </Grid>
          {children}
        </Grid>
      </Container>
      <Divider />
    </Box>
  )
}

export default NavbarContainer
