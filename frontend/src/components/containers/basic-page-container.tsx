import { Box, Container } from "@mui/material"
import { FC } from "react"
import { useEffectOnce } from "react-use"
import { useRecoilValue, useResetRecoilState } from "recoil"
import { aAlert, aLoading, aLogged } from "../../state/global"
import Footer from "../footer/page-footer"
import LoadingOverlay from "../loading-overlay"
import MainNavbar from "../navbars/main"
import UserNavbar from "../navbars/user"
import AlertSnackbar from "../snackbars/alert-snackbar"

const BasicPageContainer: FC = ({ children }) => {
  const loading = useRecoilValue(aLoading)
  const logged = useRecoilValue(aLogged)
  const alert = useRecoilValue(aAlert)
  const resetAlert = useResetRecoilState(aAlert)

  useEffectOnce(() => {
    resetAlert()
  })

  return (
    <Box height={"100%"} minHeight={"100vh"}>
      {logged ? <UserNavbar /> : <MainNavbar />}
      <Container
        sx={{ minHeight: "100vh", px: { xs: 5, md: 10 } }}
        maxWidth="xl"
      >
        <Box height={"100%"}>{children}</Box>
      </Container>
      <Footer />
      <LoadingOverlay open={loading} />
      <AlertSnackbar
        severity={alert.severity}
        onClose={() => resetAlert()}
        open={alert.open}
      >
        {alert.alertMsg}
      </AlertSnackbar>
    </Box>
  )
}

export default BasicPageContainer
