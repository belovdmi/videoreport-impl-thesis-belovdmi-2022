import { Button } from "@mui/material"
import { FC } from "react"
import { Link, useLocation } from "react-router-dom"

type NavbarStandardButtonProps = {
  pathname: string
}

const NavbarStandardButton: FC<NavbarStandardButtonProps> = ({
  pathname,
  children,
}) => {
  const currentPathname = useLocation().pathname

  return (
    <Button
      color="secondary"
      sx={
        currentPathname === pathname
          ? {
              fontWeight: 700,
              color: "primary.main",
            }
          : null
      }
      component={Link}
      to={pathname}
      disableRipple
      fullWidth
    >
      {children}
    </Button>
  )
}

export default NavbarStandardButton
