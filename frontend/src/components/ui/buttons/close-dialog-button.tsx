import { Close } from "@material-ui/icons"
import { IconButton } from "@mui/material"

type CloseDialogButtonProps = {
  onClick: () => void
  topBotPosition?: number
}

const CloseDialogButton = ({
  topBotPosition = 12,
  onClick,
}: CloseDialogButtonProps) => (
  <IconButton
    aria-label="close"
    size={"large"}
    onClick={onClick}
    sx={{ position: "absolute", right: topBotPosition, top: topBotPosition }}
  >
    <Close fontSize="inherit" />
  </IconButton>
)

export default CloseDialogButton
