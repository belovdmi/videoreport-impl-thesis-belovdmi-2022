import { ArrowLeft } from "@material-ui/icons"
import { Grid, Box, Button } from "@mui/material"

type NextAndPrevPageButtonsProps = {
  onNextPage?: () => void
  onPrevPage?: () => void
  nextDisabled?: boolean
  prevDisabled?: boolean
}

const NextAndPrevPageButtons = ({
  onNextPage,
  onPrevPage,
  prevDisabled = false,
  nextDisabled = false,
}: NextAndPrevPageButtonsProps) => {
  return (
    <Grid container justifyContent={"center"} pt={4} pb={2}>
      <Box pr={1}>
        <Button
          variant="contained"
          color="secondary"
          onClick={onPrevPage}
          disabled={prevDisabled}
        >
          <ArrowLeft />
        </Button>
      </Box>
      <Button variant="contained" onClick={onNextPage} disabled={nextDisabled}>
        Další stránka
      </Button>
    </Grid>
  )
}

export default NextAndPrevPageButtons
