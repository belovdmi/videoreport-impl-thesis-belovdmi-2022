import { Button } from "@mui/material"
import { useErrorHandler } from "react-error-boundary"
import { useNavigate } from "react-router-dom"
import {
  useRecoilState,
  useRecoilValue,
  useResetRecoilState,
  useSetRecoilState,
} from "recoil"
import { execRefreshToken } from "../../../requests/refresh-token"
import { securedUser } from "../../../requests/Users"
import {
  aAuthToken,
  aLoading,
  aLogged,
  aLoggedUsername,
  aRefreshToken,
} from "../../../state/global"

const SignOutButton = () => {
  const handleError = useErrorHandler()
  const setLoading = useSetRecoilState(aLoading)
  const navigate = useNavigate()
  const resetLogged = useResetRecoilState(aLogged)
  const resetLoggedUsername = useResetRecoilState(aLoggedUsername)
  const resetAuthToken = useResetRecoilState(aAuthToken)
  const resetRefreshToken = useResetRecoilState(aRefreshToken)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)

  return (
    <Button
      disableRipple
      fullWidth
      color="secondary"
      variant="outlined"
      onClick={async () => {
        try {
          setLoading(true)
          const newAuthToken = await execRefreshToken({
            authToken,
            refreshToken,
          })
          setAuthToken(newAuthToken)

          const api = securedUser
          api.setSecurityData(newAuthToken)
          await api.logoutUser()
          resetLogged()
          resetLoggedUsername()
          resetAuthToken()
          resetRefreshToken()
          navigate("/")
        } catch (error) {
          if (
            error instanceof Error &&
            error?.message === "refresh token expired"
          ) {
            resetLogged()
            resetLoggedUsername()
            resetAuthToken()
            resetRefreshToken()
            navigate("/")
          } else {
            handleError(error)
          }
        } finally {
          setLoading(false)
        }
      }}
    >
      Odhlásit se
    </Button>
  )
}

export default SignOutButton
