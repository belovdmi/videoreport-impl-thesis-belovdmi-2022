import { SxProps, TextField, Theme } from "@mui/material"
import { Controller, Control } from "react-hook-form"

type Props = {
  type?: "text" | "number" | "password" | "email" | "date"
  variant?: "filled" | "standard" | "outlined"
  label?: string
  sx?: SxProps<Theme>
  multiline?: boolean
  fullWidth?: boolean
  rows?: number
  // eslint-disable-next-line @typescript-eslint/ban-types
  InputLabelProps?: Object
}

type ControllerProps = {
  name: string
  control: Control
  defaultValue?: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  rules?: Object
}

const ControlledTextField = (props: ControllerProps & Props) => {
  return (
    <Controller
      name={props.name}
      control={props.control}
      defaultValue={props?.defaultValue || ""}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <TextField
          label={props.label}
          variant={props.variant || "standard"}
          value={value}
          type={props.type || "text"}
          onChange={onChange}
          error={!!error}
          helperText={error ? error.message : null}
          sx={{ ...props.sx, height: 55 }}
          multiline={props?.multiline}
          rows={props?.rows}
          fullWidth={props?.fullWidth}
          InputLabelProps={props?.InputLabelProps}
        />
      )}
      rules={props.rules}
    />
  )
}

export default ControlledTextField
