import { Card, CardContent, Typography, Button } from "@mui/material"
import CenterContentContainer from "../containers/center-content-container"
import LogoFooter from "../footer/logo-footer"
import BasicButtonsConatiner from "../containers/basic-buttons-container"
import { FC, useState } from "react"
import { Link } from "react-router-dom"

type ErrorTemplateProps = {
  title?: string
  buttonText?: string
  buttonLink?: string
  dontRefresh?: boolean
}

const ErrorTemplate: FC<ErrorTemplateProps> = ({
  title,
  buttonText,
  buttonLink,
  children,
  dontRefresh = false,
}) => {
  const [redicetded, setRedicetded] = useState(false)

  if (redicetded && !dontRefresh) {
    window.location.reload()
  }

  return (
    <CenterContentContainer dontRefresh={false}>
      <Card raised>
        <CardContent sx={{ p: 4 }}>
          <Typography variant={"h4"} gutterBottom pb={2}>
            {title || "A jejda neco se pokazilo !"}
          </Typography>
          <Typography gutterBottom>
            {children ||
              "Zdřejmě nastala chyba, kterou jsme nečekali... omlouváme se a pokusíme se jí co nejdřive opravit. Kontaktujte prosím administrátora stránek a pokručujte zpět na uvodní obrazovku."}
          </Typography>
          <BasicButtonsConatiner>
            <Button
              variant="contained"
              component={Link}
              to={buttonLink || "/"}
              disableRipple
              onClick={() => {
                setRedicetded(true)
              }}
            >
              {buttonText || "Zpět na úvodní obrazovku"}
            </Button>
          </BasicButtonsConatiner>
        </CardContent>
        <LogoFooter />
      </Card>
    </CenterContentContainer>
  )
}

export default ErrorTemplate
