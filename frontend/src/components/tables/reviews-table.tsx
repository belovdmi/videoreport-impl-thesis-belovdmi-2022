import { Stack, Tooltip } from "@mui/material"
import { DataGrid, GridColumns, GridSortModel } from "@mui/x-data-grid"
import { useState } from "react"
import { useRecoilState } from "recoil"
import useLoadReviews from "../../hooks/use-load-reviews"
import { SortReviewsBy } from "../../requests/Users"
import { aReviewsQuery } from "../../state/global"

const columns: GridColumns = [
  { field: "jobId", headerName: "Id Práce", sortable: false },
  {
    headerName: "Uživatelské jméno",
    field: "reviewerUsername",
    flex: 0.2,
    minWidth: 150,
    sortable: false,
  },
  {
    headerName: "Rezenze",
    field: "review",
    flex: 1,
    minWidth: 300,
    sortable: false,
    renderCell: params => (
      <Tooltip title={params.value}>
        <span className="csutable-cell-trucate">{params.value}</span>
      </Tooltip>
    ),
  },
  {
    headerName: "Skóre",
    field: "score",
  },
  {
    headerName: "Vytvořeno",
    field: "created",
    flex: 0.2,
    minWidth: 150,
  },
]

type ReviewsTableProps = {
  username: string
}

const ReviewsTable = ({ username }: ReviewsTableProps) => {
  const [sortModel, setSortModel] = useState<GridSortModel>([
    { field: "created", sort: "asc" },
  ])
  const [reviewsQuery, setReviewsQuery] = useRecoilState(aReviewsQuery)

  const { reviews, totalCount } = useLoadReviews(username)

  const handleSortModelChange = (newModel: GridSortModel) => {
    setSortModel(newModel)

    if (newModel.length) {
      const sortString = newModel[0].field + "." + newModel[0].sort
      const newReviewsQuery = { ...reviewsQuery }
      newReviewsQuery.sortBy = sortString as SortReviewsBy
      setReviewsQuery(newReviewsQuery)
    }
  }

  return (
    <div style={{ height: "100%" }}>
      <DataGrid
        autoHeight
        sx={{ minHeight: 360 }}
        disableSelectionOnClick
        disableColumnMenu
        pagination
        rowsPerPageOptions={[5]}
        pageSize={5}
        paginationMode="server"
        onPageChange={page => {
          const newReviewsQuery = { ...reviewsQuery }
          newReviewsQuery.page = page + 1
          setReviewsQuery(newReviewsQuery)
        }}
        rowCount={totalCount}
        sortingMode="server"
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}
        columns={columns}
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        rows={reviews as { [key: string]: any }[]}
        components={{
          NoRowsOverlay: () => (
            <Stack height="100%" alignItems="center" justifyContent="center">
              Zatím tu nic není :(
            </Stack>
          ),
          NoResultsOverlay: () => (
            <Stack height="100%" alignItems="center" justifyContent="center">
              Nic jsem nenašel :(
            </Stack>
          ),
        }}
      />
    </div>
  )
}

export default ReviewsTable
