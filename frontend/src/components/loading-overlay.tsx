import { Backdrop, CircularProgress } from "@mui/material"

type LoadingOverlayProps = {
  open: boolean
}

const LoadingOverlay = ({ open }: LoadingOverlayProps) => {
  return (
    <Backdrop
      sx={{
        color: "#fff",
        zIndex: theme => theme.zIndex.drawer + 2000,
        position: "ablosute",
      }}
      open={open}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  )
}

export default LoadingOverlay
