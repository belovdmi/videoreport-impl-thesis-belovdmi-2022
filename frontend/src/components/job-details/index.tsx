import { Close } from "@material-ui/icons"
import {
  Box,
  Chip,
  Divider,
  Grid,
  IconButton,
  Stack,
  Typography,
} from "@mui/material"
import BasicLabeledValue from "../format/basic-labeled-value"
import HouseIcon from "../../assets/house-icon.png"
import CarIcon from "../../assets/car-icon.png"
import OtherIcon from "../../assets/other-icon.png"
import { Job } from "../../requests/data-contracts"
import regionNameFromAbbreviation from "../../utils/region-name-from-abbreviation"

type JobDetailsProps = {
  data: Job
  onClose: () => void
}

const JobDetails = ({ data, onClose }: JobDetailsProps) => {
  const {
    price,
    dueDate,
    title,
    description,
    advertisementLink,
    category,
    contactPhoneNumber,
    contactEmail,
    location,
  } = data
  let iconSrc = OtherIcon
  if (category?.name === "Auta") {
    iconSrc = CarIcon
  }

  if (category?.name === "Nemovitosti") {
    iconSrc = HouseIcon
  }
  return (
    <>
      <Stack direction={"row"} alignItems={"center"} pb={2}>
        <Box width={"100%"} display="flex" alignItems={"center"}>
          <Box component={"span"} width={"40px"}>
            <img src={iconSrc} alt={"job-icon"} width={"100%"} />
          </Box>

          <Typography variant={"h4"} color="primary" pl={2}>
            {title || "Unknown"}
          </Typography>
        </Box>

        <Box>
          <IconButton aria-label="close" size={"large"} onClick={onClose}>
            <Close fontSize="inherit" />
          </IconButton>
        </Box>
      </Stack>
      <Divider sx={{ mb: 3 }} />
      <Grid container columnGap={10} px={{ xs: 0, md: 5 }}>
        <Grid xs item container rowGap={3}>
          <Grid xs={12} item overflow={"auto"} height={{ xs: "auto", lg: 90 }}>
            <BasicLabeledValue label={"Popis"}>
              {description || "-"}
            </BasicLabeledValue>
          </Grid>

          <Grid xs={12} sm item>
            <BasicLabeledValue label={"Město"} value={location?.town || "-"} />
          </Grid>
          <Grid xs={12} sm item>
            <BasicLabeledValue
              label={"Adressa"}
              value={location?.address || "-"}
            />
          </Grid>
          <Grid xs={12} item>
            <BasicLabeledValue
              label={"Kraj"}
              value={regionNameFromAbbreviation(location?.region) || "-"}
            />
          </Grid>
        </Grid>
        <Grid xs={12} lg container item pt={2} rowSpacing={2}>
          <Grid xs={12} item container>
            <Grid xs={12} sm item container>
              <BasicLabeledValue label={"Email:"} value={contactEmail || "-"} />
            </Grid>
            <Grid xs={12} sm item container>
              <BasicLabeledValue
                label={"Telefoní číslo:"}
                value={contactPhoneNumber || "-"}
              />
            </Grid>
          </Grid>
          <Grid xs={12} item container>
            <Grid xs={12} sm item container>
              <BasicLabeledValue
                label={"Odkaz na inzerci:"}
                value={advertisementLink || "-"}
              />
            </Grid>
            <Grid xs={12} sm item container>
              <BasicLabeledValue
                label={"Cena:"}
                value={price ? price + " Kč" : "-"}
              />
            </Grid>
          </Grid>

          <Grid xs={12} sm item container>
            <BasicLabeledValue
              label={"Datum splnění:"}
              value={dueDate || "-"}
            />
          </Grid>
          <Grid xs={12} sm item container>
            <BasicLabeledValue label={"Kategorie:"}>
              <Chip
                label={category?.name || "-"}
                variant="outlined"
                color="primary"
                size="small"
              />
            </BasicLabeledValue>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}

export default JobDetails
