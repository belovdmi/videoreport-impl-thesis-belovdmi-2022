import { Box, Chip, Grid, IconButton, Stack, Typography } from "@mui/material"
import ProfileLabeledValue from "../format/profile-labeled-value"
import ProfileIcon from "../../assets/profile-icon.png"
import { User } from "../../requests/data-contracts"
import regionNameFromAbbreviation from "../../utils/region-name-from-abbreviation"
import { Edit } from "@material-ui/icons"
import { Link } from "react-router-dom"

type ProfileDetailsProps = {
  data: User | undefined
  editButton?: boolean
}

const ProfileDetails = ({ data, editButton = false }: ProfileDetailsProps) => {
  return (
    <Grid container rowGap={2} p={3}>
      <Grid container display={"flex"} alignItems={"center"}>
        <Box pr={2} width={40}>
          <img src={ProfileIcon} alt="profile-icon" width={"100%"} />
        </Box>
        <Grid xs item>
          <Typography variant="h5" fontWeight={600}>
            {data?.username || "Unknown"}
          </Typography>
        </Grid>
        {editButton ? (
          <IconButton
            aria-label="close"
            size="large"
            component={Link}
            to={"/settings"}
          >
            <Edit fontSize="inherit" />
          </IconButton>
        ) : null}
      </Grid>
      <Grid item container rowGap={2} px={{ xs: 0, sm: 3 }}>
        <Grid xs={12} md={6} container item direction={"column"} rowGap={2}>
          <ProfileLabeledValue label={"Email:"} value={data?.email || "-"} />
          <ProfileLabeledValue
            label={"Telefoní číslo:"}
            value={data?.phoneNumber || "-"}
          />
        </Grid>
        <Grid xs container item>
          <ProfileLabeledValue
            label={"Popis:"}
            value={data?.description || "-"}
          />
        </Grid>
      </Grid>
      <Grid item container px={{ xs: 0, sm: 3 }} rowGap={2}>
        <Grid xs={12} md={6} item>
          <ProfileLabeledValue label={"Specializace:"}>
            <Stack spacing={2} direction="row">
              {data?.specializations?.length === 0
                ? "-"
                : data?.specializations?.map(item => {
                    return (
                      <Chip
                        label={item.name}
                        variant="outlined"
                        color="primary"
                      />
                    )
                  })}
            </Stack>
          </ProfileLabeledValue>
        </Grid>
        <Grid xs={12} md={6} item>
          <ProfileLabeledValue
            label={"Kraj:"}
            value={regionNameFromAbbreviation(data?.region)}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default ProfileDetails
