import { Box, Typography } from "@mui/material"
import { FC } from "react"

type BasicTitledContentProps = {
  title: string
}

const BasicTitledContent: FC<BasicTitledContentProps> = ({
  title,
  children,
}) => {
  return (
    <Box pt={2}>
      <Typography variant="h5" color={"primary"} pb={4}>
        {title}
      </Typography>
      <Box px={{ xs: 0, md: 5 }} pb={4}>
        {children}
      </Box>
    </Box>
  )
}

export default BasicTitledContent
