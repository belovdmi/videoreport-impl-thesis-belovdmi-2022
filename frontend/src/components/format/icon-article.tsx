import { SvgIconProps, Typography } from "@mui/material"
import { FC } from "react"

type IconArticleProps = {
  Icon: React.ReactElement<SvgIconProps>
  title: string
}
const IconArticle: FC<IconArticleProps> = ({ Icon, title, children }) => {
  return (
    <>
      <Typography variant="h1" textAlign={"center"} color="primary.main">
        {Icon}
      </Typography>
      <Typography variant="h4" textAlign={"center"}>
        {title}
      </Typography>
      <Typography
        variant="body1"
        sx={{ lineHeight: 2 }}
        maxWidth={500}
        m={"auto"}
        pb={4}
      >
        {children}
      </Typography>
    </>
  )
}

export default IconArticle
