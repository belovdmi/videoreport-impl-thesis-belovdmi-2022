import { Box, Typography } from "@mui/material"
import { FC } from "react"

type ProfileLabeledValueProps = {
  label: string
  value?: string
}
const ProfileLabeledValue: FC<ProfileLabeledValueProps> = ({
  label,
  value,
  children,
}) => {
  return (
    <Box>
      <Typography variant="body2" fontWeight={600} gutterBottom>
        {label}
      </Typography>
      {value && (
        <Typography variant="body2" gutterBottom>
          {value}
        </Typography>
      )}
      {children}
    </Box>
  )
}

export default ProfileLabeledValue
