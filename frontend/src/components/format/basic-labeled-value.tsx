import { Box, Typography } from "@mui/material"
import { FC } from "react"

type BasicLabeledValueProps = {
  label: string
  labelColor?: string
  value?: string
}
const BasicLabeledValue: FC<BasicLabeledValueProps> = ({
  label,
  labelColor,
  value,
  children,
}) => {
  return (
    <Box>
      <Typography
        variant="subtitle2"
        color={labelColor ?? "text.secondary"}
        component="div"
      >
        {label}
      </Typography>
      {value && (
        <Typography
          component="div"
          variant="body2"
          fontWeight={600}
          gutterBottom
          noWrap
        >
          {value}
        </Typography>
      )}
      {children}
    </Box>
  )
}

export default BasicLabeledValue
