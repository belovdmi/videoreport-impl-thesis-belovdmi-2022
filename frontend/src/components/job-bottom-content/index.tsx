import { ViewedJob } from "../../state/global"
import FinishedJob from "./finished-job"
import AssignedGivenJob from "./given/assigned-given-job"
import FreeGivenJob from "./given/free-given-job"
import InformationText from "./inforamtion-text"
import AssignedTakenJob from "./taken/assigned-taken-job"
import InProgressTakenJob from "./taken/in-progress-taken-job"
import SubscribedTakenJob from "./taken/subscribed-taken-job"

type JobBottomContentProps = {
  jobData: ViewedJob | undefined
}

/**
 * Component is displaying allowed actions for job based on logged user role in job and jobs status
 */
const JobBottomContent = ({ jobData }: JobBottomContentProps) => {
  if (!jobData) {
    return null
  }

  if (!jobData?.status) {
    return <InformationText>Práce byla smazána...</InformationText>
  }

  // creators
  if (jobData?.status === "free" && jobData?.loggedUserRole === "creator") {
    return <FreeGivenJob />
  }

  if (jobData?.status === "assigned" && jobData?.loggedUserRole === "creator") {
    return <AssignedGivenJob />
  }

  if (
    jobData?.status === "in_progress" &&
    jobData?.loggedUserRole === "creator"
  ) {
    return (
      <InformationText>
        Práce byla zadána a plnitel na ní pracuje.
      </InformationText>
    )
  }

  if (
    (jobData?.status === "finished" &&
      jobData?.loggedUserRole === "fulfiller") ||
    (jobData?.status === "finished" && jobData?.loggedUserRole === "creator")
  ) {
    return <FinishedJob />
  }

  // subscribers
  if (jobData?.status === "free" && jobData?.loggedUserRole === "subscriber") {
    return <SubscribedTakenJob />
  }

  if (jobData?.loggedUserRole === "subscriber") {
    return (
      <InformationText>
        Bohužel jste nebyl vybrán k plnění prace.
      </InformationText>
    )
  }

  // assigned
  if (
    jobData?.status === "assigned" &&
    jobData?.loggedUserRole === "assigned"
  ) {
    return <AssignedTakenJob />
  }

  // fulfiller
  if (
    jobData?.status === "in_progress" &&
    jobData?.loggedUserRole === "fulfiller"
  ) {
    return <InProgressTakenJob />
  }

  return <InformationText>Této práce se neučastníte...</InformationText>
}

export default JobBottomContent
