import {
  Box,
  Button,
  Grid,
  MenuItem,
  Select,
  SelectChangeEvent,
  Stack,
  Typography,
} from "@mui/material"
import axios from "axios"
import { useEffect, useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import { User } from "../../../requests/data-contracts"
import { Jobs, securedJob, SortSubsBy } from "../../../requests/Jobs"
import { execRefreshToken } from "../../../requests/refresh-token"
import {
  aAlert,
  aAuthToken,
  aErrorType,
  aLoading,
  aRefreshToken,
  aViewedJob,
} from "../../../state/global"
import getSpecializationsNames from "../../../utils/get-secializations-names"
import IconCard from "../../cards/icon-card"
import BasicButtonsContainer from "../../containers/basic-buttons-container"
import ForeignProfileDialog from "../../dialogs/foreign-profile-dialog"
import UpdateJobDialog from "../../dialogs/update-job-dialog"
import InformationText from "../inforamtion-text"

const FreeGivenJob = () => {
  const handleError = useErrorHandler()
  const [sortBy, setSortBy] = useState("")
  const [openUpdateWork, setOpenUpdateWork] = useState(false)
  const handleChange = (event: SelectChangeEvent) => {
    setSortBy(event.target.value)
  }
  const [profileDialog, setProfileDialog] = useState<{
    open: boolean
    user: User | undefined
  }>({ open: false, user: undefined })
  const [subscribers, setSubscribers] = useState<User[] | undefined>(undefined)
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const [viewedJob, setViewedJob] = useRecoilState(aViewedJob)

  useEffect(() => {
    const handleLoadSubs = async () => {
      try {
        setLoading(true)
        const api = new Jobs()
        const { data } = await api.jobGetSubscribers(viewedJob?.id || -1, {
          sortBy: sortBy ? (sortBy as SortSubsBy) : undefined,
        })

        setSubscribers(data)
      } catch (error) {
        if (axios.isAxiosError(error) && error.response?.status === 404) {
          return
        } else {
          handleError(error)
        }
      } finally {
        setLoading(false)
      }
    }

    handleLoadSubs()
  }, [handleError, setLoading, sortBy, viewedJob])

  const getNewToken = async () => {
    const newAuthToken = await execRefreshToken({
      authToken,
      refreshToken,
    })
    setAuthToken(newAuthToken)
    return newAuthToken
  }

  const processError = (error: unknown) => {
    if (error instanceof Error && error?.message === "refresh token expired") {
      setErrorType("logAgain")
    }

    if (axios.isAxiosError(error) && error.response?.status === 403) {
      setErrorType("forbidden")
    }

    handleError(error)
  }

  const handleCancel = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      await api.deleteJob(viewedJob?.id || -1)

      setAlert({
        alertMsg: "Práce byla úspěšne smazána.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.creator = undefined
        newVeiwedJob.status = undefined
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 404) {
        setAlert({
          alertMsg: "Nepodařilo se najít vámi vytvořenou práci.",
          severity: "error",
          open: true,
        })
        return
      }

      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleChooseSub = async (username: string) => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      await api.assignJob(viewedJob?.id || -1, {
        username,
      })

      setAlert({
        alertMsg:
          "Uživatel byl přiřazen k plnění práce. Praci naleznete v sekci Zadané práce kategorii Přiřazené.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.status = "assigned"
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      if (
        axios.isAxiosError(error) &&
        ((error.response?.status === 400 &&
          error.response.data === "job already assigned") ||
          error.response?.status === 404)
      ) {
        setAlert({
          alertMsg:
            "Zřejmě jste již někoho přiřadil. Znovu to udělat nemůžete.",
          severity: "success",
          open: true,
        })
        return
      }

      processError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <>
      <Grid container alignItems={"center"} pt={1} pb={5}>
        <Grid xs item>
          <Typography variant="h5" color={"primary"}>
            Nabídky
          </Typography>
        </Grid>
        <Select
          labelId="demo-simple-select-standard-label"
          id="demo-simple-select-standard"
          value={sortBy}
          variant="outlined"
          onChange={handleChange}
          sx={{
            width: 135,
            height: 30,
            textAlign: "center",
            fontSize: 13,
          }}
          displayEmpty
          MenuProps={{
            disableScrollLock: true,
          }}
          size="small"
        >
          <MenuItem value="">Řadit od</MenuItem>
          <MenuItem value={"score.desc"}>Nejvyššího skóre</MenuItem>
          <MenuItem value={"score.asc"}>Nejnižšího skóre</MenuItem>
          <MenuItem value={"numberOfReviews.desc"}>Nejvíc recenzí</MenuItem>
          <MenuItem value={"numberOfReviews.asc"}>Nejmíň recenzí</MenuItem>
        </Select>
      </Grid>

      <Stack px={{ xs: 0, md: 5 }} spacing={14}>
        {subscribers && subscribers.length ? (
          subscribers?.map((data: User) => {
            return (
              <Box display="flex" key={data.username}>
                <IconCard
                  title={data.username || "-"}
                  icon="profile"
                  dataList={[
                    {
                      name: "Specilizace:",
                      value:
                        getSpecializationsNames(data?.specializations) || "-",
                    },
                    {
                      name: "Skóre:",
                      value: data?.scoreSummary?.score?.toString() || "-",
                    },
                    {
                      name: "Počet hodnocení",
                      value:
                        data?.scoreSummary?.numberOfReviews?.toString() || "-",
                    },
                  ]}
                  onClick={() => setProfileDialog({ open: true, user: data })}
                />
                <Button
                  variant="contained"
                  onClick={() => handleChooseSub(data.username || "-")}
                >
                  Vybrat
                </Button>
              </Box>
            )
          })
        ) : (
          <InformationText color="secondary">
            O plnění práce zatím nikdo neprojevil zájem...
          </InformationText>
        )}
      </Stack>
      <Box py={"3%"}>
        <BasicButtonsContainer>
          <Button
            variant="contained"
            onClick={() => {
              setOpenUpdateWork(true)
            }}
          >
            Upravit zadaní
          </Button>
          <Button variant="contained" onClick={handleCancel}>
            Zrušit práci
          </Button>
        </BasicButtonsContainer>
        <UpdateJobDialog
          open={openUpdateWork}
          onClose={() => {
            setOpenUpdateWork(false)
          }}
        />
      </Box>
      <ForeignProfileDialog
        open={profileDialog.open}
        onClose={() => setProfileDialog({ open: false, user: undefined })}
        userData={profileDialog.user}
      />
    </>
  )
}

export default FreeGivenJob
