import axios from "axios"
import { useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useSetRecoilState, useRecoilValue } from "recoil"
import { User } from "../../../requests/data-contracts"
import { Jobs } from "../../../requests/Jobs"
import { aLoading, aViewedJob } from "../../../state/global"
import InformationText from "../inforamtion-text"

const AssignedGivenJob = () => {
  const handleError = useErrorHandler()
  const [assignee, setAssignee] = useState<User | undefined>(undefined)
  const setLoading = useSetRecoilState(aLoading)
  const viewedJob = useRecoilValue(aViewedJob)

  useEffectOnce(() => {
    const handleLoadAssignee = async () => {
      try {
        setLoading(true)
        const api = new Jobs()
        const { data } = await api.getAssignee(viewedJob?.id || -1)
        setAssignee(data)
      } catch (error) {
        if (axios.isAxiosError(error) && error.response?.status === 404) {
          return
        } else {
          handleError(error)
        }
      } finally {
        setLoading(false)
      }
    }

    handleLoadAssignee()
  })

  return (
    <InformationText>
      Práci jeste zadal uživateli: <b>{assignee?.username}</b>. Čeká se na jeho
      potvrzení.
    </InformationText>
  )
}

export default AssignedGivenJob
