import { Typography } from "@mui/material"
import { FC } from "react"

type InformationTextProps = {
  color?: string
}

const InformationText: FC<InformationTextProps> = ({
  color = "primary",
  children,
}) => {
  return (
    <Typography variant="h4" color={color} textAlign={"center"} py={"7%"}>
      {children}
    </Typography>
  )
}

export default InformationText
