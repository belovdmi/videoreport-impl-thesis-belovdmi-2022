import { Send } from "@material-ui/icons"
import { Box, Button, Divider, Typography } from "@mui/material"
import axios from "axios"
import { useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useForm } from "react-hook-form"
import { useEffectOnce } from "react-use"
import { useSetRecoilState, useRecoilValue, useRecoilState } from "recoil"
import { securedJob } from "../../../requests/Jobs"
import { execRefreshToken } from "../../../requests/refresh-token"
import {
  aLoading,
  aErrorType,
  aAlert,
  aRefreshToken,
  aAuthToken,
  aViewedJob,
} from "../../../state/global"
import BasicButtonsContainer from "../../containers/basic-buttons-container"
import BasicTitledContent from "../../format/basic-titled-content"

// cannot be imported from video-react (@types/video-react dont exists)
// eslint-disable-next-line @typescript-eslint/no-var-requires
const videoReact = require("video-react")

const InProgressTakenJob = () => {
  const handleError = useErrorHandler()
  const { handleSubmit, register, watch } = useForm()

  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const [viewedJob, setViewedJob] = useRecoilState(aViewedJob)
  const [videoId, setVideoId] = useState<number | undefined>(undefined)
  const [url, setUrl] = useState<string | undefined>(undefined)

  const getNewToken = async () => {
    const newAuthToken = await execRefreshToken({
      authToken,
      refreshToken,
    })
    setAuthToken(newAuthToken)
    return newAuthToken
  }

  const processError = (error: unknown) => {
    if (axios.isAxiosError(error) && error.response?.status === 404) {
      return
    }
    if (error instanceof Error && error?.message === "refresh token expired") {
      setErrorType("logAgain")
    }

    if (axios.isAxiosError(error) && error.response?.status === 403) {
      setErrorType("forbidden")
    }

    handleError(error)
  }

  const handleLoadVideo = async (loadVideoId: number | undefined) => {
    try {
      setLoading(true)
      setUrl(undefined)
      if (loadVideoId === undefined) {
        return
      }

      const api = securedJob
      api.setSecurityData(await getNewToken())

      const { data } = await api.getVideoResult(viewedJob?.id || 1, loadVideoId)

      setUrl(data.url)
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 404) {
        return
      }

      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleLoadVideoId = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      const { data } = await api.getResults(viewedJob?.id || 1)

      const getVideoId =
        data.videos && data.videos?.length ? data.videos[0] : undefined

      setVideoId(getVideoId)

      await handleLoadVideo(getVideoId)
    } catch (error) {
      if (axios.isAxiosError(error) && error.response?.status === 404) {
        return
      }

      processError(error)
    } finally {
      setLoading(false)
    }
  }

  useEffectOnce(() => {
    handleLoadVideoId()
  })

  const watchVideo = watch("video")

  const onSubmit = async (data: File) => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      if (videoId) {
        await api.updateVideoResult(viewedJob?.id || -1, videoId, {
          video: data,
        })
        await handleLoadVideo(videoId)
      } else {
        await api.createVideoResult(viewedJob?.id || -1, { video: data })
        await handleLoadVideoId()
      }

      return
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleCancel = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      await api.declineFulfillment(viewedJob?.id || -1)

      setAlert({
        alertMsg: "Práce byla vypovězena.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.status = "free"
        newVeiwedJob.loggedUserRole = undefined
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleResultSubmit = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      await api.submitResults(viewedJob?.id || -1)

      setAlert({
        alertMsg: "Práce byla odeslána.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.status = "finished"
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      if (
        axios.isAxiosError(error) &&
        error.response?.status === 400 &&
        error.response?.data === "result not present"
      ) {
        setAlert({
          alertMsg:
            "Abyste mohl odeslat práci musíte nejdříve nahrát výsledky.",
          severity: "error",
          open: true,
        })
        return
      }

      processError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Box>
      <Box pb={5}>
        <form onSubmit={handleSubmit(data => onSubmit(data.video[0]))}>
          <BasicButtonsContainer>
            <Button variant="contained" component="label">
              Vybrat video
              <input
                {...register("video")}
                required
                id="upload-video"
                type="file"
                accept="video/*"
                name="video"
                hidden
              />
            </Button>
            <Typography textAlign={"center"}>
              <b>Vybraný soubor:</b>{" "}
              {watchVideo && watchVideo[0]?.name ? watchVideo[0]?.name : "-"}
            </Typography>
            <Button variant="contained" endIcon={<Send />} type="submit">
              Nahrát
            </Button>
          </BasicButtonsContainer>
        </form>
      </Box>

      <Divider />
      <BasicTitledContent title={"Výsledné video:"}>
        <Box maxWidth={1000} margin={"auto"} pb={4} textAlign={"center"}>
          {url ? (
            <videoReact.Player>
              <source src={url} />
              <videoReact.BigPlayButton position="center" />
            </videoReact.Player>
          ) : (
            <Typography variant="h3">Zatím jste nic nenahrál...</Typography>
          )}
        </Box>
      </BasicTitledContent>

      <Divider />
      <Box py={2}>
        <BasicButtonsContainer>
          <Button variant="contained" onClick={handleCancel}>
            Vypovědět plnění
          </Button>
          <Button variant="contained" onClick={handleResultSubmit}>
            Odeslat práci
          </Button>
        </BasicButtonsContainer>
      </Box>
    </Box>
  )
}

export default InProgressTakenJob
