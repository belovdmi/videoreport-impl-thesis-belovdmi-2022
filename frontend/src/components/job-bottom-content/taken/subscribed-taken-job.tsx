import { Box, Button } from "@mui/material"
import axios from "axios"
import { useErrorHandler } from "react-error-boundary"
import { useSetRecoilState, useRecoilValue, useRecoilState } from "recoil"
import { securedJob } from "../../../requests/Jobs"
import { execRefreshToken } from "../../../requests/refresh-token"
import {
  aLoading,
  aErrorType,
  aAlert,
  aRefreshToken,
  aAuthToken,
  aViewedJob,
  aLoggedUsername,
} from "../../../state/global"
import BasicButtonsContainer from "../../containers/basic-buttons-container"

const SubscribedTakenJob = () => {
  const handleError = useErrorHandler()
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const [viewedJob, setViewedJob] = useRecoilState(aViewedJob)

  return (
    <Box pt={{ sm: 0, lg: "5%" }}>
      <BasicButtonsContainer>
        <Button
          variant="contained"
          onClick={async () => {
            try {
              setLoading(true)

              const newAuthToken = await execRefreshToken({
                authToken,
                refreshToken,
              })
              setAuthToken(newAuthToken)

              const api = securedJob
              api.setSecurityData(newAuthToken)

              if (loggedUsername) {
                await api.jobDeleteSubscriber(
                  viewedJob?.id || -1,
                  loggedUsername
                )
              } else {
                throw new Error("user not logged")
              }

              setAlert({
                alertMsg: "Byl jste úspěšně odshlášen od odběru.",
                severity: "success",
                open: true,
              })

              if (viewedJob) {
                const newVeiwedJob = { ...viewedJob }
                newVeiwedJob.status = "free"
                newVeiwedJob.loggedUserRole = undefined
                setViewedJob(newVeiwedJob)
              }
            } catch (error) {
              if (
                error instanceof Error &&
                error?.message === "refresh token expired"
              ) {
                setErrorType("logAgain")
              }

              if (axios.isAxiosError(error) && error.response?.status === 403) {
                setErrorType("forbidden")
              }

              handleError(error)
            } finally {
              setLoading(false)
            }
          }}
        >
          Přestat odebírat
        </Button>
      </BasicButtonsContainer>
    </Box>
  )
}

export default SubscribedTakenJob
