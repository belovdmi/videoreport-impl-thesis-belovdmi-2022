import { Box, Button } from "@mui/material"
import axios from "axios"
import { useErrorHandler } from "react-error-boundary"
import { useSetRecoilState, useRecoilValue, useRecoilState } from "recoil"
import { securedJob } from "../../../requests/Jobs"
import { execRefreshToken } from "../../../requests/refresh-token"
import { Users } from "../../../requests/Users"
import {
  aLoading,
  aErrorType,
  aAlert,
  aRefreshToken,
  aAuthToken,
  aViewedJob,
  aLoggedUsername,
} from "../../../state/global"
import BasicButtonsContainer from "../../containers/basic-buttons-container"

const AssignedTakenJob = () => {
  const handleError = useErrorHandler()
  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const setAlert = useSetRecoilState(aAlert)
  const refreshToken = useRecoilValue(aRefreshToken)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const [viewedJob, setViewedJob] = useRecoilState(aViewedJob)
  const loggedUsername = useRecoilValue(aLoggedUsername)

  const getNewToken = async () => {
    const newAuthToken = await execRefreshToken({
      authToken,
      refreshToken,
    })
    setAuthToken(newAuthToken)
    return newAuthToken
  }

  const processError = (error: unknown) => {
    if (error instanceof Error && error?.message === "refresh token expired") {
      setErrorType("logAgain")
    }

    if (axios.isAxiosError(error) && error.response?.status === 403) {
      setErrorType("forbidden")
    }

    handleError(error)
  }

  const handelCancelation = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())
      await api.declineAssigment(viewedJob?.id || -1)

      setAlert({
        alertMsg: "Plnění práce bylo odmítnuto.",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.status = "free"
        newVeiwedJob.loggedUserRole = undefined
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleAccept = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      await api.acceptFulfillment(viewedJob?.id || -1)

      setAlert({
        alertMsg:
          "Plnění práce bylo přijato. Praci naleznete v sekci Vzaté práce kategorii Probíhající. ",
        severity: "success",
        open: true,
      })

      if (viewedJob) {
        const newVeiwedJob = { ...viewedJob }
        newVeiwedJob.status = "in_progress"
        newVeiwedJob.loggedUserRole = "fulfiller"

        const userApi = new Users()
        const { data } = await userApi.getUserByName(loggedUsername)

        newVeiwedJob.fulfiller = data
        setViewedJob(newVeiwedJob)
      }
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Box pt={{ sm: 0, lg: "5%" }}>
      <BasicButtonsContainer>
        <Button variant="contained" onClick={handelCancelation}>
          Vypovědět plnění
        </Button>
        <Button variant="contained" onClick={handleAccept}>
          Příjmout plnění
        </Button>
      </BasicButtonsContainer>
    </Box>
  )
}

export default AssignedTakenJob
