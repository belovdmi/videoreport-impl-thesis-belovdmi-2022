import { Box, Button, Divider } from "@mui/material"
import axios from "axios"
import { useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useSetRecoilState, useRecoilValue, useRecoilState } from "recoil"
import useLoadReviews from "../../hooks/use-load-reviews"
import { securedJob } from "../../requests/Jobs"
import { execRefreshToken } from "../../requests/refresh-token"
import {
  aLoading,
  aErrorType,
  aRefreshToken,
  aAuthToken,
  aViewedJob,
  aLoggedUsername,
} from "../../state/global"
import BasicButtonsContainer from "../containers/basic-buttons-container"
import ReviewDialog from "../dialogs/review-dialog"
import BasicTitledContent from "../format/basic-titled-content"

// cannot import from video-react (@types/video-react dont exists)
// eslint-disable-next-line @typescript-eslint/no-var-requires
const videoReact = require("video-react")

const FinishedJob = () => {
  const handleError = useErrorHandler()

  const setLoading = useSetRecoilState(aLoading)
  const setErrorType = useSetRecoilState(aErrorType)
  const refreshToken = useRecoilValue(aRefreshToken)
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const [authToken, setAuthToken] = useRecoilState(aAuthToken)
  const viewedJob = useRecoilValue(aViewedJob)
  const [url, setUrl] = useState<string | undefined>(undefined)
  const [openReviewDialog, setOpenReviewDialog] = useState<boolean>(false)

  const { reviews } = useLoadReviews(loggedUsername, true)

  const getNewToken = async () => {
    const newAuthToken = await execRefreshToken({
      authToken,
      refreshToken,
    })
    setAuthToken(newAuthToken)
    return newAuthToken
  }

  const processError = (error: unknown) => {
    if (axios.isAxiosError(error) && error.response?.status === 404) {
      return
    }
    if (error instanceof Error && error?.message === "refresh token expired") {
      setErrorType("logAgain")
    }

    if (axios.isAxiosError(error) && error.response?.status === 403) {
      setErrorType("forbidden")
    }

    handleError(error)
  }

  const handleLoadVideo = async (loadVideoId: number | undefined) => {
    try {
      setLoading(true)

      if (loadVideoId === undefined) {
        return
      }

      const api = securedJob
      api.setSecurityData(await getNewToken())

      const { data } = await api.getVideoResult(viewedJob?.id || 1, loadVideoId)

      setUrl(data.url)
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  const handleLoadVideoId = async () => {
    try {
      setLoading(true)

      const api = securedJob
      api.setSecurityData(await getNewToken())

      const { data } = await api.getResults(viewedJob?.id || 1)

      const getVideoId =
        data.videos && data.videos?.length ? data.videos[0] : undefined

      await handleLoadVideo(getVideoId)
    } catch (error) {
      processError(error)
    } finally {
      setLoading(false)
    }
  }

  useEffectOnce(() => {
    handleLoadVideoId()
  })

  return (
    <>
      <BasicTitledContent title={"Výsledné video:"}>
        <Box maxWidth={1000} margin={"auto"} textAlign={"center"}>
          {url ? (
            <videoReact.Player aspectRatio={"16:9"}>
              <source src={url} />
              <videoReact.BigPlayButton position="center" />
            </videoReact.Player>
          ) : (
            "-"
          )}
        </Box>
      </BasicTitledContent>
      <Divider sx={{ pt: 1, mb: 2 }} />
      <BasicButtonsContainer>
        <Button
          variant="contained"
          onClick={() => {
            setOpenReviewDialog(true)
          }}
          disabled={
            viewedJob?.reviewed ||
            (reviews &&
              reviews?.some(revData => {
                return revData.jobId === viewedJob?.id
              }))
          }
        >
          Ohodnotit protistranu
        </Button>
      </BasicButtonsContainer>
      <ReviewDialog
        open={openReviewDialog}
        jobId={viewedJob?.id || -1}
        username={
          viewedJob?.creator?.username === loggedUsername
            ? viewedJob?.fulfiller?.username || "-"
            : viewedJob?.creator?.username || "-"
        }
        onClose={() => {
          setOpenReviewDialog(false)
        }}
      />
    </>
  )
}

export default FinishedJob
