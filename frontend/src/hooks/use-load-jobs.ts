/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { useEffect, useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import { Job } from "../requests/data-contracts"
import { GetJobQuery, Jobs, Role, Status } from "../requests/Jobs"
import {
  aCleanQuery,
  aLoading,
  aLoggedUsername,
  aOffersQuery,
} from "../state/global"

type UseLoadJobsReturn = { jobs: Job[] | undefined; totalCount: number }

/**
 * hook for loading jobs
 * @param  {boolean} loggedUserJobs is indication that job of logged user should be loaded
 * @param  {Role} userRole is role of user in job
 * @param  {Status} jobStatus is status of job
 * @return {UseLoadJobsReturn} returns jobs (if loaded)
 */
const useLoadJobs = (
  loggedUserJobs?: boolean,
  userRole?: Role,
  jobStatus?: Status
): UseLoadJobsReturn => {
  const handleError = useErrorHandler()
  const [totalCount, setTotalCount] = useState(0)
  const [cleaned, setCleaned] = useState(false)
  const [jobs, setJobs] = useState<Job[] | undefined>([])
  const [offersQuery, setOffersQuery] = useRecoilState(aOffersQuery)
  const [cleanQuery, setCleanQuery] = useRecoilState(aCleanQuery)
  const setLoading = useSetRecoilState(aLoading)
  const loggedUsername = useRecoilValue(aLoggedUsername)

  // at start it resets query
  useEffectOnce(() => {
    const newOffersQuery: GetJobQuery = {
      sortBy: "created.desc",
      page: 1,
      count: 12,
      priceGte: undefined,
      priceLte: undefined,
      dueDateGte: undefined,
      dueDateLte: undefined,
      region: undefined,
      username: undefined,
      category: undefined,
      status: "free",
    }

    if (loggedUserJobs) {
      newOffersQuery.username = loggedUsername
    }

    if (jobStatus) {
      newOffersQuery.status = jobStatus
    }

    if (userRole) {
      newOffersQuery.role = userRole
    }

    if (cleanQuery) {
      setOffersQuery(newOffersQuery)
    }

    setCleanQuery(true)

    setCleaned(true)
  })

  useEffect(() => {
    const handleLoadJobs = async () => {
      try {
        setLoading(true)

        const api = new Jobs()

        const { data } = await api.getJobs(offersQuery)
        setJobs(data?.jobs)
        setTotalCount(data?.totalCount || 0)
      } catch (error) {
        handleError(error)
      } finally {
        setLoading(false)
      }
    }
    if (cleaned) {
      handleLoadJobs()
    }
  }, [cleaned, handleError, offersQuery, setLoading])

  return { jobs, totalCount }
}

export default useLoadJobs
