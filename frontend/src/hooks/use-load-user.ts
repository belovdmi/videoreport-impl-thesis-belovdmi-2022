import axios from "axios"
import { useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useSetRecoilState } from "recoil"
import { User } from "../requests/data-contracts"
import { Users } from "../requests/Users"
import { aLoading } from "../state/global"

type UseLoadUserReturn = User | undefined

/**
 * hook for loading user by name
 * @param  {string} username is name of user will be loaded
 * @return {UseLoadUserReturn} returns user (if loaded)
 */
const useLoadUser = (username: string): UseLoadUserReturn => {
  const handleError = useErrorHandler()
  const setLoading = useSetRecoilState(aLoading)
  const [userData, setUserData] = useState<UseLoadUserReturn>(undefined)

  useEffectOnce(() => {
    const handleLoadUser = async () => {
      try {
        setLoading(true)
        const api = new Users()
        const { data } = await api.getUserByName(username)
        setUserData(data)
      } catch (error) {
        if (axios.isAxiosError(error) && error.response?.status === 404) {
          return
        }
        handleError(error)
      } finally {
        setLoading(false)
      }
    }

    handleLoadUser()
  })

  return userData
}

export default useLoadUser
