import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useRecoilValue, useSetRecoilState } from "recoil"
import {
  aAuthToken,
  aErrorType,
  aLogged,
  aLoggedUsername,
  aRefreshToken,
} from "../state/global"

// checks if user if logged in
const useAuthorizeUser = () => {
  const handleError = useErrorHandler()
  const setErrorType = useSetRecoilState(aErrorType)
  const refreshToken = useRecoilValue(aRefreshToken)
  const authToken = useRecoilValue(aAuthToken)
  const logged = useRecoilValue(aLogged)
  const loggedUsername = useRecoilValue(aLoggedUsername)

  useEffectOnce(() => {
    if (!refreshToken || !authToken || !logged || !loggedUsername) {
      setErrorType("unauthorized")
      handleError("unauthorized")
    }
  })
}

export default useAuthorizeUser
