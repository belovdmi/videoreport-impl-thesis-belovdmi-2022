/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { useEffect, useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useEffectOnce } from "react-use"
import { useRecoilValue, useResetRecoilState, useSetRecoilState } from "recoil"
import { Review } from "../requests/data-contracts"
import { Users } from "../requests/Users"
import { aLoading, aReviewsQuery } from "../state/global"

type UseLoadReviewsReturn = {
  reviews: Review[] | undefined
  totalCount: number
}

/**
 * hook for loading reviews
 * @param  {string} username is name of user that is in review
 * @param  {boolean} givenReviews is indication that user reviews that user has given should be loaded
 * @return {UseLoadReviewsReturn} returns reviews (if loaded)
 */
const useLoadReviews = (
  username: string,
  givenReviews?: boolean
): UseLoadReviewsReturn => {
  const handleError = useErrorHandler()
  const [totalCount, setTotalCount] = useState(0)
  const [cleaned, setCleaned] = useState(false)
  const [reviews, setReviews] = useState<Review[] | undefined>([])
  const setLoading = useSetRecoilState(aLoading)
  const reviewsQuery = useRecoilValue(aReviewsQuery)
  const resetReviewsQuery = useResetRecoilState(aReviewsQuery)

  // first clean query
  useEffectOnce(() => {
    resetReviewsQuery()
    setCleaned(true)
  })

  useEffect(() => {
    const handleLoadReviews = async () => {
      try {
        setLoading(true)

        const api = new Users()

        const { data } = await api.getUserReviews(
          username,
          givenReviews ? { role: "reviewer" } : reviewsQuery
        )
        setReviews(data?.reviews)

        setTotalCount(data?.totalCount || 0)
      } catch (error) {
        handleError(error)
      } finally {
        setLoading(false)
      }
    }
    if (cleaned) {
      handleLoadReviews()
    }
  }, [cleaned, handleError, givenReviews, reviewsQuery, setLoading, username])

  return { reviews, totalCount }
}

export default useLoadReviews
