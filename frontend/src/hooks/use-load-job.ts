import axios from "axios"
import { useEffect, useState } from "react"
import { useErrorHandler } from "react-error-boundary"
import { useNavigate } from "react-router-dom"
import { useRecoilValue, useSetRecoilState } from "recoil"
import { Job, User } from "../requests/data-contracts"
import { Jobs, Role } from "../requests/Jobs"
import { aLoading, aLoggedUsername, aViewedJob } from "../state/global"

type UseLoadJobReturn = {
  job: Job | undefined
  creator: User | undefined
  fulfiller: User | undefined
}

/**
 * hook for loading job by id
 * @param  {number} jobId is id of job
 * @return {UseLoadJobReturn} returns job data (if loaded)
 */
const useLoadJob = (jobId: number): UseLoadJobReturn => {
  const handleError = useErrorHandler()
  const navigate = useNavigate()
  const loggedUsername = useRecoilValue(aLoggedUsername)
  const setLoading = useSetRecoilState(aLoading)
  const setViewedJob = useSetRecoilState(aViewedJob)
  const [job, setJob] = useState<Job | undefined>(undefined)
  const [creator, setCreator] = useState<User | undefined>(undefined)
  const [fulfiller, setFulfiller] = useState<User | undefined>(undefined)

  // loads job and checks what role is logged user in it... also loads creator and fulfiller
  useEffect(() => {
    const handleLoadJob = async () => {
      try {
        setViewedJob(undefined)

        setLoading(true)

        const api = new Jobs()

        const jobRes = await api.getJobById(jobId)
        setJob(jobRes.data)

        let loggedUserRole: Role | undefined = undefined
        const creatorRes = await api.getJobsCreator(jobId)
        setCreator(creatorRes.data)

        if (creatorRes.data.username === loggedUsername) {
          loggedUserRole = "creator"
        }

        if (jobRes.data.status === "assigned" && !loggedUserRole) {
          try {
            const assigneeRes = await api.getAssignee(jobId)

            if (assigneeRes.data.username === loggedUsername) {
              loggedUserRole = "assigned"
            }
          } catch (error) {
            if (axios.isAxiosError(error) && error.response?.status !== 404) {
              handleError(error)
            }
          }
        }

        if (jobRes.data.status === "free" && !loggedUserRole) {
          try {
            const subscribersRes = await api.jobGetSubscribers(jobId)

            if (
              subscribersRes.data.some(sub => {
                return sub.username === loggedUsername
              })
            ) {
              loggedUserRole = "subscriber"
            }
          } catch (error) {
            if (axios.isAxiosError(error) && error.response?.status !== 404) {
              handleError(error)
            }
          }
        }

        let fulfillerUser: User | undefined
        if (
          jobRes.data.status === "in_progress" ||
          jobRes.data.status === "finished"
        ) {
          try {
            const fulfillerRes = await api.getJobsFulfiller(jobId)
            setFulfiller(fulfillerRes.data)

            if (fulfillerRes.data.username === loggedUsername) {
              loggedUserRole = "fulfiller"
            }
            fulfillerUser = fulfillerRes.data
          } catch (error) {
            if (axios.isAxiosError(error) && error.response?.status !== 404) {
              handleError(error)
            }
          }
        }
        setViewedJob({
          ...jobRes.data,
          creator: creatorRes.data,
          fulfiller: fulfillerUser,
          loggedUserRole,
          reviewed: false,
        })
      } catch (error) {
        if (axios.isAxiosError(error) && error.response?.status === 404) {
          navigate("/page-not-found")
        } else {
          handleError(error)
        }
      } finally {
        setLoading(false)
      }
    }

    handleLoadJob()
  }, [handleError, jobId, loggedUsername, navigate, setLoading, setViewedJob])

  return { job, creator, fulfiller }
}

export default useLoadJob
