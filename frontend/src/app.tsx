import { ErrorBoundary } from "react-error-boundary"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import Error from "./pages/error"
import FrogotPassword from "./pages/forgot-password"
import Login from "./pages/login"
import MyProfile from "./pages/my-profile"
import Offers from "./pages/offers"
import Register from "./pages/register"
import About from "./pages/about"
import MyWorks from "./pages/my-works"
import Settings from "./pages/settings"
import Wrok from "./pages/work"
import Offer from "./pages/offer"
import ResetPassword from "./pages/reset-password"
import ErrorTemplate from "./components/error/error-template"

const App = () => {
  const errorHandler = (error: Error, info: Record<string, string>) => {
    // connect endpoint to frontend for logging
    console.log(error, info)
  }

  return (
    <Router>
      <Routes>
        <Route
          path="/login"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Login />
            </ErrorBoundary>
          }
        />
        <Route
          path="/register"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Register />
            </ErrorBoundary>
          }
        />
        <Route
          path="/forgot"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <FrogotPassword />
            </ErrorBoundary>
          }
        />
        <Route
          path="/reset-password"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <ResetPassword />
            </ErrorBoundary>
          }
        />
        <Route
          path="/offers"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Offers />
            </ErrorBoundary>
          }
        />
        <Route
          path="/my-profile"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <MyProfile />
            </ErrorBoundary>
          }
        />
        <Route
          path="/my-works"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <MyWorks />
            </ErrorBoundary>
          }
        />
        <Route
          path="/settings"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Settings />
            </ErrorBoundary>
          }
        />
        <Route
          path="/my-works/:jobId"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Wrok />
            </ErrorBoundary>
          }
        />
        <Route
          path="/offers/:jobId"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <Offer />
            </ErrorBoundary>
          }
        />
        <Route
          path="/"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <About />
            </ErrorBoundary>
          }
        />
        <Route
          // this is 404 page, * means everything else
          path="*"
          element={
            <ErrorBoundary onError={errorHandler} FallbackComponent={Error}>
              <ErrorTemplate title="Stránku se nám bohužel nepodařilo najít...">
                Bohužel se nám nepodařilo vámi hledanou stránku najít... pokud
                si myslite že hledaná stranka existuje zkuste to prosím později
                nebo kotaktujte administrátora prostřednictvím emailu.
              </ErrorTemplate>
            </ErrorBoundary>
          }
        />
      </Routes>
    </Router>
  )
}

export default App
