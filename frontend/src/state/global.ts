import { AlertColor } from "@mui/material"
import { atom } from "recoil"
import { Job, Review, User } from "../requests/data-contracts"
import { GetJobQuery, Role } from "../requests/Jobs"
import { GetUserReviewsQuery } from "../requests/Users"

/**
 * Indicates that request in loading / pending.
 */
export const aLoading = atom<boolean>({
  key: "aLoading",
  default: false,
})

/**
 * Stores recived refresh token after login. Refresh token is used to get new auth token.
 */
export const aRefreshToken = atom<string>({
  key: "aRefreshToken",
  default: "",
})

/**
 * Stores received auth token. Is needed to preform requests that require authorization and authentication.
 */
export const aAuthToken = atom<string>({
  key: "aAuthToken",
  default: "",
})

/**
 * Indicates that user successfully passed login.
 */
export const aLogged = atom<boolean>({
  key: "aLogged",
  default: false,
})

/**
 * Stores username of currentlly logged user.
 */
export const aLoggedUsername = atom<string>({
  key: "aLoggedUsername",
  default: "",
})

type ErrorType =
  | "general"
  | "forbidden"
  | "logAgain"
  | "unauthorized"
  | "resetInvalidLink"
  | "blocked"

/**
 * Stores type of error that occurred.
 */
export const aErrorType = atom<ErrorType>({
  key: "aErrorType",
  default: "general",
})

type Alert = {
  alertMsg: string
  severity: AlertColor
  open: boolean
}

/**
 * Stores alert data. Severity is color of alert. If open is true, an alert will pop up. After 5 sec, alert will despite.
 */
export const aAlert = atom<Alert>({
  key: "aAlert",
  default: { alertMsg: "Alert !", severity: "success", open: false },
})

/**
 * Stores data of query for getJobs call.
 */
export const aOffersQuery = atom<GetJobQuery>({
  key: "aOffersQuery",
  default: {
    sortBy: "created.desc",
    page: 1,
    count: 12,
    priceGte: undefined,
    priceLte: undefined,
    dueDateGte: undefined,
    dueDateLte: undefined,
    region: undefined,
    username: undefined,
    category: undefined,
    status: "free",
  },
})

export type ViewedJob = Job & {
  loggedUserRole: Role | undefined
  creator: User | undefined
  fulfiller: User | undefined
  reviewed: boolean
}

/**
 * Stores data of currently viewed job.
 */
export const aViewedJob = atom<ViewedJob | undefined>({
  key: "aViewedJob",
  default: undefined,
})

/**
 * Stores data of query for getReviews call.
 */
export const aReviewsQuery = atom<GetUserReviewsQuery>({
  key: "aReviewsQuery",
  default: {
    sortBy: "created.desc",
    page: 1,
    count: 5,
    role: "reciver",
  },
})

/**
 * Indicates that query should be cleaned after transferring to another page.
 */
export const aCleanQuery = atom<boolean>({
  key: "aCleanQuery",
  default: true,
})

export const aGivenReviews = atom<Review[] | undefined>({
  key: "aGivenReviews",
  default: undefined,
})
