# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:8080](https://localhost:8080) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## About project

The thesis focuses on the development of a web portal prototype. First, the requirements for the prototype are determined using competition analysis. Subsequently, using software engineering methods, a suitable solution is designed to meet the requirements. Both the technologies used in the implementation and the necessary prototype functional are designed. After the design, I will demonstrate the use of the most important technologies in the implementation of the prototype. The entire implementation is written in JavaScript using TypeScript and other tools and libraries like Express and React. The finished implementation is then tested and the shortcomings of the implemented prototype are determined.

The result is a functional prototype fulfilling the identified requirements.

## Before npm run...

Before you start installing, ensure that you have npm a node. Npm should be at least at version 8.1. And node at least at 17.0.0. Before starting the application, please install dependencies with the following command:

```sh
npm install
```

## Requests

Application should be run with backend on port 3000. If backend and frontend is not running in docker-compose, change proxy from https://backend:3000/ to https://localhost:3000/. As client for communication, backend app is using Axios.

Axios client is generated from swagger file. To generate client run command:

```sh
npx swagger-typescript-api --axios --modular --extract-request-body -p ../backend/src/swagger/swagger.json
```

## HTTPS

Because frontend is using https with self-signed certs, chrome and other browsers will display a warning "Your connection is not private". Just click on advanced and then proceed.


## Main tools and technologies

- axios -> https://axios-http.com/docs/intro
- recoil -> https://recoiljs.org/
- TypeScript -> https://www.typescriptlang.org/
- React -> https://reactjs.org/
- eslint -> https://eslint.org/
- Material UI -> https://mui.com/
- react-hook-form -> https://react-hook-form.com/
- react-use -> https://www.npmjs.com/package/react-use
- react-router-dom -> https://v5.reactrouter.com/web/guides/quick-start
- swagger-typescript-api -> https://www.npmjs.com/package/swagger-typescript-api
- prettier -> https://prettier.io/
