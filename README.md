## About project

The thesis focuses on the development of a web portal prototype. First, the requirements for the prototype are determined using competition analysis. Subsequently, using software engineering methods, a suitable solution is designed to meet the requirements. Both the technologies used in the implementation and the necessary prototype functional are designed. After the design, I will demonstrate the use of the most important technologies in the implementation of the prototype. The entire implementation is written in JavaScript using TypeScript and other tools and libraries like Express and React. The finished implementation is then tested and the shortcomings of the implemented prototype are determined.

The result is a functional prototype fulfilling the identified requirements.

## Project structure

Project is divided to backend and frontend. You can run both separately, but I strongly advise you to use docker as it saves time. You don't need to configure anything, just install docker a run two commands, after that all necessary things will be installed. So you don't need to install Redis and PostgreSQL by your self. This approach is for good for fast preview of application. But for development I suggest running frontend and backend separately because then after each change (save) while running server it will be restarted for the changes to take effect. (This is not happening with docker !)
## Docker

This project is dockerized. To run commands for docker, you first need to install it. Docker needs to be installed with docker-compose. For installation, please use following guide: https://docs.docker.com/get-docker/

## Setup

After installing docker, the only other thing needed is npm to run docker command from your terminal. Please use at least version 8.1.

## Commands

For docker, I created the following commands that are preformed via npm. Before running commands, make sure that you installed docker and docker is up and running (Important ! If you are using Docker Desktop in w10, make sure the docker application is open or running at background). If you have everything ready, you can preform the following commands:

to install need dependencies (use before npm run docker:up):
```sh
npm run docker:build
```
to start images:
```sh
npm run docker:up
```
to stop images:
```sh
npm run docker:down
```
After starting, you can access backend from https://localhost:3000/ and frontend from https://localhost:8080/ . Backend has interactive swagger API documentation available from: https://localhost:3000/api-docs . Both backend and frontend are using HTTPS protocol with self-signed certs. And because of that, browser will display warning "Your connection is not private" after trying to access https://localhost:3000/ or https://localhost:8080/. Just click on advanced and then proceed. Too change (save) take effect stop and run images again.

## After start

After first start database will be initialized with basic categories and user with username: admin and password: Admin123. If you want to reset database (delete all data) then delete contents of directory postgresgl. (But not directory itself !!!)

## Main tools and technologies

All main tools and technologies are stated in backend and frontend READMEs.