import express from "express"
import { body } from "express-validator"
import AdminsController from "../controllers/admins.controller"
import authenticateToken from "../middlewares/authenticate-token.middleware"
import authorizeAdmin from "../middlewares/authorize-admin.middleware"
import validateRequest from "../middlewares/validate-request.middleware"
import {
  BlockUserRequired,
  GiveAdminRightsRequired,
} from "../services/admins-service.data-types"

const adminsRouter = express.Router()
const adminsController = new AdminsController()

adminsRouter.post(
  "/",
  body(GiveAdminRightsRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  authorizeAdmin,
  adminsController.giveAdminRights
)

adminsRouter.delete(
  "/:username",
  authenticateToken,
  authorizeAdmin,
  adminsController.removeAdminRights
)

adminsRouter.post(
  "/admins/user-managment/blocked",
  body(BlockUserRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  authorizeAdmin,
  adminsController.blockUser
)

adminsRouter.get(
  "/admins/user-managment/blocked",
  authenticateToken,
  authorizeAdmin,
  adminsController.getBlockedUsers
)

adminsRouter.delete(
  "/admins/user-managment/blocked/:username",
  authenticateToken,
  authorizeAdmin,
  adminsController.unblockUser
)

export default adminsRouter
