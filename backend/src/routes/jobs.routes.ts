import express from "express"
import { body } from "express-validator"
import JobsController from "../controllers/jobs.controller"
import authenticateToken from "../middlewares/authenticate-token.middleware"
import authorizeResultUpload from "../middlewares/authorize-result-upload.middleware"
import authorizeUser from "../middlewares/authorize-user.middleware"
import uploadVideoS3 from "../middlewares/upload-video.maddleware"
import validateRequest from "../middlewares/validate-request.middleware"
import {
  CreateJobRequired,
  UpdateJobRequired,
} from "../services/jobs-service.data-types"

const jobsRouter = express.Router()
const jobsController = new JobsController()

jobsRouter.post(
  "/",
  body(CreateJobRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  jobsController.createJob
)

jobsRouter.get("/", jobsController.getJobs)

jobsRouter.post(
  "/:jobId",
  body(UpdateJobRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  jobsController.updateJob
)

jobsRouter.get("/:jobId", jobsController.getOneJob)

jobsRouter.delete("/:jobId", authenticateToken, jobsController.deleteJob)

jobsRouter.get("/:jobId/creator", jobsController.getJobsCreator)

jobsRouter.post(
  "/:jobId/subscribers",
  authenticateToken,
  jobsController.subscribeToJob
)

jobsRouter.get("/:jobId/subscribers", jobsController.getSubscribers)

jobsRouter.delete(
  "/:jobId/subscribers/:username",
  authenticateToken,
  authorizeUser,
  jobsController.deleteSubscriber
)

jobsRouter.get("/:jobId/assignee", jobsController.getJobsAssignee)

jobsRouter.post("/:jobId/assignee", authenticateToken, jobsController.assignJob)

// decline assignement
jobsRouter.delete(
  "/:jobId/assignee",
  authenticateToken,
  jobsController.decileJobAssigment
)

jobsRouter.get("/:jobId/fulfiller", jobsController.getJobsFulfiller)

// accept fulfillment
jobsRouter.post(
  "/:jobId/fulfiller",
  authenticateToken,
  jobsController.acceptJobFulfillmet
)

// cancel fulfillment
jobsRouter.delete(
  "/:jobId/fulfiller/",
  authenticateToken,
  jobsController.cancelJobFulfillmet
)

jobsRouter.get("/:jobId/results/", authenticateToken, jobsController.getResults)

jobsRouter.post(
  "/:jobId/results/submit",
  authenticateToken,
  jobsController.submitResults
)

jobsRouter.post(
  "/:jobId/results/videos",
  authenticateToken,
  authorizeResultUpload,
  uploadVideoS3.single("video"),
  jobsController.createVideoResult
)

jobsRouter.post(
  "/:jobId/results/videos/:videoId",
  authenticateToken,
  authorizeResultUpload,
  uploadVideoS3.single("video"),
  jobsController.updateVideoResult
)

jobsRouter.get(
  "/:jobId/results/videos/:videoId",
  authenticateToken,
  jobsController.getVideoResult
)

export default jobsRouter
