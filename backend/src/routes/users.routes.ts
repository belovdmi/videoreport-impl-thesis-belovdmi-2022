import express from "express"
import { body, query } from "express-validator"
import UsersController from "../controllers/users.controller"
import authenticateToken from "../middlewares/authenticate-token.middleware"
import authorizeAdmin from "../middlewares/authorize-admin.middleware"
import authorizeUser from "../middlewares/authorize-user.middleware"
import validateRequest from "../middlewares/validate-request.middleware"
import {
  ChangePasswordRequired,
  CreateReviewRequired,
  CreateUserRequired,
  ForgotPasswordRequired,
  LoginRequired,
  NewAuthTokenRequired,
  ResetForgottenPasswordRequired,
  UpdateUserRequired,
} from "../services/users-service.data-types"

const usersRouter = express.Router()
const usersController = new UsersController()

usersRouter.post(
  "/",
  body(CreateUserRequired, "required").notEmpty(),
  validateRequest,
  usersController.createUser
)

usersRouter.get(
  "/",
  query(["page", "count"]).optional().isInt({ min: 1 }),
  validateRequest,
  usersController.getUsers
)

usersRouter.post(
  "/login",
  body(LoginRequired, "required").notEmpty(),
  validateRequest,
  usersController.login
)

usersRouter.post(
  "/refresh-auth-token",
  body(NewAuthTokenRequired, "required").notEmpty(),
  validateRequest,
  usersController.token
)

usersRouter.post("/logout", authenticateToken, usersController.logout)

usersRouter.post(
  "/forgot-password",
  body(ForgotPasswordRequired, "required").notEmpty(),
  validateRequest,
  usersController.forgotPassword
)

usersRouter.post(
  "/reset-forgotten-password",
  body(ResetForgottenPasswordRequired, "required").notEmpty(),
  validateRequest,
  usersController.resetForgottenPassword
)

usersRouter.post(
  "/:username",
  body(UpdateUserRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  authorizeUser,
  usersController.updateUser
)

usersRouter.get("/:username", usersController.getOneUser)

usersRouter.post(
  "/:username/change-password",
  body(ChangePasswordRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  authorizeUser,
  usersController.changePassword
)

usersRouter.post(
  "/:username/reviews",
  body(CreateReviewRequired, "required").notEmpty(),
  validateRequest,
  authenticateToken,
  usersController.createReview
)

usersRouter.get(
  "/:username/reviews",
  query(["page", "count"]).optional().isInt({ min: 1 }),
  validateRequest,
  usersController.getUserReviews
)

usersRouter.delete(
  "/:username/reviews/:reviewId",
  authenticateToken,
  authorizeAdmin,
  usersController.deleteReview
)

export default usersRouter
