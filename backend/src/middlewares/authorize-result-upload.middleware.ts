import { NextFunction, Request, Response } from "express"
import JobsService from "../services/jobs.service"

/**
 * middleware checks if user can upload video to cloud (sends 401 if he can't)
 * user needs to be previously authorizes by authenticateToken
 */
const authorizeResultUpload = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const jobsService = new JobsService()
  if (
    await jobsService.canUploadResult(
      Number(req.params.jobId),
      res.locals.authData.username
    )
  ) {
    next()
  } else {
    res.status(401).send("unauthorized upload")
  }
}

export default authorizeResultUpload
