import multer, { FileFilterCallback } from "multer"
import { Request } from "express"
import multerS3 from "multer-s3"
import aws from "aws-sdk"

require("dotenv").config()

// configuration for filters for file extension
const fileFilterVideo = (
  request: Request,
  file: Express.Multer.File,
  callback: FileFilterCallback
): void => {
  if (
    file.mimetype === "video/mov" ||
    file.mimetype === "video/quicktime" ||
    file.mimetype === "video/mp4" ||
    file.mimetype === "video/3gp" ||
    file.mimetype === "video/mkv" ||
    file.mimetype === "video/wmv" ||
    file.mimetype === "video/avi"
  ) {
    callback(null, true)
  } else {
    callback(null, false)
  }
}

// configuration for connectio to aws s3
const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_KEY,
})

// middleware for file uploading
const uploadVideoS3 = multer({
  storage: multerS3({
    s3,
    bucket: "zchecknito-videos",
    acl: "public-read",
    metadata(req, file, cb) {
      cb(null, { fieldName: file.fieldname })
    },
    key(req, file, cb) {
      cb(null, `${Date.now()}-${file.originalname}`)
    },
  }),
  fileFilter: fileFilterVideo,
})

export default uploadVideoS3
