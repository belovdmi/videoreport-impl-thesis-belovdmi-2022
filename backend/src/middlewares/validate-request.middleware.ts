import { NextFunction, Request, Response } from "express"
import { validationResult } from "express-validator"

/**
 * middleware checking express-validator... if validation has been passed or not (if not sends 400 status code)
 */
const validateRequest = (req: Request, res: Response, next: NextFunction) => {
  try {
    validationResult(req).throw()
    next()
  } catch (error) {
    res.status(400).send(error)
  }
}

export default validateRequest
