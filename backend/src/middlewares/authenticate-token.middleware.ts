import { NextFunction, Request, Response } from "express"
import jwt from "jsonwebtoken"

require("dotenv").config()

export interface TokenPayload {
  username: string
  id: number
  isAdmin: boolean
}

/**
 * middleware for auth token validation, (if token invalid sends 403 and next middleware won't be called)
 * TokenPayload will be saved to res.locals
 */
const authenticateToken = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization
  const token = authHeader && authHeader.split(" ")[1]

  if (token || authHeader) {
    jwt.verify(
      token || authHeader,
      process.env.AUTH_TOKEN_SECRET_KEY || "key",
      (error, user) => {
        if (error) {
          res.status(403).send("invalid auth token")
        } else {
          res.locals = { authData: user as TokenPayload }
          next()
        }
      }
    )
  } else {
    res.status(403).send("invalid auth token")
  }
}

export default authenticateToken
