import { NextFunction, Request, Response } from "express"

/**
 * middleware checks if TokenPayload that is saved to res.locals has indication that user is admin (sends 401 if he isn't)
 */
const authorizeAdmin = (req: Request, res: Response, next: NextFunction) => {
  if (res.locals.authData.isAdmin) {
    next()
  } else {
    res.status(401).send("admin rights required")
  }
}

export default authorizeAdmin
