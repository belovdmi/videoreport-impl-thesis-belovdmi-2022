import { NextFunction, Request, Response } from "express"

/**
 * middleware checks if preformed operation is on previously authorized user by authenticateToken
 */
const authorizeUser = (req: Request, res: Response, next: NextFunction) => {
  if (res.locals.authData.username === req.params.username) {
    next()
  } else {
    res.status(401).send("unauthorized for this action")
  }
}

export default authorizeUser
