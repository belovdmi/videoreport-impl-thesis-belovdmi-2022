import { Request, Response } from "express"
import {
  ChangePasswordData,
  CreateReviewData,
  CreateUserData,
  ForgotPasswordData,
  GetReviewsQueryData,
  LoginData,
  NewAuthTokenData,
  ResetForgottenPasswordData,
  UpdateUserData,
} from "../services/users-service.data-types"
import UsersService from "../services/users.service"
import controllerErrorHandler from "../utils/controller-error-handler"

// This needs to be outside
const usersService = new UsersService()

class UsersController {
  async createUser(req: Request, res: Response) {
    try {
      res.send(await usersService.createUser(req.body as CreateUserData))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async login(req: Request, res: Response) {
    try {
      res.send(await usersService.login(req.body as LoginData))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async token(req: Request, res: Response) {
    try {
      res.send(await usersService.newAuthToken(req.body as NewAuthTokenData))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async logout(req: Request, res: Response) {
    try {
      res.send(await usersService.logout(res.locals.authData.username))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getUsers(req: Request, res: Response) {
    try {
      const queryParams = req.query
      res.send(
        await usersService.getUsers(
          Number(queryParams?.page),
          Number(queryParams?.count),
          queryParams.sortBy?.toString().split(",")
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async changePassword(req: Request, res: Response) {
    try {
      res.send(
        await usersService.changePassword(
          req.body as ChangePasswordData,
          req.params.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getOneUser(req: Request, res: Response) {
    try {
      res.send(await usersService.getOneUser(req.params.username))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async updateUser(req: Request, res: Response) {
    try {
      res.send(
        await usersService.updateUser(
          req.body as UpdateUserData,
          req.params.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async forgotPassword(req: Request, res: Response) {
    try {
      res.send(
        await usersService.forgotPassword(req.body as ForgotPasswordData)
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async resetForgottenPassword(req: Request, res: Response) {
    try {
      res.send(
        await usersService.resetForgottenPassword(
          req.body as ResetForgottenPasswordData
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async createReview(req: Request, res: Response) {
    try {
      res.send(
        await usersService.createReview(
          req.body as CreateReviewData,
          req.params.username,
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getUserReviews(req: Request, res: Response) {
    try {
      const queryParams = req.query
      res.send(
        await usersService.getUserReviews(
          {
            page: Number(queryParams?.page),
            count: Number(queryParams?.count),
            sortBy: queryParams.sortBy?.toString().split(","),
            role: queryParams?.role?.toString(),
          } as GetReviewsQueryData,
          req.params.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async deleteReview(req: Request, res: Response) {
    try {
      res.send(
        await usersService.deleteReview(
          Number(req.params.reviewId),
          req.params.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }
}

export default UsersController
