import { Request, Response } from "express"
import {
  BlockUserData,
  GiveAdminRightsData,
} from "../services/admins-service.data-types"
import AdminsService from "../services/admins.service"
import controllerErrorHandler from "../utils/controller-error-handler"

// This needs to be outside
const adminsService = new AdminsService()

class AdminsController {
  giveAdminRights = async (req: Request, res: Response) => {
    try {
      res.send(
        await adminsService.giveAdminRights(req.body as GiveAdminRightsData)
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  removeAdminRights = async (req: Request, res: Response) => {
    try {
      res.send(await adminsService.removeAdminRights(req.params.username))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  blockUser = async (req: Request, res: Response) => {
    try {
      res.send(await adminsService.blockUser(req.body as BlockUserData))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  getBlockedUsers = async (req: Request, res: Response) => {
    try {
      res.send(await adminsService.getBlockedUsers())
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  unblockUser = async (req: Request, res: Response) => {
    try {
      res.send(await adminsService.unblockUser(req.params.username))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }
}

export default AdminsController
