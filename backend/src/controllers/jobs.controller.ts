import { Request, Response } from "express"
import {
  CreateJobData,
  VideoResultData,
} from "../services/jobs-service.data-types"
import JobsService from "../services/jobs.service"
import controllerErrorHandler, {
  ServiceError,
} from "../utils/controller-error-handler"

// This needs to be outside
const jobsService = new JobsService()

class JobsController {
  async createJob(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.createJob(
          req.body as CreateJobData,
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getJobs(req: Request, res: Response) {
    try {
      const queryParams = req.query
      res.send(
        await jobsService.getJobs({
          sortBy: queryParams.sortBy?.toString().split(","),
          page: Number(queryParams.page),
          count: Number(queryParams.count),
          priceLte: Number(queryParams.priceLte),
          dueDateLte: queryParams.dueDateLte?.toString(),
          priceGte: Number(queryParams.priceGte),
          dueDateGte: queryParams.dueDateGte?.toString(),
          region: queryParams.region?.toString().split(","),
          username: queryParams.username?.toString(),
          role: queryParams.role?.toString(),
          category: queryParams.category?.toString(),
          status: queryParams.status?.toString(),
        })
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async updateJob(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.updateJob(
          req.body as CreateJobData,
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async deleteJob(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.deleteJob(
          Number(req.params.jobId),
          res.locals.authData.username,
          res.locals.authData.isAdmin
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getOneJob(req: Request, res: Response) {
    try {
      res.send(await jobsService.getOneJob(Number(req.params.jobId)))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getJobsCreator(req: Request, res: Response) {
    try {
      res.send(await jobsService.getJobsCreator(Number(req.params.jobId)))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getJobsFulfiller(req: Request, res: Response) {
    try {
      res.send(await jobsService.getJobsFulfiller(Number(req.params.jobId)))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async subscribeToJob(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.subscribeToJob(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getJobsAssignee(req: Request, res: Response) {
    try {
      res.send(await jobsService.getJobsAssignee(Number(req.params.jobId)))
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async assignJob(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.assignJob(
          Number(req.params.jobId),
          req.params.username,
          req.body.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getSubscribers(req: Request, res: Response) {
    try {
      const queryParams = req.query
      res.send(
        await jobsService.getSubscribers(
          {
            sortBy: queryParams.sortBy?.toString(),
          },
          Number(req.params.jobId)
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async deleteSubscriber(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.deleteSubscriber(
          Number(req.params.jobId),
          req.params.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async acceptJobFulfillmet(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.acceptJobFulfillmet(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async decileJobAssigment(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.decileJobAssigment(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async cancelJobFulfillmet(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.cancelJobFulfillmet(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getResults(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.getResults(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async submitResults(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.submitResults(
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async createVideoResult(req: Request, res: Response) {
    try {
      if (!req.file) {
        throw new ServiceError(422, "unsupported file format")
      }
      res.send(
        await jobsService.createVideoResult(
          req.file as unknown as VideoResultData,
          Number(req.params.jobId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async updateVideoResult(req: Request, res: Response) {
    try {
      if (!req.file) {
        throw new ServiceError(422, "unsupported file format")
      }
      res.send(
        await jobsService.updateVideoResult(
          req.file as unknown as VideoResultData,
          Number(req.params.jobId),
          Number(req.params.videoId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }

  async getVideoResult(req: Request, res: Response) {
    try {
      res.send(
        await jobsService.getVideoResult(
          Number(req.params.jobId),
          Number(req.params.videoId),
          res.locals.authData.username
        )
      )
    } catch (error) {
      controllerErrorHandler(error, res)
    }
  }
}

export default JobsController
