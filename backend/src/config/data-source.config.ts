import { DataSource } from "typeorm"
import Category from "../entities/Category"
import Job from "../entities/Job"
import Participation from "../entities/Participation"
import Result from "../entities/Result"
import Review from "../entities/Review"
import Location from "../entities/Location"
import ScoreSummary from "../entities/ScoreSummary"
import User from "../entities/User"

require("dotenv").config()

// configuration for database connection
const dataSource = new DataSource({
  type: "postgres",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [
    User,
    ScoreSummary,
    Job,
    Location,
    Review,
    Result,
    Category,
    Participation,
  ],
  synchronize: true,
})

export default dataSource
