const Redis = require("ioredis")

// with creation connection is established
// if you are running redis outside docker-compose (on localhost) change redis to localhost
const radisClient = new Redis({ port: 6379, host: "redis" })

export default radisClient
