import bcrypt from "bcrypt"
import { DataSource } from "typeorm"
import Category from "../entities/Category"
import ScoreSummary from "../entities/ScoreSummary"
import User from "../entities/User"

require("dotenv").config()

// initialize database and create basic categories and admin user
const initDatabase = async (dataSource: DataSource) => {
  const userRepository = dataSource.getRepository(User)
  const categoryRepository = dataSource.getRepository(Category)
  const scoreSummaryRepository = dataSource.getRepository(ScoreSummary)

  const findCategories = await categoryRepository.find()

  if (!findCategories.length) {
    await categoryRepository.save({ name: "Auta" })
    await categoryRepository.save({ name: "Nemovitosti" })
    await categoryRepository.save({ name: "Ostatní" })
  }

  const findUsers = await userRepository.find()

  if (!findUsers.length) {
    const hashedPassword = await bcrypt.hash(
      process.env.INIT_ADMIN_PASSWORD || "Heslo123",
      10
    )

    const scoreSummary = await scoreSummaryRepository.save({
      score: 0,
      jobsGiven: 0,
      jobsTaken: 0,
      numberOfReviews: 0,
    })

    await userRepository.save({
      username: "admin",
      email: "zchecknito@gmail.com",
      password: hashedPassword,
      isAdmin: true,
      isBlocked: false,
      scoreSummary,
    })
  }
}

export default initDatabase
