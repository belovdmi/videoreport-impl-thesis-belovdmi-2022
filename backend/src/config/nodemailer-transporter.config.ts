import nodemailer from "nodemailer"

require("dotenv").config()

// !!! google is canceling ability co connect to Gmail at the end of 2022
const nodemailerTransporter = nodemailer.createTransport({
  service: process.env.NODEMAILER_SERVICE,
  auth: {
    user: process.env.NODEMAILER_USER,
    pass: process.env.NODEMAILER_PASSWORD,
  },
})

export default nodemailerTransporter
