import { Between } from "typeorm"
import moment from "moment"
import aws from "aws-sdk"
import * as emailValidator from "email-validator"
import dataSource from "../config/data-source.config"
import Category from "../entities/Category"
import Job, { JobState } from "../entities/Job"
import Location from "../entities/Location"
import Participation, { ParticipationRole } from "../entities/Participation"
import Result, { ResultType } from "../entities/Result"
import User, { RegionsCZE } from "../entities/User"
import { ServiceError } from "../utils/controller-error-handler"
import getOrder from "../utils/get-order"
import {
  CreateJobData,
  GetJobsQueryData,
  GetSubscribersQueryData,
  UpdateJobData,
  VideoResultData,
} from "./jobs-service.data-types"
import ScoreSummary from "../entities/ScoreSummary"
import { checkRegion } from "../utils/checkers"
import nodemailerTransporter from "../config/nodemailer-transporter.config"

require("dotenv").config()

class JobsService {
  #jobRepository = dataSource.getRepository(Job)

  #userRepository = dataSource.getRepository(User)

  #scoreSummaryRepository = dataSource.getRepository(ScoreSummary)

  #participationRepository = dataSource.getRepository(Participation)

  #locationRepository = dataSource.getRepository(Location)

  #resultRepository = dataSource.getRepository(Result)

  #categoryRepository = dataSource.getRepository(Category)

  #s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  })

  async #sendNotification(
    email: string | undefined,
    jobId: number,
    message: string
  ) {
    if (!email || !emailValidator.validate(email)) {
      return
    }

    const mailOptions = {
      from: "zchecknito@gmail.com",
      to: email,
      subject: `Notifikace ohledně práce s identifikačním číslem: ${jobId}`,
      text: `${message} \n${process.env.CLIENT_URL}/my-works/${jobId}`,
    }

    await nodemailerTransporter.sendMail(mailOptions).catch(err => {
      throw new Error(err)
    })
  }

  /**
   * notifies creator of job and fillfiler if counterpartyRole is not specified
   */
  async #notifyUsers(
    jobId: number,
    message: string,
    counterpartyRole: ParticipationRole
  ) {
    const creator = await this.#participationRepository.findOneBy({
      role: ParticipationRole.CREATOR,
      job: { id: jobId },
    })

    await this.#sendNotification(creator?.user.email, jobId, message)

    const counterparty = await this.#participationRepository.findOneBy({
      role: counterpartyRole,
      job: { id: jobId },
    })

    await this.#sendNotification(counterparty?.user.email, jobId, message)
  }

  async #changeJobState(
    jobId: number,
    username: string,
    currentJobState: JobState,
    newJobState: JobState,
    currentParticipationRole: ParticipationRole,
    newParticipationRole?: ParticipationRole
  ) {
    const newRole = newParticipationRole || currentParticipationRole
    const participant = await this.#participationRepository.findOneBy({
      job: { id: jobId },
      user: { username },
      role: currentParticipationRole,
    })
    if (!participant) {
      throw new ServiceError(404, "job not found")
    }

    if (participant.job.status !== currentJobState) {
      throw new ServiceError(400, "invalid job state")
    }

    await this.#participationRepository.save({
      id: participant.id,
      role: newRole,
    })

    await this.#jobRepository.save({
      id: participant.job.id,
      status: newJobState,
    })
  }

  async canUploadResult(jobId: number, username: string) {
    const uploader = await this.#participationRepository.findOneBy({
      job: { id: jobId },
      user: { username },
      role: ParticipationRole.FULFILLER,
    })

    return uploader
  }

  async #canViewResults(jobId: number, username: string) {
    const viewer = await this.#participationRepository.findOne({
      where: [
        {
          job: { id: jobId },
          user: { username },
          role: ParticipationRole.FULFILLER,
        },
        {
          job: { id: jobId },
          user: { username },
          role: ParticipationRole.CREATOR,
        },
      ],
    })

    return viewer
  }

  /**
   * deletes file from aws s3 bucket
   */
  #deleteFile(key: string) {
    this.#s3.deleteObject(
      {
        Bucket: "zchecknito-videos",
        Key: key,
      },
      err => {
        if (err) {
          throw new Error("s3 deletion failed")
        }
      }
    )
  }

  async createJob(
    {
      title,
      description,
      price,
      dueDate,
      phoneNumber,
      category,
      email,
      address,
      region,
      town,
      advertisementLink,
    }: CreateJobData,
    username: string
  ) {
    const user = await this.#userRepository.findOneBy({ username })
    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    if (!checkRegion(region)) {
      throw new ServiceError(400, "invalid region")
    }

    let findCategory
    if (category) {
      findCategory = await this.#categoryRepository.findOneBy({
        name: category,
      })
      if (!findCategory) {
        throw new ServiceError(400, "invalid category")
      }
    }

    const participation = await this.#participationRepository.save({
      role: ParticipationRole.CREATOR,
      user,
    })

    const location = await this.#locationRepository.save({
      region: region as RegionsCZE,
      town,
      address,
    })

    const job = await this.#jobRepository.save({
      advertisementLink,
      contactEmail: email,
      contactPhoneNumber: phoneNumber,
      title,
      description,
      dueDate,
      price,
      status: JobState.FREE,
      location,
      category: findCategory,
      participants: [participation],
    })

    await this.#participationRepository.save({
      id: participation.id,
      job,
    })
  }

  //  unknown sortBys are ignored
  async getJobs({
    sortBy,
    page,
    count,
    priceLte,
    priceGte,
    dueDateLte,
    dueDateGte,
    region,
    username,
    category,
    status,
    role,
  }: GetJobsQueryData) {
    const defaultReturnData = { page: page || 0, count: count || 0, jobs: [] }

    if (page && count && (page <= 0 || count <= 0)) {
      return defaultReturnData
    }

    if (
      status &&
      !Object.values(JobState).includes(status as unknown as JobState)
    ) {
      return defaultReturnData
    }

    if (
      role &&
      !Object.values(ParticipationRole).includes(
        role as unknown as ParticipationRole
      )
    ) {
      return defaultReturnData
    }

    if (priceLte && priceGte && priceLte < priceGte) {
      return defaultReturnData
    }

    if (
      (dueDateLte && !moment(dueDateLte).isValid()) ||
      (dueDateGte && !moment(dueDateGte).isValid())
    ) {
      return defaultReturnData
    }

    if (
      dueDateLte &&
      dueDateGte &&
      Date.parse(dueDateLte) < Date.parse(dueDateGte)
    ) {
      return defaultReturnData
    }

    type LocationRegionsItem = { region: RegionsCZE }
    const locationRegions: LocationRegionsItem[] = []
    if (region) {
      for (let i = 0; i < region.length; i += 1) {
        if (checkRegion(region[i])) {
          locationRegions.push({ region: region[i] as RegionsCZE })
        } else {
          return defaultReturnData
        }
      }
    }

    const participationsIds: { id: number }[] = []
    if (username) {
      const participations = await this.#participationRepository.find({
        select: { id: true },
        where: {
          user: { username },
          role: (role as ParticipationRole) || undefined,
        },
      })

      if (!participations.length) {
        return defaultReturnData
      }

      participations.forEach(data => {
        participationsIds.push({ id: data.id })
      })
    }

    const [jobs, jobsCount] = await this.#jobRepository.findAndCount({
      relations: {
        location: true,
        category: true,
      },
      where: {
        price: Between(priceGte || 0, priceLte || 100000000),
        dueDate: Between(
          dueDateGte
            ? (dueDateGte as unknown as Date)
            : ("1/1/1900" as unknown as Date),
          dueDateLte
            ? (dueDateLte as unknown as Date)
            : ("1/1/3000" as unknown as Date)
        ),
        status: status as unknown as JobState,
        location: locationRegions,
        participants: participationsIds.length ? participationsIds : undefined,
        category: {
          name: category,
        },
      },
      order: {
        price: getOrder("price", sortBy),
        created: getOrder("created", sortBy),
        id: getOrder("jobId", sortBy),
        dueDate: getOrder("dueDate", sortBy),
      },
      skip: ((page || 1) - 1) * (count || 1),
      take: count || undefined,
    })

    return { page, count, totalCount: jobsCount, jobs }
  }

  async updateJob(
    {
      title,
      description,
      price,
      dueDate,
      phoneNumber,
      category,
      email,
      address,
      region,
      town,
      advertisementLink,
    }: UpdateJobData,
    jobId: number,
    username: string
  ) {
    const participation = await this.#participationRepository.findOneBy({
      job: {
        id: jobId,
        status: JobState.FREE,
      },
      user: {
        username,
      },
      role: ParticipationRole.CREATOR,
    })

    if (!participation) {
      throw new ServiceError(404, "job not found")
    }

    if (!checkRegion(region)) {
      throw new ServiceError(400, "invalid region")
    }

    const foundCategory = await this.#categoryRepository.findOneBy({
      name: category,
    })
    if (!foundCategory) {
      throw new ServiceError(400, "invalid category")
    }

    await this.#locationRepository.save({
      id: participation.job.location.id,
      address,
      town,
      region: region as RegionsCZE,
    })

    await this.#jobRepository.save({
      id: jobId,
      price,
      title,
      description,
      contactPhoneNumber: phoneNumber,
      contactEmail: email,
      advertisementLink,
      dueDate,
      category: foundCategory,
    })
  }

  async getOneJob(jobId: number) {
    const job = await this.#jobRepository.findOne({
      relations: { location: true, category: true },
      where: { id: jobId },
    })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    return job
  }

  async deleteJob(jobId: number, username: string, isAdmin: boolean) {
    const creator = await this.#participationRepository.findOneBy({
      job: { id: jobId },
      user: { username },
    })

    const fulfiller = await this.#participationRepository.findOne({
      relations: { results: true },
      where: {
        job: { id: jobId },
        user: { username },
      },
    })

    const job = await this.#jobRepository.findOneBy({ id: jobId })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    if (
      (creator === null || creator.role !== ParticipationRole.CREATOR) &&
      !isAdmin
    ) {
      throw new ServiceError(401, "only admin or owner of job can delete it")
    }

    fulfiller?.results?.forEach(data => {
      this.#deleteFile(data.key)
    })

    await this.#sendNotification(
      creator?.user.email,
      jobId,
      "Práce byla smazána."
    )

    await this.#sendNotification(
      fulfiller?.user.email,
      jobId,
      "Práce byla smazána."
    )

    await this.#jobRepository.delete(jobId)

    await this.#notifyUsers(
      jobId,
      "Práce byla smazána.",
      ParticipationRole.FULFILLER
    )
  }

  async getJobsCreator(jobId: number) {
    const job = await this.#jobRepository.findOne({
      relations: { location: true, participants: true, category: true },
      where: { id: jobId },
    })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    const creatorParticipant = job.participants.filter(
      participant => participant.role === ParticipationRole.CREATOR
    )

    if (!creatorParticipant.length) {
      throw new ServiceError(404, "creator not found")
    }

    const creator = await this.#userRepository.findOne({
      relations: { specializations: true },
      where: { id: creatorParticipant[0].user.id },
    })

    if (!creator) {
      throw new ServiceError(404, "creator not found")
    }

    return creator
  }

  async getSubscribers({ sortBy }: GetSubscribersQueryData, jobId: number) {
    const checkJob = await this.#jobRepository.findOneBy({ id: jobId })

    if (!checkJob) {
      throw new ServiceError(404, "job not found")
    }

    const participations = await this.#participationRepository.find({
      where: {
        job: {
          id: jobId,
        },
        role: ParticipationRole.SUBSCRIBER,
      },
      order: {
        user: {
          scoreSummary: {
            score: getOrder("score", [sortBy || ""]),
            numberOfReviews: getOrder(
              "numberOfReviews",
              sortBy ? [sortBy] : undefined
            ),
          },
        },
      },
    })

    const subscribers: User[] = []
    for (let i = 0; i < participations.length; i += 1) {
      subscribers.push(participations[i].user)
    }

    return subscribers
  }

  async subscribeToJob(jobId: number, username: string) {
    const job = await this.#jobRepository.findOneBy({
      id: jobId,
    })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    const user = await this.#userRepository.findOne({
      where: {
        username,
      },
    })

    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    // check if allready subscribed
    const checkParticipation = await this.#participationRepository.findOneBy({
      job: { id: jobId },
      user: { username },
    })

    if (checkParticipation) {
      throw new ServiceError(400, "user is already participating in job")
    }

    await this.#participationRepository.save({
      role: ParticipationRole.SUBSCRIBER,
      user,
      job,
    })
  }

  async deleteSubscriber(jobId: number, username: string) {
    const participation = await this.#participationRepository.findOneBy({
      job: {
        id: jobId,
      },
      user: {
        username,
      },
    })

    if (!participation) {
      throw new ServiceError(404, "subscriber of this job not found")
    }

    if (participation.role !== ParticipationRole.SUBSCRIBER) {
      throw new ServiceError(400, "only subscriber can unsubscribe")
    }

    await this.#participationRepository.delete(participation.id)
  }

  async getJobsAssignee(jobId: number) {
    const job = await this.#jobRepository.findOne({
      relations: { location: true, participants: true, category: true },
      where: { id: jobId },
    })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    const assigneeParticipant = job.participants.filter(
      participant => participant.role === ParticipationRole.ASSIGNED
    )

    if (!assigneeParticipant.length) {
      throw new ServiceError(404, "asignee not found")
    }

    const assignee = await this.#userRepository.findOne({
      relations: { specializations: true },
      where: { id: assigneeParticipant[0].user.id },
    })

    if (!assignee) {
      throw new ServiceError(404, "asignee not found")
    }

    return assignee
  }

  /**
   * choosen subscriber will be assigned to job
   */
  async assignJob(
    jobId: number,
    creatorsUsername: string,
    subscribersUsername: string
  ) {
    const creator = await this.#participationRepository.findOneBy({
      job: {
        id: jobId,
      },
      user: {
        username: creatorsUsername,
      },
      role: ParticipationRole.CREATOR,
    })

    if (!creator) {
      throw new ServiceError(404, "creator of job not found")
    }

    const subscriber = await this.#participationRepository.findOneBy({
      job: {
        id: jobId,
      },
      user: {
        username: subscribersUsername,
      },
      role: ParticipationRole.SUBSCRIBER,
    })

    if (!subscriber) {
      throw new ServiceError(404, "subscriber with this job not found")
    }

    const checkAssigned = await this.#participationRepository.findOneBy({
      job: {
        id: jobId,
      },
      role: ParticipationRole.ASSIGNED,
    })

    if (checkAssigned) {
      throw new ServiceError(400, "job already assigned")
    }

    await this.#changeJobState(
      jobId,
      subscriber.user.username,
      JobState.FREE,
      JobState.ASSIGNED,
      ParticipationRole.SUBSCRIBER,
      ParticipationRole.ASSIGNED
    )

    await this.#sendNotification(
      subscriber.user.email,
      jobId,
      "Byl jste vybrán k plnění práce. Aby jste mohl začít práci plnit je nutné plnění protvrdit v portálu."
    )

    await this.#sendNotification(
      creator.user.email,
      jobId,
      "K práci byl přižazen plnitel."
    )
  }

  async decileJobAssigment(jobId: number, username: string) {
    await this.#notifyUsers(
      jobId,
      "Plnění práce bylo odmítnuto.",
      ParticipationRole.ASSIGNED
    )

    await this.#changeJobState(
      jobId,
      username,
      JobState.ASSIGNED,
      JobState.FREE,
      ParticipationRole.ASSIGNED,
      ParticipationRole.SUBSCRIBER
    )

    this.deleteSubscriber(jobId, username)
  }

  async getJobsFulfiller(jobId: number) {
    const job = await this.#jobRepository.findOne({
      relations: { location: true, participants: true, category: true },
      where: { id: jobId },
    })

    if (!job) {
      throw new ServiceError(404, "job not found")
    }

    const fulfillerParticipant = job.participants.filter(
      participant => participant.role === ParticipationRole.FULFILLER
    )

    if (!fulfillerParticipant.length) {
      throw new ServiceError(404, "fulfiller not found")
    }

    const fulfiller = await this.#userRepository.findOne({
      relations: { specializations: true },
      where: { id: fulfillerParticipant[0].user.id },
    })

    if (!fulfiller) {
      throw new ServiceError(404, "fulfiller not found")
    }

    return fulfiller
  }

  /**
   * assigned user to job (assignee) will accept fulfillment
   */
  async acceptJobFulfillmet(jobId: number, username: string) {
    await this.#changeJobState(
      jobId,
      username,
      JobState.ASSIGNED,
      JobState.IN_PROGRESS,
      ParticipationRole.ASSIGNED,
      ParticipationRole.FULFILLER
    )

    await this.#notifyUsers(
      jobId,
      "Plnění práce bylo přijato.",
      ParticipationRole.FULFILLER
    )
  }

  /**
   * fullfiller will cancel fulfillment
   */
  async cancelJobFulfillmet(jobId: number, username: string) {
    await this.#notifyUsers(
      jobId,
      "Plnění práce bylo zrušeno.",
      ParticipationRole.FULFILLER
    )

    await this.#changeJobState(
      jobId,
      username,
      JobState.IN_PROGRESS,
      JobState.FREE,
      ParticipationRole.FULFILLER,
      ParticipationRole.SUBSCRIBER
    )

    const fulfiller = await this.#participationRepository.findOne({
      relations: { results: true },
      where: {
        user: { username },
        job: { id: jobId },
      },
    })

    fulfiller?.results?.forEach(data => {
      this.#deleteFile(data.key)
    })

    await this.deleteSubscriber(jobId, username)
  }

  /**
   * returns id of results
   */
  async getResults(jobId: number, username: string) {
    const viewer = await this.#canViewResults(jobId, username)
    if (!viewer) {
      throw new ServiceError(
        401,
        "only creator and fulfiller of job have access to results"
      )
    }

    const participators = await this.#participationRepository.find({
      relations: { results: true },
      where: {
        job: { id: jobId },
      },
    })

    const result = participators.filter(item => {
      return item?.results?.length !== 0 && item.results
    })

    if (!result.length) {
      throw new ServiceError(404, "results not found")
    }

    const videoIds: number[] = []

    result.forEach(data => {
      data.results.forEach(videoData => {
        if (videoData.type === ResultType.VIDEO) {
          videoIds.push(videoData.id)
        }
      })
    })

    if (!videoIds.length) {
      throw new ServiceError(404, "results not found")
    }

    return { videos: videoIds }
  }

  /**
   * sends result to creator
   */
  async submitResults(jobId: number, username: string) {
    const creator = await this.#participationRepository.findOneBy({
      job: { id: jobId },
      role: ParticipationRole.CREATOR,
    })

    if (!creator) {
      throw new ServiceError(404, "creator not found")
    }

    const fulfiller = await this.#participationRepository.findOne({
      relations: { results: true },
      where: {
        job: { id: jobId },
        user: { username },
        role: ParticipationRole.FULFILLER,
      },
    })

    if (!fulfiller) {
      throw new ServiceError(404, "fulfiller not found")
    }

    if (!fulfiller.results.length) {
      throw new ServiceError(400, "result not present")
    }

    await this.#changeJobState(
      jobId,
      username,
      JobState.IN_PROGRESS,
      JobState.FINISHED,
      ParticipationRole.FULFILLER
    )

    await this.#scoreSummaryRepository.save({
      id: creator.user.scoreSummary.id,
      jobsGiven: creator.user.scoreSummary.jobsGiven + 1,
    })

    await this.#scoreSummaryRepository.save({
      id: fulfiller.user.scoreSummary.id,
      jobsTaken: fulfiller.user.scoreSummary.jobsTaken + 1,
    })

    await this.#notifyUsers(
      jobId,
      "Práce je dokončená.",
      ParticipationRole.FULFILLER
    )
  }

  async createVideoResult(
    { location, key }: VideoResultData,
    jobId: number,
    username: string
  ) {
    const fulfiller = await this.canUploadResult(jobId, username)
    if (!fulfiller) {
      throw new ServiceError(401, "only fulfiller can upload results")
    }

    const job = await this.#jobRepository.findOneBy({ id: jobId })

    if (job?.status !== JobState.IN_PROGRESS) {
      throw new ServiceError(400, "bad job state")
    }

    await this.#resultRepository.save({
      location,
      key,
      type: ResultType.VIDEO,
      participant: fulfiller,
    })
  }

  /**
   * deletes old video and uploades new
   */
  async updateVideoResult(
    { location, key }: VideoResultData,
    jobId: number,
    videoId: number,
    username: string
  ) {
    const fulfiller = await this.canUploadResult(jobId, username)
    if (!fulfiller) {
      throw new ServiceError(401, "only fulfiller can upload results")
    }

    const video = await this.#resultRepository.findOne({
      relations: { participant: true },
      where: { id: videoId },
    })

    if (!video) {
      throw new ServiceError(404, "video not found")
    }

    if (fulfiller.id !== video.participant.id) {
      throw new ServiceError(401, "only fulfiller can upload results")
    }

    this.#deleteFile(video.key)

    await this.#resultRepository.save({
      id: videoId,
      location,
      key,
    })
  }

  /**
   * returns link for video download
   */
  async getVideoResult(jobId: number, videoId: number, username: string) {
    const viewer = await this.#canViewResults(jobId, username)
    if (!viewer) {
      throw new ServiceError(
        401,
        "only creator and fulfiller of job have access to results"
      )
    }

    const video = await this.#resultRepository.findOne({
      where: { id: videoId },
    })

    if (!video) {
      throw new ServiceError(404, "video not found")
    }

    return { url: video.location }
  }
}

export default JobsService
