import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import * as emailValidator from "email-validator"
import dataSource from "../config/data-source.config"
import radisClient from "../config/redis-clinet.config"
import Category from "../entities/Category"
import ScoreSummary from "../entities/ScoreSummary"
import User, { RegionsCZE } from "../entities/User"
import nodemailerTransporter from "../config/nodemailer-transporter.config"
import getOrder from "../utils/get-order"
import {
  ChangePasswordData,
  CreateReviewData,
  CreateUserData,
  ForgotPasswordData,
  GetReviewsQueryData,
  LoginData,
  NewAuthTokenData,
  ResetForgottenPasswordData,
  UpdateUserData,
} from "./users-service.data-types"
import { TokenPayload } from "../middlewares/authenticate-token.middleware"
import Participation, { ParticipationRole } from "../entities/Participation"
import { JobState } from "../entities/Job"
import Review from "../entities/Review"
import { ServiceError } from "../utils/controller-error-handler"
import { checkRegion } from "../utils/checkers"

require("dotenv").config()

class UsersService {
  #userRepository = dataSource.getRepository(User)

  #scoreSummaryRepository = dataSource.getRepository(ScoreSummary)

  #categoryRepository = dataSource.getRepository(Category)

  #participationRepository = dataSource.getRepository(Participation)

  #reviewRepository = dataSource.getRepository(Review)

  async createUser({ username, email, password }: CreateUserData) {
    const findUsername = await this.#userRepository.findOneBy({ username })
    const findEmail = await this.#userRepository.findOneBy({ email })

    if (findUsername || findEmail) {
      throw new ServiceError(
        400,
        `${findUsername ? "username " : ""}${findEmail ? "email " : ""}taken`
      )
    }

    const hashedPassword = await bcrypt.hash(password, 10)

    const scoreSummary = await this.#scoreSummaryRepository.save({
      score: 0,
      jobsGiven: 0,
      jobsTaken: 0,
      numberOfReviews: 0,
    })

    await this.#userRepository.save({
      email,
      username,
      password: hashedPassword,
      isAdmin: false,
      isBlocked: false,
      scoreSummary,
    })
  }

  // unknown sortBys are ignored
  async getUsers(page?: number, count?: number, sortBy?: string[]) {
    if (page && count && (page <= 0 || count <= 0)) {
      return { page, count, totalCount: 0, users: [] }
    }

    const [users, usersCount] = await this.#userRepository.findAndCount({
      relations: {
        scoreSummary: true,
        specializations: true,
      },
      order: {
        username: getOrder("username", sortBy),
        id: getOrder("userId", sortBy),
      },
      skip: ((page || 1) - 1) * (count || 1),
      take: count || 12,
    })

    return {
      page: page || 1,
      count: users.length,
      totalCount: usersCount,
      users,
    }
  }

  /**
   * validates username and password after that create refresh token that will be stored in to Redis and auth token
   */
  async login({ username, password }: LoginData) {
    const user = await this.#userRepository.findOne({
      where: { username },
      select: {
        username: true,
        id: true,
        isAdmin: true,
        password: true,
        isBlocked: true,
      },
    })

    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    if (user.isBlocked) {
      throw new ServiceError(403, "blocked user")
    }

    const passwordCorrect = await bcrypt.compare(password, user.password)

    if (!passwordCorrect) {
      throw new ServiceError(404, "user not found")
    }

    const jwtPayload = {
      username: user.username,
      id: user.id,
      isAdmin: user.isAdmin,
    }

    const authToken = jwt.sign(
      jwtPayload,
      process.env.AUTH_TOKEN_SECRET_KEY || "key",
      { expiresIn: "30m" }
    )

    const refreshToken = jwt.sign(
      jwtPayload,
      process.env.REFRESH_TOKEN_SECRET_KEY || "key",
      { expiresIn: "6h" }
    )

    // 21600 sec = 6 h
    const res = await radisClient.setex(username, 21600, refreshToken)
    if (res !== "OK") {
      throw new Error("Redis setex failed")
    }

    return {
      authToken,
      refreshToken,
    }
  }

  /**
   * validates refresh token and returns new auth token
   */
  async newAuthToken({ refreshToken }: NewAuthTokenData) {
    let jwtPayload: TokenPayload
    try {
      jwtPayload = jwt.verify(
        refreshToken,
        process.env.REFRESH_TOKEN_SECRET_KEY || "key"
      ) as TokenPayload
    } catch (error) {
      throw new ServiceError(403, "invalid refresh token")
    }

    const findRefreshToken = await radisClient.get(jwtPayload.username)
    if (!findRefreshToken) {
      throw new ServiceError(403, "invalid refresh token")
    }

    return {
      authToken: jwt.sign(
        {
          username: jwtPayload.username,
          id: jwtPayload.id,
          isAdmin: jwtPayload.isAdmin,
        },
        process.env.AUTH_TOKEN_SECRET_KEY || "key",
        { expiresIn: "30m" }
      ),
    }
  }

  /**
   * deletes refresh token from database
   */
  async logout(username: string) {
    const findRefreshToken = await radisClient.get(username)
    if (findRefreshToken) {
      const res = await radisClient.del(username)
      if (!res) {
        throw new ServiceError(400, "user already logout")
      }

      return
    }

    throw new ServiceError(403, "invalid auth token")
  }

  /**
   * sends email with created link to reset pwd
   */
  async forgotPassword({ email }: ForgotPasswordData) {
    if (!emailValidator.validate(email)) {
      throw new ServiceError(400, "invalid email format")
    }

    const user = await this.#userRepository.findOneBy({
      email,
    })

    if (user === null) {
      throw new ServiceError(404, "user not found")
    }

    const resetPaswordToken = jwt.sign(
      {
        username: user.username,
        id: user.id,
        isAdmin: user.isAdmin,
      },
      process.env.RESET_TOKEN_SECRET_KEY || "key",
      {
        expiresIn: "15m",
      }
    )

    const mailOptions = {
      from: "zchecknito@gmail.com",
      to: email,
      subject: "Zapomenuté heslo",
      text: `Dobry den,\nzasiláme vám link na obnovení hesla paltí po dobu 15 minut:\n${process.env.CLIENT_URL}/reset-password?resetToken=${resetPaswordToken}`,
    }

    await nodemailerTransporter.sendMail(mailOptions).catch(err => {
      throw new Error(err)
    })
  }

  /**
   * validates resetToken and after sets new password
   */
  async resetForgottenPassword({
    newPassword,
    resetToken,
  }: ResetForgottenPasswordData) {
    let jwtPayload: TokenPayload
    try {
      jwtPayload = jwt.verify(
        resetToken,
        process.env.RESET_TOKEN_SECRET_KEY || "key"
      ) as TokenPayload
    } catch (error) {
      throw new ServiceError(403, "invalid reset token")
    }

    const hashedNewPassword = await bcrypt.hash(newPassword, 10)

    await this.#userRepository.save({
      id: jwtPayload.id,
      password: hashedNewPassword,
    })
  }

  async changePassword(
    { oldPassword, newPassword }: ChangePasswordData,
    username: string
  ) {
    const findUser = await this.#userRepository.findOne({
      where: { username },
      select: { password: true, id: true },
    })

    if (!findUser) {
      throw new ServiceError(404, "user not found")
    }

    const passwordCorrect = await bcrypt.compare(oldPassword, findUser.password)
    if (!passwordCorrect) {
      throw new ServiceError(400, "invalid password")
    }

    const hashedNewPassword = await bcrypt.hash(newPassword, 10)
    await this.#userRepository.save({
      id: findUser.id,
      password: hashedNewPassword,
    })
  }

  async updateUser(
    {
      email,
      phoneNumber,
      region,
      description,
      specializations,
    }: UpdateUserData,
    username: string
  ) {
    const user = await this.#userRepository.findOneBy({
      username,
    })

    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    if (email && email !== user.email) {
      const userByEmail = await this.#userRepository.findOneBy({
        email,
      })

      if (userByEmail) {
        throw new ServiceError(400, "email taken")
      }
    }

    let categories: Category[] | undefined
    if (specializations?.length > 0) {
      const names: { name: string }[] = []
      specializations.forEach(item => names.push({ name: item }))

      categories = await this.#categoryRepository.find({
        where: names,
      })
    }

    if (
      region &&
      (!checkRegion(region) ||
        (categories && categories?.length !== specializations.length))
    ) {
      throw new ServiceError(400, "invalid categories or region")
    }

    user.email = email
    user.specializations = categories || []
    user.region = region ? (region as RegionsCZE) : null
    user.description = description
    user.phoneNumber = phoneNumber

    await this.#userRepository.save(user)
  }

  async getOneUser(username: string) {
    const user = await this.#userRepository.findOne({
      relations: {
        scoreSummary: true,
        specializations: true,
      },
      where: { username },
    })

    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    return user
  }

  /**
   * user that sends request is creator of review and username in request is reviewed user
   */
  async createReview(
    { jobId, score, review }: CreateReviewData,
    reviewedUsername: string,
    reviewCreatorUsername: string
  ) {
    const craetorOfReview = await this.#participationRepository.findOne({
      relations: { user: true, job: true },
      where: {
        user: {
          username: reviewCreatorUsername,
        },
        job: {
          id: jobId,
        },
      },
    })

    if (!craetorOfReview) {
      throw new ServiceError(404, "creator of review not found")
    }

    const reviewedParticipant = await this.#participationRepository.findOne({
      relations: { user: true, job: true },
      where: {
        user: {
          username: reviewedUsername,
        },
        job: {
          id: jobId,
        },
      },
    })

    if (!reviewedParticipant) {
      throw new ServiceError(404, "reviewed user not found")
    }

    const foundReview = await this.#reviewRepository.findOne({
      relations: { reviewedParticipant: true },
      where: {
        reviewedParticipant: { id: reviewedParticipant.id },
      },
    })

    if (foundReview) {
      throw new ServiceError(400, "aleready exists")
    }

    const reviewToFulfiller =
      craetorOfReview.role === ParticipationRole.CREATOR &&
      reviewedParticipant.role === ParticipationRole.FULFILLER &&
      reviewedParticipant.job.status === JobState.FINISHED

    const reviewToCreator =
      craetorOfReview.role === ParticipationRole.FULFILLER &&
      reviewedParticipant.role === ParticipationRole.CREATOR &&
      reviewedParticipant.job.status === JobState.FINISHED

    if (reviewToFulfiller || reviewToCreator) {
      await this.#reviewRepository.save({
        score,
        review,
        reviewedParticipant,
        reviewer: craetorOfReview,
      })

      const oldNumberOfReviews =
        reviewedParticipant.user.scoreSummary.numberOfReviews

      await this.#scoreSummaryRepository.save({
        id: reviewedParticipant.user.scoreSummary.id,
        numberOfReviews: oldNumberOfReviews + 1,
        score:
          (reviewedParticipant.user.scoreSummary.score * oldNumberOfReviews +
            score) /
          (oldNumberOfReviews + 1),
      })

      return
    }

    throw new ServiceError(401, "unauthorized to create review")
  }

  /**
   * role of user in review can be: reciver(then reviews that he recived will be send) or reviewer(then reviews that he created will be send)
   */
  async getUserReviews(
    { page, count, sortBy, role }: GetReviewsQueryData,
    username: string
  ) {
    if (page && count && (page <= 0 || count <= 0)) {
      return { page, count, totalCount: 0, users: [] }
    }
    const participarions = await this.#participationRepository.findBy({
      user: {
        username,
      },
    })

    if (!participarions.length) {
      return {
        page: page || 1,
        count: 0,
        totalCount: 0,
        reviews: [],
      }
    }

    const participarionIds: { id: number }[] = []
    participarions.forEach(data => participarionIds.push({ id: data.id }))

    const [foundReviews, reviewsCount] =
      await this.#reviewRepository.findAndCount({
        relations: {
          reviewedParticipant: true,
          reviewer: true,
        },
        where: {
          reviewedParticipant:
            role === "reciver" ? participarionIds : undefined,
          reviewer: role === "reviewer" ? participarionIds : undefined,
        },
        order: {
          created: getOrder("created", sortBy),
          score: getOrder("score", sortBy),
        },
        skip: ((page || 1) - 1) * (count || 1),
        take: count || undefined,
      })

    const reviews: {
      id: number
      reviewerUsername: string
      jobId: number
      review: string
      score: number
      created: Date
    }[] = []

    foundReviews.forEach(data => {
      reviews.push({
        id: data.id,
        reviewerUsername: data.reviewer.user.username,
        jobId: data.reviewer.job.id,
        review: data.review,
        score: data.score,
        created: data.created,
      })
    })

    return {
      page: page || 1,
      count: reviews.length,
      totalCount: reviewsCount,
      reviews,
    }
  }

  async deleteReview(reviewId: number, username: string) {
    const participarions = await this.#participationRepository.find({
      relations: { user: true },
      where: {
        user: {
          username,
        },
      },
      select: {
        id: true,
      },
    })

    const review = await this.#reviewRepository.findOne({
      where: {
        id: reviewId,
        reviewedParticipant: participarions as { id: number }[],
      },
    })

    if (!review) {
      throw new ServiceError(404, "review not found")
    }

    await this.#reviewRepository.delete({
      id: reviewId,
    })
  }
}

export default UsersService
