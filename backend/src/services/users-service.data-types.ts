export interface CreateUserData {
  username: string
  email: string
  password: string
}
export const CreateUserRequired = ["username", "email", "password"]

export interface LoginData {
  username: string
  password: string
}
export const LoginRequired = ["username", "password"]

export interface NewAuthTokenData {
  refreshToken: string
}
export const NewAuthTokenRequired = ["refreshToken"]

export interface ForgotPasswordData {
  email: string
}
export const ForgotPasswordRequired = ["email"]

export interface ResetForgottenPasswordData {
  newPassword: string
  resetToken: string
}
export const ResetForgottenPasswordRequired = ["newPassword", "resetToken"]

export interface ChangePasswordData {
  oldPassword: string
  newPassword: string
}
export const ChangePasswordRequired = ["oldPassword", "newPassword"]

export interface UpdateUserData {
  email: string
  phoneNumber: string
  region: string
  description: string
  specializations: string[]
}
export const UpdateUserRequired = ["email"]

export interface CreateReviewData {
  jobId: number
  score: number
  review: string
}
export const CreateReviewRequired = ["jobId", "score", "review"]

export interface GetReviewsQueryData {
  sortBy?: string[]
  page?: number
  count?: number
  role?: string
}
