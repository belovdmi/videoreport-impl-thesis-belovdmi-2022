import dataSource from "../config/data-source.config"
import User from "../entities/User"
import { ServiceError } from "../utils/controller-error-handler"
import { BlockUserData, GiveAdminRightsData } from "./admins-service.data-types"

class AdminsService {
  #userRepository = dataSource.getRepository(User)

  async #findUserByUsername(username: string) {
    const user = await this.#userRepository.findOneBy({ username })
    if (!user) {
      throw new ServiceError(404, "user not found")
    }

    return user
  }

  async #changeAdminRights(username: string, setRights: boolean) {
    const user = await this.#findUserByUsername(username)

    await this.#userRepository.save({ id: user.id, isAdmin: setRights })
  }

  async #changeBlocked(username: string, setBlock: boolean) {
    const user = await this.#findUserByUsername(username)

    await this.#userRepository.save({ id: user.id, isBlocked: setBlock })
  }

  async giveAdminRights({ username }: GiveAdminRightsData) {
    await this.#changeAdminRights(username, true)
  }

  async removeAdminRights(username: string) {
    await this.#changeAdminRights(username, false)
  }

  async blockUser({ username }: BlockUserData) {
    await this.#changeBlocked(username, true)
  }

  async getBlockedUsers() {
    return this.#userRepository.find({
      select: { username: true },
      where: { isBlocked: true },
    })
  }

  unblockUser = async (username: string) => {
    await this.#changeBlocked(username, false)
  }
}

export default AdminsService
