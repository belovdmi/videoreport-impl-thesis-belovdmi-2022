export interface GiveAdminRightsData {
  username: string
}
export const GiveAdminRightsRequired = ["username"]

export interface BlockUserData {
  username: string
}
export const BlockUserRequired = ["username"]
