export interface CreateJobData {
  title: string
  description: string
  price: number
  dueDate: string
  phoneNumber: string
  category: string
  email: string
  address: string
  region: string
  town: string
  advertisementLink: string
}
export const CreateJobRequired = [
  "title",
  "price",
  "dueDate",
  "phoneNumber",
  "email",
  "address",
  "region",
  "town",
]

export interface GetJobsQueryData {
  sortBy?: string[]
  page?: number
  count?: number
  priceLte?: number
  priceGte?: number
  dueDateLte?: string
  dueDateGte?: string
  region?: string[]
  username?: string
  category?: string
  status?: string
  role?: string
}

export interface GetSubscribersQueryData {
  sortBy?: string
}

export interface VideoResultData {
  location: string
  key: string
}

export type UpdateJobData = CreateJobData
export const UpdateJobRequired = CreateJobRequired
