import { Response } from "express"
import log from "./log"

export class ServiceError extends Error {
  statusCode: number

  constructor(statusCode: number, message?: string | undefined) {
    super(message)

    this.name = "PageNotFoundError"
    this.statusCode = statusCode
  }
}

/**
 * handles sending of error to user and prints error to console
 * @param  {unknown} error that should be send
 * @param  {Response} res for sending responses to user
 */
const controllerErrorHandler = async (error: unknown, res: Response) => {
  if (error instanceof ServiceError) {
    res.status(error.statusCode).send(error.message)
  } else {
    res.sendStatus(500)
    log.error(error)
  }
}

export default controllerErrorHandler
