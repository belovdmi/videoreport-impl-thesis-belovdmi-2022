import logger from "pino"
import dayjs from "dayjs"

// just to make logs prettier, the benefit is also that this approach is non-blocking
const log = logger({
  base: {
    pid: false,
  },
  timestamp: () => `,"time":"${dayjs().format()}"`,
  transport: {
    target: "pino-pretty",
    options: { colorize: true },
  },
})

export default log
