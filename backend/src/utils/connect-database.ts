import { DataSource } from "typeorm"
import log from "./log"

/**
 * establishes connection to database
 * @param  {DataSource} dataSource configured connetion to database
 * @throws {Error} if can't connect
 */
const connectDatabase = async (dataSource: DataSource) => {
  try {
    await dataSource.initialize()
    log.info("DB connected")
  } catch (error) {
    log.error(error)
    throw new Error("Could not connect to db")
  }
}

export default connectDatabase
