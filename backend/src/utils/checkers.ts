/* eslint-disable import/prefer-default-export */
import { RegionsCZE } from "../entities/User"

export const checkRegion = function (regionToCheck: string) {
  return Object.values(RegionsCZE).includes(regionToCheck as RegionsCZE)
}
