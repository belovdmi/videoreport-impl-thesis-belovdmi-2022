/**
 * gets direction of found order
 * @param  {string} name of order column to be found
 * @param  {string[]} orders array of orders
 * @returns {undefined | string} direction of order or undefined (if order not found)
 */
const getOrder = (name: string, orders?: string[]) => {
  const found = orders?.find(item => {
    return item.startsWith(name)
  })

  if (found?.includes(".asc") && found.length === name.length + 4) {
    return "ASC"
  }

  if (found?.includes(".desc") && found.length === name.length + 5) {
    return "DESC"
  }

  return undefined
}

export default getOrder
