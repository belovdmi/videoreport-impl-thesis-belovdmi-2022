import express from "express"
import https from "https"
import fs from "fs"
import connectDatabase from "./utils/connect-database"
import log from "./utils/log"
import dataSource from "./config/data-source.config"
import usersRouter from "./routes/users.routes"
import jobsRouter from "./routes/jobs.routes"
import adminsRouter from "./routes/admins.routes"
import initDatabase from "./config/init-database.config"

const swaggerUi = require("swagger-ui-express")
const swaggerDocument = require("./swagger/swagger.json")

require("dotenv").config()

// Set SSH
const privateKey = fs.readFileSync("src/certs/ssl/selfsigned.key", "utf8")
const certificate = fs.readFileSync("src/certs/ssl/selfsigned.crt", "utf8")
const credentials = { key: privateKey, cert: certificate }

// App setup
const app = express()
app.use(express.json())
const httpsServer = https.createServer(credentials, app)

// App swagger ui
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// Routes
app.use("/api/v1/users", usersRouter)
app.use("/api/v1/jobs", jobsRouter)
app.use("/api/v1/admins", adminsRouter)

httpsServer.listen(process.env.SERVER_PORT, async () => {
  log.info(`App is running at port ${process.env.SERVER_PORT}`)
  await connectDatabase(dataSource)
  await initDatabase(dataSource)
})
