import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm"

@Entity("category")
class Category extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ unique: true })
  name: string
}

export default Category
