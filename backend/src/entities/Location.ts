import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm"
import { RegionsCZE } from "./User"

@Entity("location")
class Location extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: "enum", enum: RegionsCZE })
  region: RegionsCZE

  @Column({ type: "varchar" })
  address: string

  @Column({ type: "varchar" })
  town: string
}

export default Location
