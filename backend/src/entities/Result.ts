import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm"
import type Participation from "./Participation"

export enum ResultType {
  VIDEO = "video",
}

@Entity("result")
class Result extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  // "name" of file in AWS S3
  @Column()
  key: string

  // link to download from AWS S3
  @Column()
  location: string

  @Column({ type: "enum", enum: ResultType })
  type: ResultType

  @ManyToOne("participation", "results", {
    nullable: false,
    onDelete: "CASCADE",
  })
  participant: Participation
}

export default Result
