import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm"
import Category from "./Category"
import ScoreSummary from "./ScoreSummary"

export enum RegionsCZE {
  PHA = "PHA",
  STČ = "STČ",
  JHČ = "JHČ",
  PLK = "PLK",
  KVK = "KVK",
  ULK = "ULK",
  LBK = "LBK",
  HKK = "HKK",
  PAK = "PAK",
  VYS = "VYS",
  JHM = "JHM",
  OLK = "OLK",
  MSK = "MSK",
  ZLK = "ZLK",
}

@Entity("user")
class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: "varchar", unique: true })
  username: string

  @Column({ type: "varchar", unique: true })
  email: string

  @Column({ type: "varchar", nullable: true })
  phoneNumber: string

  @Column({ type: "enum", enum: RegionsCZE, nullable: true })
  region: RegionsCZE | null

  @Column({ type: "varchar", nullable: true })
  description: string

  @Column({ select: false })
  password: string

  @Column()
  isAdmin: boolean

  @Column()
  isBlocked: boolean

  @OneToOne("score_summary", { nullable: false, eager: true })
  @JoinColumn()
  scoreSummary: ScoreSummary

  @ManyToMany("category")
  @JoinTable()
  specializations: Category[]
}

export default User
