import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm"

@Entity("score_summary")
class ScoreSummary extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  jobsGiven: number

  @Column()
  jobsTaken: number

  @Column()
  numberOfReviews: number

  @Column({ type: "decimal", precision: 5, scale: 1 })
  score: number
}

export default ScoreSummary
