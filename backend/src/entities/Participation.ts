import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm"

import type Job from "./Job"
import type Result from "./Result"
import User from "./User"

export enum ParticipationRole {
  FULFILLER = "fulfiller",
  SUBSCRIBER = "subscriber",
  ASSIGNED = "assigned",
  CREATOR = "creator",
}

@Entity("participation")
class Participation extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: "enum", enum: ParticipationRole })
  role: ParticipationRole

  @OneToMany("result", "participant")
  results: Result[]

  @ManyToOne("job", "participations", { onDelete: "CASCADE", eager: true })
  job: Job

  @ManyToOne("user", { nullable: false, eager: true })
  user: User
}

export default Participation
