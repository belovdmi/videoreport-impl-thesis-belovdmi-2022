import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm"

// Why import type: https://stackoverflow.com/questions/62980221/dependency-cycle-detected-eslint-with-typeorm
import type Category from "./Category"
import type Location from "./Location"
import type Participation from "./Participation"

export enum JobState {
  FREE = "free",
  ASSIGNED = "assigned",
  IN_PROGRESS = "in_progress",
  FINISHED = "finished",
}

@Entity("job")
class Job extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: "varchar" })
  advertisementLink: string

  @Column({ type: "varchar" })
  contactEmail: string

  @Column({ type: "varchar" })
  contactPhoneNumber: string

  @CreateDateColumn()
  created: Date

  @Column({ type: "varchar" })
  title: string

  @Column({ type: "varchar" })
  description: string

  @Column({ type: "date" })
  dueDate: Date

  @Column()
  price: number

  @Column({ type: "enum", enum: JobState, default: JobState.FREE })
  status: JobState

  @OneToOne("location", { onDelete: "CASCADE", nullable: false, eager: true })
  @JoinColumn()
  location: Location

  @ManyToOne("category")
  category: Category

  @OneToMany("participation", "job", { nullable: false })
  participants: Participation[]
}

export default Job
