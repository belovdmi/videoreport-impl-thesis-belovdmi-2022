import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm"
import Participation from "./Participation"

@Entity("review")
class Review extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @CreateDateColumn()
  created: Date

  @Column({ type: "varchar" })
  review: string

  @Column({ type: "decimal", precision: 5, scale: 1 })
  score: number

  // reviewer and reviewedParticipant cannot be same participant / user !!
  @OneToOne("participation", { nullable: false, orphanedRowAction: "delete" })
  @JoinColumn()
  reviewer: Participation

  @OneToOne("participation", { nullable: false, orphanedRowAction: "delete" })
  @JoinColumn()
  reviewedParticipant: Participation
}

export default Review
