## About project

The thesis focuses on the development of a web portal prototype. First, the requirements for the prototype are determined using competition analysis. Subsequently, using software engineering methods, a suitable solution is designed to meet the requirements. Both the technologies used in the implementation and the necessary prototype functional are designed. After the design, I will demonstrate the use of the most important technologies in the implementation of the prototype. The entire implementation is written in JavaScript using TypeScript and other tools and libraries like Express and React. The finished implementation is then tested and the shortcomings of the implemented prototype are determined.

The result is a functional prototype fulfilling the identified requirements. Backend is implementing REST API.

## Setup

Before you start installing, ensure that you have npm a node. Npm should be at least at version 8.1. and node at least at 17.0.0.

## PostgreSQL

The application requires postgreSQL to be installed and running. After installation, please change .env params that are starting with DB\_... to your postgreSQL configuration. Make sure that everything is running on correct ports with correct credentials.

## Redis

The application requires Redis to be installed and running. If you want to run on localhost (instead of docker-compose) please change redis-client.config.ts file. Change parameter host from "redis" to "localhost". Make sure that everything is running on correct port 6379.

## Installation

Before starting the application, please install dependencies with the following command:

```sh
npm install
```

## Start application

Before starting, make sure that postgreSQL and Redis are up and running and that their configurations are correct. Application will be started by the following command:

```sh
npm run serve
```

After that, backend will be running at https://localhost:3000 . Because https is using self-signed certs, chrome and other browsers will display a warning "Your connection is not private". Just click on advanced and then proceed. After each change (save) while running backend will be restarted for the changes to take effect.

On first connection to database backend will initialize it by creating basic categories and one admin user. (username: admin, pwd: Admin123). In aplication eamil is used for sending notifications and links to reset pwd:

- email: zchecknito@gmail.com
- pwd: Zchecknito123

## Swagger

Application is using swagger documentation for API. So you can interact with endpoints on:
https://localhost:3000/api-docs

## Main tools and technologies

- aws-sdk (AWS S3) -> https://www.npmjs.com/package/aws-sdk
- bcrypt -> https://www.npmjs.com/package/bcrypt
- dotenv -> https://www.npmjs.com/package/dotenv
- nodemon -> https://www.npmjs.com/package/nodemon
- TypeScript -> https://www.typescriptlang.org/
- express -> https://expressjs.com/
- express-validator -> https://express-validator.github.io/docs/
- https -> https://www.npmjs.com/package/https
- ioredis (redis) -> https://www.npmjs.com/package/ioredis
- jsonwebtoken -> https://www.npmjs.com/package/jsonwebtoken
- JWT -> https://jwt.io/
- multer -> https://www.npmjs.com/package/multer
- multer-s3 -> https://www.npmjs.com/package/multer-s3
- nodemailer -> https://nodemailer.com/about/ -> used email: zchecknito@gmail.com, pwd: Zchecknito123
- swagger-ui-express -> https://www.npmjs.com/package/swagger-ui-express
- swagger -> https://swagger.io/
- typeorm -> https://typeorm.io/
- eslint -> https://eslint.org/
- prettier -> https://prettier.io/
- REST API -> https://www.redhat.com/en/topics/api/what-is-a-rest-api#rest (just info what it is)